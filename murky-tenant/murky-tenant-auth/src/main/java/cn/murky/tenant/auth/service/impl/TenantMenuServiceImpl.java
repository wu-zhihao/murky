package cn.murky.tenant.auth.service.impl;

import cn.murky.tenant.core.utils.SecurityUtils;
import cn.murky.tenant.system.api.TenantMenuApi;
import cn.murky.tenant.system.api.domain.vo.TenantMenuTreeVO;
import cn.murky.tenant.system.api.enums.MenuType;
import cn.murky.tenant.auth.service.ITenantMenuService;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.ArrayList;
import java.util.List;

@Component
public class TenantMenuServiceImpl implements ITenantMenuService {
    @Inject
    private TenantMenuApi tenantMenuApi;
    @Override
    public List<TenantMenuTreeVO> treeSysMenu(List<MenuType> menuTypes) {
        return tenantMenuApi.treeSysMenu(menuTypes);
    }
}
