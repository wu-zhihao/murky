package cn.murky.tenant.auth.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.murky.common.constant.CommonErrorConstant;
import cn.murky.common.web.ApiResult;
import cn.murky.security.utils.CaptchaUtil;
import cn.murky.tenant.auth.domain.dto.LoginDto;
import cn.murky.tenant.auth.record.AuthRecord;
import cn.murky.tenant.core.utils.SecurityUtils;
import cn.murky.tenant.system.api.domain.vo.TenantMenuTreeVO;
import cn.murky.tenant.system.api.enums.MenuType;
import cn.murky.tenant.auth.service.IMurkyLoginService;
import cn.murky.tenant.auth.service.ITenantMenuService;
import cn.murky.tenant.core.SecurityTenantUserInfo;
import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Context;
import org.noear.solon.validation.annotation.Valid;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.anji.captcha.controller.CaptchaController.getRemoteId;

/***
 * 安全控制器
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("auth")
public class MurkyAuthController {

    @Inject
    private IMurkyLoginService iMurkyLoginService;
    @Inject
    private ITenantMenuService iTenantMenuService;
    @Inject
    private CaptchaService captchaService;


    @Post
    @Mapping("/captcha")
    public ApiResult<Object> check(CaptchaVO data, Context request) {
        data.setBrowserInfo(getRemoteId(request));
        ResponseModel responseModel = this.captchaService.check(data);
        return CaptchaUtil.parseApiResult(responseModel);
    }

    /**
     * 获取图形验证码信息
     * @return token
     */
    @Get
    @Mapping("/captcha")
    public ApiResult<Object> getCaptcha(CaptchaVO data, Context request) {
        assert request.realIp() != null;

        data.setBrowserInfo(getRemoteId(request));

        ResponseModel responseModel = captchaService.get(data);
        return CaptchaUtil.parseApiResult(responseModel);
    }

    /**
     * 登录
     * @param loginDto loginDto
     * @return token
     */
    @Post
    @Mapping("login")
    public ApiResult<Object> login(@Body LoginDto loginDto) throws Exception {
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(loginDto.getCaptchaVerification());
        ResponseModel response = captchaService.verification(captchaVO);
        ApiResult<Object> apiResult = CaptchaUtil.parseApiResult(response);
        if(CommonErrorConstant.SUCCESS.errCode().equals(apiResult.getCode())){
            AuthRecord login = iMurkyLoginService.login(loginDto);
            Map<String,Object> result=new HashMap<>();
            result.put("token",login.saTokenInfo().getTokenValue());
            result.put("tenantId",login.tenantId());
            return ApiResult.ok(result);
        }
        return apiResult;
    }

    /**
     * 登出
     */
    @Post
    @Mapping("logout")
    public ApiResult<?> logout() {
//        SecurityUtils.delUserMenu();
        SecurityUtils.delUserInfo();
        StpUtil.logout();
        return ApiResult.ok();
    }

    /**
     * 获取菜单
     * @return List<TenantMenuTreeVO>
     */
    @Get
    @Mapping("menu")
    public ApiResult<List<TenantMenuTreeVO>> menu() {
        return ApiResult.ok(iTenantMenuService.treeSysMenu(Arrays.asList(MenuType.MENU, MenuType.DIRECTORY)));
    }

    /**
     * 获取用户信息
     * @return SecurityTenantUserInfo
     */
    @Get
    @Mapping("info")
    public ApiResult<SecurityTenantUserInfo> userInfo() {
        return ApiResult.ok(iMurkyLoginService.userInfo());
    }
}
