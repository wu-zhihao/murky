package cn.murky.tenant.auth.controller;

import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.tenant.auth.domain.dto.EditPasswordDTO;
import cn.murky.tenant.core.utils.SecurityUtils;
import cn.murky.tenant.system.api.TenantUserApi;
import cn.murky.tenant.system.api.domain.UserProfile;
import cn.murky.tenant.system.api.domain.dto.ProfileFromDTO;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;

/***
 * 个人信息控制器
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("profile")
public class MurkyProfileController extends BaseController {
    @Inject
    private TenantUserApi tenantUserApi;

    /**
     * 修改语言偏好
     * @param language 语言
     */
    @Post
    @Mapping("language/{language}")
    public ApiResult<?> setLanguage(String language) {
        return toResult(tenantUserApi.setLanguage(language));
    }

    /**
     * 获取用户展示信息
     * @return UserProfile
     */
    @Get
    @Mapping
    public ApiResult<UserProfile> info() {
        return ApiResult.ok(tenantUserApi.getProfile(SecurityUtils.getUserId()));
    }

    /**
     * 修改用户信息
     * @param profileFromDTO profileFromDTO
     */
    @Put
    @Mapping
    public ApiResult<?> editProfile(@Body ProfileFromDTO profileFromDTO) {
        return toResult(tenantUserApi.setProfile(profileFromDTO));
    }

    /**
     * 修改密码
     * @param editPasswordDTO editPasswordDTO
     */
    @Put
    @Mapping
    public ApiResult<?> editProfile(@Body EditPasswordDTO editPasswordDTO) {
        return toResult(tenantUserApi.setPassword(editPasswordDTO.getOldPassword(),editPasswordDTO.getPassword(),editPasswordDTO.getSurePassword()));
    }

}
