package cn.murky.tenant.auth.constant;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstant {
    public static final ErrorRecord ACCOUNT_PASSWORD_ERROR = new ErrorRecord(10000, "login.accountOrPasswordError");

}
