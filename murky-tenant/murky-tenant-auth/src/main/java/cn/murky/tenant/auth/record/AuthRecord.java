package cn.murky.tenant.auth.record;

import cn.dev33.satoken.stp.SaTokenInfo;

public record AuthRecord(SaTokenInfo saTokenInfo,Long tenantId) {
}
