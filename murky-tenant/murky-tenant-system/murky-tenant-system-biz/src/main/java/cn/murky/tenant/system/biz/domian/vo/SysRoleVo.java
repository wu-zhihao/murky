package cn.murky.tenant.system.biz.domian.vo;

import cn.murky.common.enums.DataScope;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 角色视图类,包含对应的菜单关系
 *
 * @since hans
 */
@Data
@Accessors(chain = true)
public class SysRoleVo implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色码,全局唯一
     */
    private String roleCode;

    /**
     * 描述
     */
    private String describe;

    /**
     * 数据权限
     */
    private DataScope dataScope;

    /**
     * 菜单id集合
     */
    private List<Long> fkMenuIds;

    /**
     * 部门Id集合
     */
    private List<Long> fkDeptIds;
}
