package cn.murky.tenant.system.biz.domian.dto;

import cn.murky.tenant.system.api.enums.SysNoticeType;
import cn.murky.tenant.system.biz.domian.vo.UserNoticePageVO;
import cn.murky.tenant.system.biz.enums.UserNoticeReadStatus;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 通知中心请求参数实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class UserNoticePageDTO extends Page<UserNoticePageVO> {
    /**
     * 标题
     */
    private String title;

    /**
     * 公告类型（0通知 1公告）
     */
    private SysNoticeType type;

    /**
     * 用户通知读取状态（0已读 1未读）
     */
    private UserNoticeReadStatus readStatus;

}
