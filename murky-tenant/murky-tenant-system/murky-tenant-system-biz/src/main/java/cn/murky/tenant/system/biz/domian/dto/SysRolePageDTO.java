package cn.murky.tenant.system.biz.domian.dto;

import cn.murky.tenant.system.biz.domian.entity.SysRole;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 角色分页DTO实体类
 * @since hans
 */
@Data
@Accessors(chain = true)
public class SysRolePageDTO extends Page<SysRole> {
    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色码
     */
    private String roleCode;
}
