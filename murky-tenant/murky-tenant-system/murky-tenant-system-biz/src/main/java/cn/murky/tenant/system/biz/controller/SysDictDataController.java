package cn.murky.tenant.system.biz.controller;

import cn.murky.common.domain.bo.SysDictDataBO;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.tenant.system.biz.service.ISysDictDataService;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;

import java.util.List;

/**
 * 字典数据管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("dictData")
public class SysDictDataController extends BaseController<ISysDictDataService> {

    /**
     * 获取指定字典数据
     * @param dictType 字典类型
     * @return List<SysDictDataBO>
     */
    @Get
    @Mapping("dict/{dictType}")
    public ApiResult<List<SysDictDataBO>> list(String dictType) {
        List<SysDictDataBO> result = baseService.getDict(dictType);
        return ApiResult.ok(result);
    }

}
