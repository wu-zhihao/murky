package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.system.biz.domian.entity.SysNotice;
import com.mybatisflex.core.BaseMapper;

/**
 * 系统通知Mapper
 * @author hans
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {
}
