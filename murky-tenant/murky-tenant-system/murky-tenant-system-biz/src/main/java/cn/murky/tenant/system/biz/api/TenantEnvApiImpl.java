package cn.murky.tenant.system.biz.api;

import cn.murky.tenant.core.EnvApi;
import cn.murky.tenant.system.api.TenantEnvApi;
import cn.murky.tenant.system.biz.service.ITenantEnvService;
import org.noear.redisx.RedisClient;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.Optional;

@Component
public class TenantEnvApiImpl implements TenantEnvApi, EnvApi {
    @Inject
    private ITenantEnvService iTenantEnvService;
    @Override
    public Optional<RedisClient> getRedisClientByTenantId(Long tenantId) {
        return iTenantEnvService.getRedisClientByTenantId(tenantId);
    }
}
