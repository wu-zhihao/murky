package cn.murky.tenant.system.biz.convert;

import cn.murky.tenant.system.api.domain.bo.SysRoleBO;
import cn.murky.tenant.system.biz.domian.dto.SysRoleFromDTO;
import cn.murky.tenant.system.biz.domian.entity.SysRole;
import cn.murky.tenant.system.biz.domian.vo.SysRoleVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SysRoleConvert {
    SysRoleConvert INSTANCES = Mappers.getMapper(SysRoleConvert.class);

    SysRoleBO toBO(SysRole sysRole);

    /**
     * 将SysRoleFromDTO转为SysRole
     */
    @Mapping(target = "fkDeptId", ignore = true)
    @Mapping(target = "updateUser", ignore = true)
    @Mapping(target = "updateTime", ignore = true)
    @Mapping(target = "createUser", ignore = true)
    @Mapping(target = "createTime", ignore = true)
    SysRole toEntity(SysRoleFromDTO sysRoleFromDTO);

    /**
     * 将SysRole对象转化为SysRoleVo对象
     */
    @Mapping(target = "fkMenuIds", ignore = true)
    @Mapping(target = "fkDeptIds", ignore = true)
    SysRoleVo toVo(SysRole sysRole);

}
