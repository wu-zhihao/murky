package cn.murky.tenant.system.biz.service;

import cn.murky.tenant.system.biz.domian.dto.UserNoticePageDTO;
import cn.murky.tenant.system.biz.domian.entity.SysNotice;
import cn.murky.tenant.system.biz.domian.vo.UserNoticePageVO;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;

public interface ISysNoticeService extends IService<SysNotice> {


    /**
     * 获取有效通知
     *
     * @return 有效通知列表
     */
    Page<UserNoticePageVO> getEffectiveList(UserNoticePageDTO dto);
}
