package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.core.SecurityTenantUserInfo;
import cn.murky.tenant.core.utils.DataScopeUtils;
import cn.murky.tenant.system.biz.domian.entity.SysDept;
import cn.murky.tenant.system.biz.domian.entity.table.SysDeptTableDef;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.query.QueryWrapper;

import java.util.List;

public interface SysDeptMapper extends BaseMapper<SysDept> {

    default List<SysDept> getSelectByCreate(SecurityTenantUserInfo userInfo){
        SysDeptTableDef SYS_DEPT = SysDeptTableDef.SYS_DEPT;
        QueryWrapper queryWrapper = QueryWrapper.create().select().from(SYS_DEPT)
                .orderBy(SYS_DEPT.SORT.asc(), SYS_DEPT.DEPT_NAME.asc());
        DataScopeUtils.dataScope(queryWrapper, userInfo);
        return this.selectListByQuery(queryWrapper);
    }

}
