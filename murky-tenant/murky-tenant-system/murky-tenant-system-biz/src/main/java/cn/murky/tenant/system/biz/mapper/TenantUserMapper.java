package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.system.biz.domian.entity.TenantUser;
import cn.murky.tenant.system.biz.domian.entity.table.TenantUserTableDef;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.query.QueryWrapper;

public interface TenantUserMapper extends BaseMapper<TenantUser> {


    /**
     * 根据账号查询
     * @param account 账号
     * @return 租户用户信息
     */
    default TenantUser selectByAccount(String account){
        TenantUserTableDef TENANT_USER = TenantUserTableDef.TENANT_USER;
        return selectOneByQuery(QueryWrapper.create()
                .where(TENANT_USER.ACCOUNT.eq(account)));
    }

    /**
     * 根据部门id查询用户数量
     *
     * @param deptId 部门id
     * @return 部门用户数量
     */
    default Long getCountByDeptId(Long deptId) {
        TenantUserTableDef TENANT_USER = TenantUserTableDef.TENANT_USER;
        return selectCountByQuery(QueryWrapper.create()
                .from(TENANT_USER)
                .where(TENANT_USER.FK_DEPT_ID.eq(deptId))
        );
    }

    /**
     * 根据用户id,修改密码
     *
     * @param userId   用户id
     * @param password 密码
     * @return 受影响行数
     */
    default int resetPassword(Long userId, String password, String salt) {
        TenantUserTableDef TENANT_USER = TenantUserTableDef.TENANT_USER;
        return this.updateByQuery(new TenantUser().setPassword(password).setSalt(salt)
                , true, QueryWrapper.create().where(
                        TENANT_USER.ID.eq(userId)
                ));
    }
}
