package cn.murky.tenant.system.biz.domian.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 系统部门表
 */
@Data
@Table(schema = "tenant",value = "sys_dept")
@Accessors(chain = true)
public class SysDept extends BaseEntity<SysDept> {

    /**+
     * 部门id
     */
    @Id
    private Long id;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 父级部门id
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;
}
