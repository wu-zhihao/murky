package cn.murky.tenant.system.biz.service.impl;

import cn.murky.common.utils.CollectionUtils;
import cn.murky.core.exception.ServiceException;
import cn.murky.tenant.core.utils.SecurityUtils;
import cn.murky.tenant.system.biz.convert.SysDeptConvert;
import cn.murky.tenant.system.biz.domian.dto.SysDeptFromDTO;
import cn.murky.tenant.system.biz.domian.entity.SysDept;
import cn.murky.tenant.system.biz.domian.entity.SysDeptAncestors;
import cn.murky.tenant.system.biz.domian.vo.SysDeptTreeVO;
import cn.murky.tenant.system.biz.mapper.SysDeptAncestorsMapper;
import cn.murky.tenant.system.biz.mapper.SysDeptMapper;
import cn.murky.tenant.system.biz.mapper.TenantUserMapper;
import cn.murky.tenant.system.biz.service.ISysDeptService;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.annotation.Tran;

import java.util.ArrayList;
import java.util.List;

import static cn.murky.core.constant.ErrorConstant.ADD_ERROR;
import static cn.murky.core.constant.ErrorConstant.EDIT_ERROR;
import static cn.murky.tenant.system.api.constant.ErrorConstant.DEPT_HAS_CHILD;
import static cn.murky.tenant.system.api.constant.ErrorConstant.DEPT_IS_USED;

@Component
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Inject
    private SysDeptAncestorsMapper sysDeptAncestorsMapper;
    @Inject
    private TenantUserMapper tenantUserMapper;
    @Inject
    private ISysDeptService iSysDeptService;

    /**
     * 获取部门树
     * @return 部门树视图对象
     */
    @Override
    public List<SysDeptTreeVO> treeDept() {
        List<SysDept> allSysDept = mapper.getSelectByCreate(SecurityUtils.getUserInfo());
        List<SysDeptTreeVO> SysDeptTreeVOList = SysDeptConvert.INSTANCES.toVOs(allSysDept);
        if(CollectionUtils.isEmpty(SysDeptTreeVOList)){
            return SysDeptTreeVOList;
        }
        long minPid = SysDeptTreeVOList.stream().mapToLong(SysDeptTreeVO::getParentId).min().getAsLong();
        List<SysDeptTreeVO> list = SysDeptTreeVOList.stream().filter(item -> item.getParentId() == minPid).toList();
        buildTreeSysDept(list,SysDeptTreeVOList);
        return list;
    }

    /**
     * 保存部门
     * 1.保存部门信息
     * 2.保存部门层级关系信息
     * @return 保存状态
     */
    @Override
    @Tran
    public Boolean save(SysDeptFromDTO sysDeptFromDTO)  {
        SysDept entity = sysDeptFromDTO.toEntity();
        int insert = mapper.insert(entity);
        if(insert>0){
            List<SysDeptAncestors> SysDeptAncestors=new ArrayList<>();
            SysDeptAncestors.add(new SysDeptAncestors()
                    .setFkDeptId(entity.getId())
                    .setAncestors(entity.getParentId()));
            List<SysDeptAncestors> list = sysDeptAncestorsMapper.getListByDeptId(entity.getParentId());
            for (SysDeptAncestors deptAncestors : list) {
                SysDeptAncestors.add(new SysDeptAncestors()
                        .setFkDeptId(entity.getId())
                        .setAncestors(deptAncestors.getAncestors()));
            }
            int i = sysDeptAncestorsMapper.insertBatch(SysDeptAncestors);
            if(i == SysDeptAncestors.size()){
                return true;
            }
            throw new ServiceException(ADD_ERROR);
        }
        return false;
    }


    /**
     * 修改部门
     * 1.修改部门信息
     * 2.删除部门层级关系信息
     * 3.保存部门层级关系信息
     * @param sysDeptFromDTO 部门表单对象
     * @return 新增状态
     */
    @Override
    @Tran
    public Boolean edit(SysDeptFromDTO sysDeptFromDTO) {
        SysDept entity = sysDeptFromDTO.toEntity();
        int update = mapper.update(entity);
        if(update>0){
            //组装部门关系
            List<SysDeptAncestors> SysDeptAncestors=new ArrayList<>();
            SysDeptAncestors.add(new SysDeptAncestors()
                    .setFkDeptId(entity.getId())
                    .setAncestors(entity.getParentId()));
            List<SysDeptAncestors> list = sysDeptAncestorsMapper.getListByDeptId(entity.getParentId());
            for (SysDeptAncestors deptAncestors : list) {
                SysDeptAncestors.add(new SysDeptAncestors()
                        .setFkDeptId(entity.getId())
                        .setAncestors(deptAncestors.getAncestors()));
            }
            //删除部门关系
            sysDeptAncestorsMapper.deleteByDeptId(entity.getId());
            //保存部门关系
            int i = sysDeptAncestorsMapper.insertBatch(SysDeptAncestors);
            if(i == SysDeptAncestors.size()){
                return true;
            }
            throw new ServiceException(EDIT_ERROR);
        }
        return false;
    }

    /**
     * 删除部门
     * @param deptId 部门id
     * @return 删除状态
     */
    @Override
    @Tran
    public Boolean remove(Long deptId) {
        Long countByAncestors = sysDeptAncestorsMapper.getCountByAncestors(deptId);
        if(countByAncestors>0){
            throw new ServiceException(DEPT_HAS_CHILD);
        }
        Long countByDeptId = tenantUserMapper.getCountByDeptId(deptId);
        if(countByDeptId>0){
            throw new ServiceException(DEPT_IS_USED);
        }
        sysDeptAncestorsMapper.deleteByDeptId(deptId);
        mapper.deleteById(deptId);
        return true;
    }

    @Override
    @Tran
    public Boolean drop(List<Long> deptIds) {
        List<SysDept> SysDepts=new ArrayList<>();
        for (int i = 0; i < deptIds.size(); i++) {
            Long deptId = deptIds.get(i);
            SysDepts.add(new SysDept().setId(deptId).setSort(i));
        }
        return iSysDeptService.updateBatch(SysDepts);
    }

    /**
     * 构建部门树
     *
     * @param parentDeptList 父级部门
     * @param SysDeptList   部门资源池
     */
    private void buildTreeSysDept(List<SysDeptTreeVO> parentDeptList, List<SysDeptTreeVO> SysDeptList) {
        for (SysDeptTreeVO SysDeptTreeVO : parentDeptList) {
            List<SysDeptTreeVO> SysDeptTree = new ArrayList<>();
            for (SysDeptTreeVO deptTreeVO : SysDeptList) {
                if (deptTreeVO.getParentId().equals(SysDeptTreeVO.getId())) {
                    SysDeptTree.add(deptTreeVO);
                }
            }
            buildTreeSysDept(SysDeptTree, SysDeptList);
            SysDeptTreeVO.setChildren(SysDeptTree);
        }
    }
}
