package cn.murky.tenant.system.biz.domian.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/***
 * 租户信息表
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table("tenant")
public class Tenant extends BaseEntity<Tenant> {
    /**
     * 租户id
     */
    @Id
    private Long id;

    /**
     * 权限组id
     */
    private Long fkGroupId;

    /**
     * 租户名
     */
    private String tenantName;

    /**
     * 租户管理员
     */
    private Long adminUser;

    /**
     * 到期时间
     */
    private OffsetDateTime expires;

    /**
     * 描述
     */
    private String describe;

}
