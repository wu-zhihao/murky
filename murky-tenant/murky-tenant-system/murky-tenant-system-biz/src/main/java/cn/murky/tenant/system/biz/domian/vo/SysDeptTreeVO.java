package cn.murky.tenant.system.biz.domian.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * 部门树视图
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysDeptTreeVO {
    /**
     * 部门id
     */
    private Long id;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 上级部门id
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建事件
     */
    private OffsetDateTime createTime;

    /**
     * 子部门
     */
    private List<SysDeptTreeVO> children;
}
