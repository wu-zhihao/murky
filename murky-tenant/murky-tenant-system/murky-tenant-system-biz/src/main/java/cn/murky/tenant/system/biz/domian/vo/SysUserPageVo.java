package cn.murky.tenant.system.biz.domian.vo;

import cn.murky.tenant.system.api.enums.Sex;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户分页视图类
 *
 * @since hans
 */
@Data
@Accessors(chain = true)
public class SysUserPageVo {
    /**
     * 用户id
     */
    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别 0:男性 1:女性 2:其他
     */
    private Sex sex;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 部门id
     */
    private String fkDeptId;

    /**
     * 部门名称
     */
    private String deptName;
}
