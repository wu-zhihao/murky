package cn.murky.tenant.system.biz.service;

import cn.murky.tenant.system.biz.domian.bo.SysNoticeReadBO;
import cn.murky.tenant.system.biz.domian.entity.SysNoticeRead;
import com.mybatisflex.core.service.IService;

/**
 * SysNoticeRead Service
 */
public interface ISysNoticeReadService extends IService<SysNoticeRead> {

    /**
     * 读取通知
     * @return 是否成功读取
     */
    boolean read(SysNoticeReadBO sysNoticeReadBO);

    /**
     * 取消读取通知(设置为未读)
     * @return 是否成功
     */
    boolean unread(SysNoticeReadBO sysNoticeReadBO);
}
