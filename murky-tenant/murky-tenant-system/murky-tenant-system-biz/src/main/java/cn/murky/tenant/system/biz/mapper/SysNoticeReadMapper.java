package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.system.biz.domian.entity.SysNoticeRead;
import cn.murky.tenant.system.biz.domian.entity.table.SysNoticeReadTableDef;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.query.QueryWrapper;

import java.util.List;

public interface SysNoticeReadMapper extends BaseMapper<SysNoticeRead> {


    /**
     * 根据通知id和用户id删除
     * @param fkNoticeId 通知id
     * @param fkUserId 用户id
     */
    default int deleteNoticeIdAndCreateUser(List<Long> fkNoticeId, Long fkUserId){
        SysNoticeReadTableDef SYS_NOTICE_READ = SysNoticeReadTableDef.SYS_NOTICE_READ;
        return deleteByQuery(QueryWrapper.create()
                .where(SYS_NOTICE_READ.CREATE_USER.eq(fkUserId))
                .and(SYS_NOTICE_READ.FK_NOTICE_ID.in(fkNoticeId)));
    }
}
