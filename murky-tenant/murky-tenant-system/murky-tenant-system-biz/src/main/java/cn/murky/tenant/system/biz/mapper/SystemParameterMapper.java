package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.system.biz.domian.entity.SystemParameter;
import cn.murky.tenant.system.biz.domian.entity.table.SystemParameterTableDef;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.query.QueryWrapper;

public interface SystemParameterMapper extends BaseMapper<SystemParameter> {


    default SystemParameter selectByKey(String key){
        SystemParameterTableDef SYSTEM_PARAMETER= SystemParameterTableDef.SYSTEM_PARAMETER;
        return this.selectOneByQuery(QueryWrapper.create().where(SYSTEM_PARAMETER.KEY.eq(key)));
    }
}
