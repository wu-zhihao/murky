package cn.murky.tenant.system.biz.domian.entity;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

/***
 * 租户权限码菜单关系实体
 *
 * @auth hans
 */
@Data(staticConstructor = "create")
@Accessors(chain = true)
@Table("tenant_group_menu")
public class TenantGroupMenu extends Model<TenantGroupMenu> {
    /**
     * 权限组id
     */
    private Long fkGroupId;

    /**
     * 菜单id
     */
    private Long fkMenuId;

}
