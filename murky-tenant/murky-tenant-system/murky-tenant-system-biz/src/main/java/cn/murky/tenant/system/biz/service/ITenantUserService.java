package cn.murky.tenant.system.biz.service;

import cn.murky.tenant.system.biz.domian.dto.ResetPasswordDTO;
import cn.murky.tenant.system.biz.domian.entity.TenantUser;
import com.mybatisflex.core.service.IService;

public interface ITenantUserService extends IService<TenantUser> {

    /**
     * 根据账号用户用户信息
     * @param account 账号
     * @return 用户信息
     */
    TenantUser getByAccount(String account);

    /**
     * 重置用户密码
     *
     * @param resetPasswordDto
     * @return 重置成功状态
     */
    boolean resetPassword(ResetPasswordDTO resetPasswordDto);

}
