package cn.murky.tenant.system.biz.listener;

import cn.murky.socketd.constants.SocetdEventConstants;
import org.noear.socketd.transport.core.Message;
import org.noear.socketd.transport.core.Session;
import org.noear.socketd.transport.core.listener.EventListener;
import org.noear.solon.annotation.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class AdminListener extends EventListener {

    public AdminListener(){
        // 挂载事件
        doOn(SocetdEventConstants.SYS_NOTICE_EVENT,this::doOnSysNotice);
    }

    private void doOnSysNotice(Session session, Message message){

    }
}
