package cn.murky.tenant.system.biz.service;

import cn.murky.tenant.system.biz.domian.entity.SystemParameter;
import com.mybatisflex.core.service.IService;

public interface ISystemParameterService extends IService<SystemParameter> {


    /**
     * 刷新缓存
     */
    void refresh();

    /**
     * 获取默认密码
     * @return
     */
    String getDefaultUserPassword();
}
