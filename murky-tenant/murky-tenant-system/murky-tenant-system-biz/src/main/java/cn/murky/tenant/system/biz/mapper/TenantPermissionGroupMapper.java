package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.system.biz.domian.entity.TenantPermissionGroup;
import com.mybatisflex.core.BaseMapper;

public interface TenantPermissionGroupMapper extends BaseMapper<TenantPermissionGroup> {
}
