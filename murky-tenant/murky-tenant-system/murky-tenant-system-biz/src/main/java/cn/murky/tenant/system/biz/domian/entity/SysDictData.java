package cn.murky.tenant.system.biz.domian.entity;

import cn.murky.common.domain.entity.BaseEntity;
import cn.murky.tenant.system.api.enums.CommonStatus;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典数据实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table(value = "sys_dict_data")
public class SysDictData extends BaseEntity<SysDictData> {

    /**
     * 字典编码
     */
    @Id
    private Long dictCode;

    /**
     * 字典排序
     */
    private Short dictSort;

    /**
     * 字典标签
     */
    private String dictLabel;

    /**
     * 字典值
     */
    private String dictValue;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 状态
     */
    private CommonStatus status;

    /**
     * 备注
     */
    private String remark;
}
