package cn.murky.tenant.system.biz.domian.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Table(value = "tenant_permission_group")
public class TenantPermissionGroup extends BaseEntity<TenantPermissionGroup> {

    /**
     * 权限
     */
    @Id
    private Long id;

    /**
     * 权限组名称
     */
    private String groupName;
}
