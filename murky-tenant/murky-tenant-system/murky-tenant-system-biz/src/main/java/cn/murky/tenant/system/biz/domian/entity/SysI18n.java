package cn.murky.tenant.system.biz.domian.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * i18n实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table(value = "sys_i18n")
public class SysI18n extends BaseEntity<SysI18n> {
    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * i18n编码
     */
    private String i18nKey;

    /**
     * i18n值
     */
    private String i18nValue;

    /**
     * 地区编码(字典:i18n:language)
     */
    private String language;

    /**
     * 标签(字典:i18n:tag)
     */
    private String i18nTag;
}
