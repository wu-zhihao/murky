package cn.murky.tenant.system.biz.domian.dto;

import cn.murky.tenant.system.api.enums.Sex;
import cn.murky.tenant.system.biz.domian.entity.SysUser;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户分页查询类
 *
 * @since hans
 */
@Data
@Accessors(chain = true)
public class SysUserPageDTO extends Page<SysUser> {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Sex sex;

    /**
     * 部门id
     */
    private Long fkDeptId;
}
