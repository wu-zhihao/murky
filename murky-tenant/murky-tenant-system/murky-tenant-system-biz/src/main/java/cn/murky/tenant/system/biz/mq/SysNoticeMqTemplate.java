package cn.murky.tenant.system.biz.mq;

import cn.murky.folkmq.constant.SysGroupConstant;
import cn.murky.tenant.core.utils.RedisUtils;
import cn.murky.tenant.system.biz.domian.entity.SysNotice;
import cn.murky.tenant.system.biz.endpoint.NoticeEndpoint;
import lombok.extern.slf4j.Slf4j;
import org.noear.redisx.RedisClient;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cloud.CloudEventHandler;
import org.noear.solon.cloud.annotation.CloudEvent;
import org.noear.solon.cloud.model.Event;

import static cn.murky.folkmq.constant.SysTopicConstant.SYS_NOTICE_TOPIC;

@Component
@Slf4j
@CloudEvent(topic = SYS_NOTICE_TOPIC + 1, group = SysGroupConstant.DEFAULT)
public class SysNoticeMqTemplate implements CloudEventHandler {
    @Inject
    private NoticeEndpoint noticeEndpoint;

    @Override
    public boolean handle(Event event) throws Throwable {
        log.info("[SysNoticeMqTemplate] -> 消费系统通知消息 -> topic:{}*", event.topic());
        boolean lock = RedisUtils.getCommonClient().getLock(event.topic() + "LOCK").tryLock(5);
        // 分布式锁,在集群环境下防止重复刷新缓存
        if(lock){
            SysNotice sysNotice = ONode.deserialize(event.content(), SysNotice.class);
            noticeEndpoint.sendSysNotice(sysNotice);
        }
        return true;
    }
}
