package cn.murky.tenant.system.biz.mapper;

import cn.murky.tenant.system.biz.domian.entity.SysRoleDept;
import com.mybatisflex.core.BaseMapper;

public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
