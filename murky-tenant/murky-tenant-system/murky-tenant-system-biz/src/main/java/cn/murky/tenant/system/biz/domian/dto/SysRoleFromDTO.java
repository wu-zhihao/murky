package cn.murky.tenant.system.biz.domian.dto;

import cn.murky.common.enums.DataScope;
import cn.murky.core.validat.Update;
import cn.murky.tenant.system.biz.convert.SysRoleConvert;
import cn.murky.tenant.system.biz.domian.entity.SysRole;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

import java.util.List;

/**
 * 角色表单类
 *
 * @since hans
 */
@Data
@Accessors(chain = true)
public class SysRoleFromDTO {
    /**
     * 角色id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 角色名
     */
    @NotBlank
    private String roleName;

    /**
     * 角色码
     */
    @NotBlank
    private String roleCode;

    /**
     * 描述
     */
    private String describe;

    /**
     * 数据权限
     */
    @NotNull
    private DataScope dataScope;

    /**
     * 所属菜单id
     */
    private List<Long> fkMenuIds;

    /**
     * 自定义权限部门Id
     */
    private List<Long> fkDeptIds;

    public SysRole toEntity() {
        return SysRoleConvert.INSTANCES.toEntity(this);
    }
}
