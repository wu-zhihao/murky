package cn.murky.tenant.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.tenant.system.api.domain.vo.TenantMenuTreeVO;
import cn.murky.tenant.system.api.enums.MenuType;
import cn.murky.tenant.system.biz.domian.entity.TenantMenu;
import cn.murky.tenant.system.biz.service.ITenantMenuService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.validation.annotation.Valid;

import java.util.Arrays;
import java.util.List;

/**
 * 菜单管理
 */
@Controller
@Valid
@Mapping("menu")
public class TenantMenuController extends BaseController<ITenantMenuService> {
    /**
     * 获取用户菜单
     *
     * @return List<TenantMenuTreeVO>
     */
    @Get
    @Mapping("list")
    @SaCheckPermission("menu")
    public ApiResult<List<TenantMenuTreeVO>> list() {
        List<TenantMenuTreeVO> result = baseService.treeSysMenu(Arrays.asList(MenuType.MENU, MenuType.DIRECTORY, MenuType.BUTTON));
        return ApiResult.ok(result);
    }

    /**
     * 菜单详情
     *
     * @param menuId 菜单id
     * @return TenantMenu
     */
    @Get
    @Mapping("{menuId}")
    @SaCheckPermission("menu")
    public ApiResult<TenantMenu> info(Long menuId) {
        return ApiResult.ok(baseService.getById(menuId));
    }

}
