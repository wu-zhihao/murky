package cn.murky.tenant.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.tenant.system.biz.domian.dto.ResetPasswordDTO;
import cn.murky.tenant.system.biz.domian.dto.SysUserFromDTO;
import cn.murky.tenant.system.biz.domian.dto.SysUserPageDTO;
import cn.murky.tenant.system.biz.domian.entity.SysUser;
import cn.murky.tenant.system.biz.domian.vo.SysUserPageVo;
import cn.murky.tenant.system.biz.service.ISysUserService;
import com.mybatisflex.core.paginate.Page;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

/**
 * 用户管理
 */
@Controller
@Valid
@Mapping("user")
public class SysUserController extends BaseController<ISysUserService> {
    /**
     * 用户分页
     * @param sysUserPageDTO sysUserPageDTO
     * @return Page<SysUserPageVo>
     */
    @Get
    @Mapping("page")
    @SaCheckPermission("user")
    public ApiResult<Page<SysUserPageVo>> page(SysUserPageDTO sysUserPageDTO) {
        return ApiResult.ok(baseService.page(sysUserPageDTO));
    }

    /**
     * 用户详情
     * @param userId 用户id
     * @return SysUser
     */
    @Get
    @Mapping("{userId}")
    @SaCheckPermission("user")
    public ApiResult<SysUser> info(Long userId) {
        return ApiResult.ok(baseService.info(userId));
    }

    /**
     * 新增用户
     * @param sysUserFromDTO sysUserFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("user:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) SysUserFromDTO sysUserFromDTO) {
        return toResult(baseService.save(sysUserFromDTO));
    }

    /**
     * 修改用户
     * @param sysUserFromDTO sysUserFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("user:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) SysUserFromDTO sysUserFromDTO) {
        return toResult(baseService.update(sysUserFromDTO));
    }

    /**
     * 重置密码
     * @param resetPasswordDto resetPasswordDto
     */
    @Put
    @Mapping("restPassword")
    public ApiResult<?> resetPassword(@Body @Validated ResetPasswordDTO resetPasswordDto) {

        return toResult(baseService.resetPassword(resetPasswordDto));
    }

    /**
     * 删除用户
     * @param userId 用户id
     */
    @Delete
    @Mapping("/{userId}")
    @SaCheckPermission("user:remove")
    public ApiResult<?> remove(Long userId) {
        boolean result = baseService.removeById(userId);
        return toResult(result);
    }
}
