package cn.murky.tenant.system.biz.domian.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 通知读取业务对象
 */
@Data
@Accessors(chain = true)
public class SysNoticeReadBO {
    /**
     * 读取的通知
     */
    private List<Long> fkNoticeIds;

    /**
     * 用户id
     */
    private Long fkUserId;
}
