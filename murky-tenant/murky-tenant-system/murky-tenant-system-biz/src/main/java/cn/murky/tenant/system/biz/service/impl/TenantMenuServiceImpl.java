package cn.murky.tenant.system.biz.service.impl;

import cn.murky.tenant.core.MurkyServiceImpl;
import cn.murky.tenant.core.utils.SecurityUtils;
import cn.murky.tenant.system.api.domain.vo.TenantMenuTreeVO;
import cn.murky.tenant.system.api.enums.MenuType;
import cn.murky.tenant.system.biz.convert.TenantMenuConvert;
import cn.murky.tenant.system.biz.domian.entity.TenantMenu;
import cn.murky.tenant.system.biz.mapper.TenantMenuMapper;
import cn.murky.tenant.system.biz.service.ITenantMenuService;
import org.noear.solon.annotation.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TenantMenuServiceImpl extends MurkyServiceImpl<TenantMenuMapper, TenantMenu> implements ITenantMenuService {
    @Override
    public List<TenantMenu> getByFkRoleId(List<MenuType> menuTypes, Long fkRoleId) {
        return mapper.selectByMenuTypeAndfkRoleid(menuTypes, fkRoleId);
    }

    @Override
    public List<TenantMenu> getListByAuths(List<MenuType> auths) {
        return mapper.selectListByAuths(auths);
    }

    @Override
    public List<TenantMenuTreeVO> treeSysMenu(List<MenuType> menuTypes) {
        List<TenantMenuTreeVO> allSysMenuList = TenantMenuConvert.INSTANCES.toTreeVOs(getListByAuths(menuTypes));
        List<TenantMenuTreeVO> list = allSysMenuList.stream().filter(item -> item.getParentId() == 0).toList();
        buildTreeSysMenu(list, allSysMenuList);
        return list;
    }

    /**
     * 构建菜单树
     *
     * @param parentMenuList 父级菜单
     * @param SysMenuList    菜单资源池
     */
    private void buildTreeSysMenu(List<TenantMenuTreeVO> parentMenuList, List<TenantMenuTreeVO> SysMenuList) {
        for (TenantMenuTreeVO SysMenuTreeVO : parentMenuList) {
            List<TenantMenuTreeVO> treeSysMenu = new ArrayList<>();
            for (TenantMenuTreeVO SysMenu : SysMenuList) {
                if (SysMenu.getParentId().equals(SysMenuTreeVO.getId())) {
                    treeSysMenu.add(SysMenu);
                }
            }
            buildTreeSysMenu(treeSysMenu, SysMenuList);
            SysMenuTreeVO.setChildren(treeSysMenu);
        }
    }
}
