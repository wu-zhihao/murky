package cn.murky.tenant.system.biz.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import cn.murky.core.validat.Update;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.tenant.system.biz.domian.dto.SysDeptFromDTO;
import cn.murky.tenant.system.biz.domian.entity.SysDept;
import cn.murky.tenant.system.biz.domian.vo.SysDeptTreeVO;
import cn.murky.tenant.system.biz.service.ISysDeptService;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
 * 部门管理
 */
@Controller
@Valid
@Mapping("dept")
public class SysDeptController extends BaseController<ISysDeptService> {

    /**
     * 获取部门树形菜单
     * @return List<SysDeptTreeVO>
     */
    @Get
    @Mapping("list")
    @SaCheckPermission(value = {"dept","user"},mode = SaMode.OR)
    public ApiResult<List<SysDeptTreeVO>> list(){
        List<SysDeptTreeVO> result = baseService.treeDept();
        return ApiResult.ok(result);
    }

    /**
     * 获取部门信息详情
     * @param deptId 部门id
     * @return SysDept
     */
    @Get
    @Mapping("{deptId}")
    public ApiResult<SysDept> list(Long deptId){
        SysDept result = baseService.getById(deptId);
        return ApiResult.ok(result);
    }

    /**
     * 拖动排序
     * @param deptIds 部门id集合
     * @return boolean
     */
    @Put
    @Mapping("drop")
    public ApiResult<?> drop(List<Long> deptIds){
        Boolean b = baseService.drop(deptIds);
        return toResult(b);
    }


    /**
     * 添加部门
     * @param sysDeptFromDTO sysDeptFromDTO
     * @return boolean
     */
    @Post
    @Mapping
    @SaCheckPermission("dept:add")
    public ApiResult<?> add(@Body SysDeptFromDTO sysDeptFromDTO){
        boolean b = baseService.save(sysDeptFromDTO);
        return toResult(b);
    }

    /**
     * 修改部门
     * @param sysDeptFromDTO sysDeptFromDTO
     * @return boolean
     */
    @Put
    @Mapping
    @SaCheckPermission("dept:edit")
    public ApiResult<?> edit(@Body  @Validated(Update.class) SysDeptFromDTO sysDeptFromDTO){
        boolean b = baseService.edit(sysDeptFromDTO);
        return toResult(b);
    }

    /**
     * 删除部门
     * @param deptId 部门id
     * @return boolean
     */
    @Delete
    @Mapping("{deptId}")
    @SaCheckPermission("dept:remove")
    public ApiResult<?> remove(Long deptId){
        boolean b = baseService.remove(deptId);
        return toResult(b);
    }
}
