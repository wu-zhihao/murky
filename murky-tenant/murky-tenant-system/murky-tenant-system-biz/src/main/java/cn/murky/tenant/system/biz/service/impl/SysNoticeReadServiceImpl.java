package cn.murky.tenant.system.biz.service.impl;

import cn.murky.tenant.system.biz.domian.bo.SysNoticeReadBO;
import cn.murky.tenant.system.biz.domian.entity.SysNoticeRead;
import cn.murky.tenant.system.biz.mapper.SysNoticeReadMapper;
import cn.murky.tenant.system.biz.service.ISysNoticeReadService;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;

/**
 * SysNoticeReadServiceImpl
 *
 * @auth hans
 */
@Component
public class SysNoticeReadServiceImpl extends ServiceImpl<SysNoticeReadMapper, SysNoticeRead> implements ISysNoticeReadService {

    @Tran
    @Override
    public boolean read(SysNoticeReadBO sysNoticeReadBO) {
        List<SysNoticeRead> list = sysNoticeReadBO
                .getFkNoticeIds()
                .stream()
                .map(noticeId -> {
                            SysNoticeRead sysNoticeRead = new SysNoticeRead();
                            sysNoticeRead.setFkNoticeId(noticeId);
                            sysNoticeRead.setCreateUser(sysNoticeReadBO.getFkUserId());
                            return sysNoticeRead;
                        }
                ).toList();
        return saveBatch(list);
    }

    @Override
    public boolean unread(SysNoticeReadBO sysNoticeReadBO) {
        int i = mapper.deleteNoticeIdAndCreateUser(sysNoticeReadBO.getFkNoticeIds(), sysNoticeReadBO.getFkUserId());
        return i > 0;
    }
}
