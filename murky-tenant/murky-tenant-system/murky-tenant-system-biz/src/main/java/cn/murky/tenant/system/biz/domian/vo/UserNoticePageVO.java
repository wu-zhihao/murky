package cn.murky.tenant.system.biz.domian.vo;

import cn.murky.common.enums.CommonStatus;
import cn.murky.tenant.system.api.enums.SysNoticeTarget;
import cn.murky.tenant.system.api.enums.SysNoticeType;
import cn.murky.tenant.system.biz.enums.UserNoticeReadStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/**
 * 用户通知vo实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class UserNoticePageVO {
    /**
     * 菜单id
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 公告类型（0通知 1公告）
     */
    private SysNoticeType type;

    /**
     * 通知目标（0admin 1tenant）
     */
    private SysNoticeTarget target;

    /**
     * 内容
     */
    private String content;

    /**
     * 通用状态（0正常 1停用）
     */
    private CommonStatus status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 到期时间
     */
    private OffsetDateTime expire;

    /**
     * 创建时间
     */
    private OffsetDateTime createTime;

    /**
     * 用户通知读取状态（0已读 1未读）
     */
    private UserNoticeReadStatus readStatus;
}
