package cn.murky.tenant.system.biz.convert;

import cn.murky.tenant.system.api.domain.bo.SysUserBO;
import cn.murky.tenant.system.biz.domian.dto.SysUserFromDTO;
import cn.murky.tenant.system.biz.domian.entity.SysUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * MurkyUser实体转化接口
 *
 * @since hans
 */
@Mapper
public interface SysUserConvert {
    SysUserConvert INSTANCES = Mappers.getMapper(SysUserConvert.class);

    /**
     * 将SysUserFromDTO转为SysUser
     */
    @Mapping(target = "language", ignore = true)
    @Mapping(target = "updateUser", ignore = true)
    @Mapping(target = "updateTime", ignore = true)
    @Mapping(target = "createUser", ignore = true)
    @Mapping(target = "createTime", ignore = true)
    SysUser toEntity(SysUserFromDTO sysUserFromDTO);

    SysUserBO toBO(SysUser sysUser);
}
