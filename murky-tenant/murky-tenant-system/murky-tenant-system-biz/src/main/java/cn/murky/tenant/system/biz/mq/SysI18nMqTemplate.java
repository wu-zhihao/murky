package cn.murky.tenant.system.biz.mq;

import cn.murky.folkmq.constant.SysGroupConstant;
import cn.murky.tenant.system.biz.service.ISysI18nService;
import lombok.extern.slf4j.Slf4j;
import org.noear.redisx.RedisClient;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cloud.CloudEventHandler;
import org.noear.solon.cloud.annotation.CloudEvent;
import org.noear.solon.cloud.model.Event;

import static cn.murky.folkmq.constant.SysTopicConstant.SYS_I18N_TOPIC;

@Component
@Slf4j
@CloudEvent(topic = SYS_I18N_TOPIC, group = SysGroupConstant.DEFAULT)
public class SysI18nMqTemplate implements CloudEventHandler {
    @Inject
    private ISysI18nService iSysI18nService;

    @Override
    public boolean handle(Event event) throws Throwable {
        log.info("[I18nRedisMqTemplate] -> 消费国际化语言包清空缓存主题 -> topic:{}", event.topic());
        iSysI18nService.refreshLocal();
        return true;
    }
}
