package cn.murky.tenant.system.biz.service.impl;

import cn.murky.common.utils.CollectionUtils;
import cn.murky.tenant.core.utils.RedisUtils;
import cn.murky.tenant.system.biz.domian.entity.SysI18n;
import cn.murky.tenant.system.biz.mapper.SysI18nMapper;
import cn.murky.tenant.system.biz.service.ISysI18nService;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.redisx.plus.RedisHash;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

@Component
public class SysI18nServiceImpl extends ServiceImpl<SysI18nMapper, SysI18n> implements ISysI18nService {
    private static final String I18N_REDIS_CACHE_KEY = "admin:system:i18n:";
    private static final Cache<String, Map<String,String>> I18N_LOCAL_CACHE_KEY = Caffeine.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .maximumSize(Long.MAX_VALUE)
            .build();
    @Override
    public Map<String, String> language(List<String> i18nTag, String language) {
//        List<SysI18n> sysI18ns = mapper.selectByLanguageAndTag(i18nTag, laguage);
//        ONode node = new ONode();
//        for (SysI18n sysI18n : sysI18ns) {
//            node.set(sysI18n.getI18nKey(), sysI18n.getI18nValue());
//        }
//        return node.toObject(Map.class);
        HashMap<String, String> result = new HashMap<>();
        if(CollectionUtils.isNotEmpty(i18nTag)){
            for (String tag : i18nTag) {
                Map<String, String> data = language(tag, language);
                if(CollectionUtils.isNotEmpty(data)){
                    result.putAll(language(tag, language));
                }
            }
        }
        return result;
    }

    @Override
    public void refreshLocal() {
        I18N_LOCAL_CACHE_KEY.invalidateAll();
    }

    private Map<String, String> language(String i18nTag, String language) {
        String cacheKey = STR."\{i18nTag}:\{language}";
        // 查询本地缓存,本地缓存持续保存10分钟
        ConcurrentMap<String, Map<String, String>> map = I18N_LOCAL_CACHE_KEY.asMap();
        if (CollectionUtils.isNotEmpty(map.keySet()) && map.containsKey(cacheKey)) {
            return map.get(cacheKey);
        }
        // 本地缓存不存在查询redis缓存
        RedisHash redisHash = RedisUtils.getAdminClient().getHash(I18N_REDIS_CACHE_KEY + cacheKey);
        // 如果redis缓存不存在则查询数据库并存入缓存
        if (redisHash.isEmpty()) {
            Map<String, String> node = new HashMap();
            List<SysI18n> sysI18ns = mapper.selectByLanguageAndTag(i18nTag, language);
            for (SysI18n sysI18n : sysI18ns) {
                node.put(sysI18n.getI18nKey(), sysI18n.getI18nValue());
            }
            if(CollectionUtils.isEmpty(node)){
                return node;
            }
            // 更新redis缓存
            redisHash.putAll(node);
            // 更新本地缓存
            I18N_LOCAL_CACHE_KEY.put(cacheKey, node);
            return node;
        }
        // 更新本地缓存
        I18N_LOCAL_CACHE_KEY.put(cacheKey, redisHash);
        return redisHash;
    }
}
