package cn.murky.tenant.system.api;

import cn.murky.tenant.system.api.domain.UserProfile;
import cn.murky.tenant.system.api.domain.bo.TenantUserBO;
import cn.murky.tenant.system.api.domain.dto.ProfileFromDTO;

/**
 * 租户用户api
 */
public interface TenantUserApi {
    /**
     * 根据用户id查询用户信息
     * @param id 用户id
     */
    TenantUserBO getById(Long id);

    /**
     * 根据用户账号查询用户信息
     * @param account 账号
     */
    TenantUserBO getOneByAccount(String account);

    /**
     * 设置用户语言偏好
     */
    boolean setLanguage(String language);

    /**
     * 获取用户profile
     */
    UserProfile getProfile(Long userId);

    /**
     * 修改用户profile
     */
    boolean setProfile(ProfileFromDTO profileFromDTO);

    /**
     * 修改密码
     *  @param oldPassword 旧密码
     *  @param password 新密码
     *  @param surePassword 确定新密码
     * @return 修改状态
     */
    boolean setPassword(String oldPassword,String password,String surePassword);
}
