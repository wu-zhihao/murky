package cn.murky.tenant.system.api.domain.dto;


import cn.murky.tenant.system.api.enums.Sex;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

@Data
@Accessors(chain = true)
public class ProfileFromDTO {

    /**
     * 用户名
     */
    @NotEmpty
    private String userName;

    /**
     * 邮箱
     */
    @NotEmpty
    private String email;

    /**
     * 性别
     */
    @NotNull
    private Sex sex;
}
