package cn.murky.tenant.core;

import org.noear.redisx.RedisClient;

import java.util.Optional;

public interface EnvApi {
    /**
     * 根据租户获取指定redis客户端
     *
     * @param tenantId 租户id
     * @return redis客户端
     */
    Optional<RedisClient> getRedisClientByTenantId(Long tenantId);
}
