package cn.murky.tenant.core.config;


import cn.murky.tenant.core.utils.SecurityUtils;
import com.mybatisflex.core.table.DynamicSchemaProcessor;
import com.mybatisflex.core.table.TableManager;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Init;
import org.noear.solon.annotation.Inject;

@Configuration
public class FlexConfig {
    @Inject("${murky.tenant.schemaPrefix}")
    private String schemaPrefix;
    /**
     * 配置动态租户
     */
    @Init
    public void initSchemaProcessor(){
        TableManager.setDynamicSchemaProcessor((schema, table,optionType) -> {
           return schemaPrefix+ SecurityUtils.getTenantId();
        });
    }
}
