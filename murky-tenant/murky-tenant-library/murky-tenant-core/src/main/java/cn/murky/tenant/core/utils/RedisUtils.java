package cn.murky.tenant.core.utils;

import cn.murky.tenant.core.EnvApi;
import org.noear.redisx.RedisClient;
import org.noear.solon.Solon;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static cn.murky.tenant.core.constant.Constants.ADMIN_REDIS_CLIENT_BEAN_NAME;
import static cn.murky.tenant.core.constant.Constants.TENANT_ID_HEADER;

/**
 * 在租户端不同租户使用的redis服务可能不一致
 * 租户端所有租户的redisClient统一在此处获取
 */
public class RedisUtils {
    private static EnvApi envApi;

    static {
        Solon.context().getBeanAsync(EnvApi.class,bean->{
            envApi=bean;
        });
    }

    /**
     * 获取指定租户的redisClient
     * 如果该租户没有配置其他的redis客户端,则使用系统默认配置
     * @param tenantId 租户id
     * @return redisClient
     */
    public static RedisClient get(Long tenantId) {
       return envApi.getRedisClientByTenantId(tenantId).orElseGet(()->Solon.context().getBean(RedisClient.class));
    }

    /**
     * 获取当前租户的redisClient
     * 如果该租户没有配置其他的redis客户端,则使用系统默认配置
     * @return reidsClient
     */
    public static RedisClient get() {
        return get(SecurityUtils.getTenantId());
    }

    /**
     * 获取默认的redis客户端
     * @return redisClient
     */
    public static RedisClient getCommonClient() {
        return Solon.context().getBean(RedisClient.class);
    }

    /**
     * 获取管理端redis客户端连接
     * @return redisClient
     */
    public static RedisClient getAdminClient() {
        return Solon.context().getBean(ADMIN_REDIS_CLIENT_BEAN_NAME);
    }

}
