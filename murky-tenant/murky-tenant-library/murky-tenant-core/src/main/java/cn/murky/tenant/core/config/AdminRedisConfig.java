package cn.murky.tenant.core.config;


import org.noear.redisx.RedisClient;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import static cn.murky.tenant.core.constant.Constants.ADMIN_REDIS_CLIENT_BEAN_NAME;

@Configuration
public class AdminRedisConfig {

    @Bean(name = ADMIN_REDIS_CLIENT_BEAN_NAME)
    public RedisClient getRedisClient(@Inject("${murky.admin.redis}") RedisClient redisClient) {
        return redisClient;
    }
}
