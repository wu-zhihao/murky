package cn.murky.tenant.core.constant;

public class Constants {
    public static final String TENANT_ID_HEADER = "Tenant-Id";

    public static final String ADMIN_REDIS_CLIENT_BEAN_NAME = "adminRedisClient";

    public static final Long ZERO = 0L;


}
