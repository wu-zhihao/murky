# Java project for solon

| Version | Update Time | Status | Author | Description |
|---------|-------------|--------|--------|-------------|
|v2024-06-24 19:23:36|2024-06-24 19:23:36|auto|@Administrator|Created by smart-doc|



# default
## 
### 
**URL:** /captcha/get

**Type:** POST

**Author:** noear

**Content-Type:** application/x-www-form-urlencoded

**Description:** 

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X POST -i '/captcha/get' --data 'captchaType=""&captchaVerification=""&captchaFontType=""&point.secretKey=""&jigsawImageBase64=""&captchaFontSize=0&point.x=0&pointJson=""&point.y=0&ts=0&browserInfo=""&wordList="",""&originalImageBase64=""&result=true&token=""&secretKey=""&projectCode=""&clientUid=""&captchaOriginalPath=""&captchaId=""'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|repCode|string|No comments found.|-|
|repMsg|string|No comments found.|-|
|repData|object|No comments found.|-|

**Response-example:**
```json
{
  "repCode": "",
  "repMsg": "",
  "repData": {}
}
```

### 
**URL:** /captcha/check

**Type:** POST

**Author:** noear

**Content-Type:** application/x-www-form-urlencoded

**Description:** 

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X POST -i '/captcha/check' --data 'point.x=0&captchaOriginalPath=""&captchaFontType=""&result=true&clientUid=""&ts=0&captchaType=""&jigsawImageBase64=""&captchaFontSize=0&point.y=0&browserInfo=""&point.secretKey=""&originalImageBase64=""&projectCode=""&captchaVerification=""&wordList="",""&pointJson=""&captchaId=""&token=""&secretKey=""'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|repCode|string|No comments found.|-|
|repMsg|string|No comments found.|-|
|repData|object|No comments found.|-|

**Response-example:**
```json
{
  "repCode": "",
  "repMsg": "",
  "repData": {}
}
```

# 授权模块
## 安全控制器
### 
**URL:** /auth/captcha

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X POST -i '/auth/captcha' --data 'originalImageBase64=""&point.secretKey=""&ts=0&captchaId=""&wordList="",""&browserInfo=""&captchaFontType=""&captchaType=""&captchaFontSize=0&point.x=0&result=true&secretKey=""&point.y=0&captchaVerification=""&jigsawImageBase64=""&projectCode=""&pointJson=""&clientUid=""&token=""&captchaOriginalPath=""'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─any object|object|any object.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 获取图形验证码信息
**URL:** /auth/captcha

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取图形验证码信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X GET -i '/auth/captcha?captchaFontSize=0&x=0&y=0&wordList=,&result=true&ts=0&point.secretKey=&clientUid=&captchaOriginalPath=&projectCode=&secretKey=&jigsawImageBase64=&captchaVerification=&captchaType=&captchaFontType=&pointJson=&originalImageBase64=&captchaId=&point.y=0&token=&browserInfo=&point.x=0'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─any object|object|any object.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 登录
**URL:** /auth/login

**Type:** POST


**Content-Type:** application/json

**Description:** 登录

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─account|string|true|账号|-|
|└─password|string|true|密码|-|
|└─captchaVerification|string|false|图形验证码|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/auth/login' --data '{
  "account": "",
  "password": "",
  "captchaVerification": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─any object|object|any object.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 登出
**URL:** /auth/logout

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 登出

**Request-example:**
```bash
curl -X POST -i '/auth/logout'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 获取菜单
**URL:** /auth/menu

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取菜单

**Request-example:**
```bash
curl -X GET -i '/auth/menu'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─type|enum|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─name|string|菜单名称|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─auth|string|权限字符|-|
|└─path|string|路由地址|-|
|└─sort|int16|排序|-|
|└─parentId|int64|上级菜单id|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|
|└─children|array|下级菜单|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "label": "",
      "type": "DIRECTORY",
      "name": "",
      "openType": "CURRENT",
      "isCache": "ON",
      "isDisplay": "DISPLAY",
      "isOutside": "ON",
      "auth": "",
      "path": "",
      "sort": 0,
      "parentId": 0,
      "component": "",
      "icon": "",
      "query": "",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 获取用户信息
**URL:** /auth/info

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取用户信息

**Request-example:**
```bash
curl -X GET -i '/auth/info'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─userId|int64|用户id|-|
|└─userName|string|用户名|-|
|└─token|string|登录token|-|
|└─admin|boolean|是否是租户管理员|-|
|└─email|string|邮箱|-|
|└─language|string|语言偏好|-|
|└─fkDeptId|int64|所属部门id|-|
|└─dataScope|enum|所属部门的数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkRoleId|int64|角色ID集合|-|
|└─roleCode|string|角色code|-|
|└─permissions|array|权限码|-|
|└─tenantId|int64|租户id|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "userId": 0,
    "userName": "",
    "token": "",
    "admin": true,
    "email": "",
    "language": "",
    "fkDeptId": 0,
    "dataScope": "ALL",
    "fkRoleId": 0,
    "roleCode": "",
    "permissions": [
      ""
    ],
    "tenantId": 0
  }
}
```

## 个人信息控制器
### 修改语言偏好
**URL:** /profile/language/{language}

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 修改语言偏好

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|language|string|false|语言|-|

**Request-example:**
```bash
curl -X POST -i '/profile/language/{language}' --data '=6txwao'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 获取用户展示信息
**URL:** /profile/

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取用户展示信息

**Request-example:**
```bash
curl -X GET -i '/profile/'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─userName|string|用户名|-|
|└─email|string|邮箱|-|
|└─sex|enum|性别<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─roleName|string|所属角色|-|
|└─deptNameList|array|所属部门|-|
|└─createTime|string|创建日期|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "userName": "",
    "email": "",
    "sex": "MAN",
    "roleName": "",
    "deptNameList": [
      ""
    ],
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
  }
}
```

### 修改用户信息
**URL:** /profile/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改用户信息

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─userName|string|true|用户名|-|
|└─email|string|true|邮箱|-|
|└─sex|enum|true|性别<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/profile/' --data '{
  "userName": "",
  "email": "",
  "sex": "MAN"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改密码
**URL:** /profile/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改密码

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─oldPassword|string|true|旧密码|-|
|└─password|string|true|新密码|-|
|└─surePassword|string|true|确定新密码|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/profile/' --data '{
  "oldPassword": "",
  "password": "",
  "surePassword": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

# 系统管理模块
## 字典数据管理
### 获取指定字典数据
**URL:** /dictData/dict/{dictType}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取指定字典数据

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|dictType|string|false|字典类型|-|

**Request-example:**
```bash
curl -X GET -i '/dictData/dict/{dictType}?=b5ge9g'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─dictCode|int64|字典编码|-|
|└─dictSort|int16|字典排序|-|
|└─dictLabel|string|字典标签|-|
|└─dictValue|string|字典值|-|
|└─dictType|string|字典类型|-|
|└─status|enum|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─remark|string|备注|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "dictCode": 0,
      "dictSort": 0,
      "dictLabel": "",
      "dictValue": "",
      "dictType": "",
      "status": "NORMAL",
      "remark": ""
    }
  ]
}
```

## i18n管理
### i18n语言包
**URL:** /i18n/language

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** i18n语言包

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|i18nTag|array|true|i18n标签,[array of string]|-|
|language|string|true|语言|-|

**Request-example:**
```bash
curl -X GET -i '/i18n/language?language=&i18nTag=c80oxg&i18nTag=c80oxg'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─mapKey|string|A map key.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "mapKey1": "",
    "mapKey2": ""
  }
}
```

## 菜单管理
### 获取用户菜单
**URL:** /menu/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取用户菜单

**Request-example:**
```bash
curl -X GET -i '/menu/list'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─type|enum|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─name|string|菜单名称|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─auth|string|权限字符|-|
|└─path|string|路由地址|-|
|└─sort|int16|排序|-|
|└─parentId|int64|上级菜单id|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|
|└─children|array|下级菜单|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "label": "",
      "type": "DIRECTORY",
      "name": "",
      "openType": "CURRENT",
      "isCache": "ON",
      "isDisplay": "DISPLAY",
      "isOutside": "ON",
      "auth": "",
      "path": "",
      "sort": 0,
      "parentId": 0,
      "component": "",
      "icon": "",
      "query": "",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 菜单详情
**URL:** /menu/{menuId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 菜单详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|menuId|int64|false|菜单id|-|

**Request-example:**
```bash
curl -X GET -i '/menu/{menuId}?menuId=0&=yujpia'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─name|string|菜单名称|-|
|└─path|string|路由地址|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|权限字符|-|
|└─parentId|int64|上级菜单id|-|
|└─type|enum|菜单类型 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|排序|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "label": "",
    "name": "",
    "path": "",
    "openType": "CURRENT",
    "auth": "",
    "parentId": 0,
    "type": "DIRECTORY",
    "isCache": "ON",
    "isDisplay": "DISPLAY",
    "isOutside": "ON",
    "sort": 0,
    "component": "",
    "icon": "",
    "query": ""
  }
}
```

## 角色管理
### 角色列表分页查询
**URL:** /role/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 角色列表分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|roleName|string|false|角色名|-|
|roleCode|string|false|角色码|-|

**Request-example:**
```bash
curl -X GET -i '/role/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&roleName=&roleCode='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|主键|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─roleName|string|角色名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─roleCode|string|角色码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dataScope|enum|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─fkDeptId|int64|部门Id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─describe|string|描述|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "roleName": "",
        "roleCode": "",
        "dataScope": "ALL",
        "fkDeptId": 0,
        "describe": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 角色列表查询
**URL:** /role/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 角色列表查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|roleName|string|false|角色名|-|
|roleCode|string|false|角色码|-|

**Request-example:**
```bash
curl -X GET -i '/role/list?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&roleCode=&roleName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|主键|-|
|└─roleName|string|角色名|-|
|└─roleCode|string|角色码|-|
|└─dataScope|enum|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkDeptId|int64|部门Id|-|
|└─describe|string|描述|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "createUser": 0,
      "updateUser": 0,
      "id": 0,
      "roleName": "",
      "roleCode": "",
      "dataScope": "ALL",
      "fkDeptId": 0,
      "describe": ""
    }
  ]
}
```

### 角色详情
**URL:** /role/{roleId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 角色详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|roleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X GET -i '/role/{roleId}?roleId=0&=pzbsyp'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─id|int64|id|-|
|└─roleName|string|角色名|-|
|└─roleCode|string|角色码,全局唯一|-|
|└─describe|string|描述|-|
|└─dataScope|enum|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkMenuIds|array|菜单id集合|-|
|└─fkDeptIds|array|部门Id集合|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "id": 0,
    "roleName": "",
    "roleCode": "",
    "describe": "",
    "dataScope": "ALL",
    "fkMenuIds": [
      0
    ],
    "fkDeptIds": [
      0
    ]
  }
}
```

### 新增角色
**URL:** /role/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增角色

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|角色id|-|
|└─roleName|string|false|角色名|-|
|└─roleCode|string|false|角色码|-|
|└─describe|string|false|描述|-|
|└─dataScope|enum|false|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkMenuIds|array|false|所属菜单id|-|
|└─fkDeptIds|array|false|自定义权限部门Id|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/role/' --data '{
  "id": 0,
  "roleName": "",
  "roleCode": "",
  "describe": "",
  "dataScope": "ALL",
  "fkMenuIds": [
    0
  ],
  "fkDeptIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改角色
**URL:** /role/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改角色

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|角色id|-|
|└─roleName|string|false|角色名|-|
|└─roleCode|string|false|角色码|-|
|└─describe|string|false|描述|-|
|└─dataScope|enum|false|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkMenuIds|array|false|所属菜单id|-|
|└─fkDeptIds|array|false|自定义权限部门Id|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/role/' --data '{
  "id": 0,
  "roleName": "",
  "roleCode": "",
  "describe": "",
  "dataScope": "ALL",
  "fkMenuIds": [
    0
  ],
  "fkDeptIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除角色
**URL:** /role/{roleId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除角色

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|roleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X DELETE -i '/role/{roleId}?roleId=0&=6kkbtl'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 部门管理
### 获取部门树形菜单
**URL:** /dept/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取部门树形菜单

**Request-example:**
```bash
curl -X GET -i '/dept/list'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|部门id|-|
|└─deptName|string|部门名称|-|
|└─parentId|int64|上级部门id|-|
|└─sort|int32|排序|-|
|└─createTime|string|创建事件|-|
|└─children|array|子部门|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "deptName": "",
      "parentId": 0,
      "sort": 0,
      "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 获取部门信息详情
**URL:** /dept/{deptId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取部门信息详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|deptId|int64|false|部门id|-|

**Request-example:**
```bash
curl -X GET -i '/dept/{deptId}?deptId=0&=efi82z'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|+<br/>部门id|-|
|└─deptName|string|部门名称|-|
|└─parentId|int64|父级部门id|-|
|└─sort|int32|排序|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "deptName": "",
    "parentId": 0,
    "sort": 0
  }
}
```

### 拖动排序
**URL:** /dept/drop

**Type:** PUT


**Content-Type:** application/x-www-form-urlencoded

**Description:** 拖动排序

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|deptIds|array|false|部门id集合,[array of int64]|-|

**Request-example:**
```bash
curl -X PUT -i '/dept/drop' --data 'deptIds=67btmb&deptIds=67btmb'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 添加部门
**URL:** /dept/

**Type:** POST


**Content-Type:** application/json

**Description:** 添加部门

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|部门id|-|
|└─deptName|string|true|+部门名称|-|
|└─parentId|int64|false|父级部门id|-|
|└─sort|int32|false|排序|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/dept/' --data '{
  "id": 0,
  "deptName": "",
  "parentId": 0,
  "sort": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改部门
**URL:** /dept/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改部门

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|部门id|-|
|└─deptName|string|false|+部门名称|-|
|└─parentId|int64|false|父级部门id|-|
|└─sort|int32|false|排序|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/dept/' --data '{
  "id": 0,
  "deptName": "",
  "parentId": 0,
  "sort": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除部门
**URL:** /dept/{deptId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除部门

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|deptId|int64|false|部门id|-|

**Request-example:**
```bash
curl -X DELETE -i '/dept/{deptId}?deptId=0&=6ljjqw'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 用户管理
### 用户分页
**URL:** /user/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 用户分页

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|userName|string|false|用户名|-|
|email|string|false|邮箱|-|
|sex|enum|false|性别<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|fkDeptId|int64|false|部门id|-|

**Request-example:**
```bash
curl -X GET -i '/user/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&sex=MAN&fkDeptId=0&email=&userName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|用户id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─userName|string|用户名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─account|string|账号|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─password|string|密码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─sex|enum|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─email|string|邮箱|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─fkDeptId|string|部门id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─deptName|string|部门名称|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "id": 0,
        "userName": "",
        "account": "",
        "password": "",
        "sex": "MAN",
        "email": "",
        "fkDeptId": "",
        "deptName": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 用户详情
**URL:** /user/{userId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 用户详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|userId|int64|false|用户id|-|

**Request-example:**
```bash
curl -X GET -i '/user/{userId}?userId=0&=k622ep'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|用户id|-|
|└─userName|string|用户名|-|
|└─account|string|账号|-|
|└─password|string|密码|-|
|└─sex|enum|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─email|string|邮箱|-|
|└─fkDeptId|int64|部门id|-|
|└─language|string|语言|-|
|└─salt|string|密码加密盐值|-|
|└─fkRoleId|int64|角色Id|-|
|└─zoneOffset|string|时区|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "userName": "",
    "account": "",
    "password": "",
    "sex": "MAN",
    "email": "",
    "fkDeptId": 0,
    "language": "",
    "salt": "",
    "fkRoleId": 0,
    "zoneOffset": ""
  }
}
```

### 新增用户
**URL:** /user/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增用户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|用户id|-|
|└─userName|string|false|用户名|-|
|└─account|string|false|账号|-|
|└─sex|enum|false|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─email|string|false|邮箱<br/>Validation[Email() ]|-|
|└─fkDeptId|int64|false|部门id|-|
|└─fkRoleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/user/' --data '{
  "id": 0,
  "userName": "",
  "account": "",
  "sex": "MAN",
  "email": "",
  "fkDeptId": 0,
  "fkRoleId": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改用户
**URL:** /user/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改用户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|用户id|-|
|└─userName|string|false|用户名|-|
|└─account|string|false|账号|-|
|└─sex|enum|false|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─email|string|false|邮箱<br/>Validation[Email() ]|-|
|└─fkDeptId|int64|false|部门id|-|
|└─fkRoleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/user/' --data '{
  "id": 0,
  "userName": "",
  "account": "",
  "sex": "MAN",
  "email": "",
  "fkDeptId": 0,
  "fkRoleId": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 重置密码
**URL:** /user/restPassword

**Type:** PUT


**Content-Type:** application/json

**Description:** 重置密码

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|用户id|-|
|└─password|string|true|密码|-|
|└─confirmPassword|string|true|二次确定密码|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/user/restPassword' --data '{
  "id": 0,
  "password": "",
  "confirmPassword": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除用户
**URL:** /user/{userId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除用户

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|userId|int64|false|用户id|-|

**Request-example:**
```bash
curl -X DELETE -i '/user/{userId}?userId=0&=qf2esy'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```


