import { DashboardIcon } from 'tdesign-icons-vue-next';
import { shallowRef } from 'vue';

import Layout from '@/layouts/index.vue';
import i18n from '@/i18n';

export default [
  {
    path: '/dashboard',
    component: Layout,
    redirect: '/dashboard/index',
    name: 'dashboard',
    meta: {
      title: 'menu.index',
      icon: shallowRef(DashboardIcon),
      orderNo: 0,
    },
    children: [
      {
        path: 'index',
        name: 'Index',
        component: () => import('@/pages/dashboard/index/index.vue'),
        meta: {
          icon: shallowRef(DashboardIcon),
          title: 'menu.index',
        },
      },
      // {
      //   path: 'detail',
      //   name: 'DashboardDetail',
      //   component: () => import('@/pages/dashboard/detail/index.vue'),
      //   meta: {
      //     title: '统计报表',
      //   },
      // },
    ],
  },
];
