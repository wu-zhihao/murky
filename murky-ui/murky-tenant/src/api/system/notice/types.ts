import { BaseApiType, PageRequest } from '@/api/types';

/**
 * @param title 标题
 * @param operators 操作人
 * @param type 通知类型
 * @param target 通知目标
 * @param expire 到期时间
 */
export interface PageNoticeReq extends PageRequest {
  title?: string,
  operators?: string,
  type?: number,
  target?: number,
  expire?: Date,
}

/**
 * @param title 标题
 * @param type 通知类型
 * @param readStatus 读取状态 0:已读取 1:未读
 */
export interface UserNoticePageReq extends PageRequest {
  title?: string,
  type?: number,
  readStatus?: number,
}


/**
 * @param id 通知id
 * @param title 标题
 * @param type 通知类型
 * @param target 通知目标
 * @param content 内容
 * @param status 通知状态
 * @param remark 备注
 * @param userName 操作人
 * @param expire 到期时间
 * @param createTime 创建时间
 * @param readStatus 用户通知中心通知读取状态 （0已读 1未读）
 */
export interface PageNoticeRes {
  id: string,
  title: string,
  type: number,
  target: number,
  content: string,
  status: number,
  remark: string,
  userName: string,
  expire?: Date,
  createTime: Date,
  readStatus:number,
}

/**
 * @param id 通知id
 * @param title 标题
 * @param type 通知类型
 * @param target 通知目标
 * @param content 内容
 * @param status 通知状态
 * @param remark 备注
 * @param expire 到期时间
 */
export class Notice extends BaseApiType {
  id?: string;
  title: string;
  type: number;
  target: number;
  content: string;
  status: number;
  remark: string;
  expire?: Date;
}