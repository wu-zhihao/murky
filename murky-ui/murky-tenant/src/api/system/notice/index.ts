import { PageResponse } from "@/api/types";
import { Notice, PageNoticeReq, PageNoticeRes} from "./types";
import { request } from '@/utils/request';
import { Result } from "@/types/axios";

const Api = {
  notice: '/sysNotice',
  page: '/sysNotice/page',
};

/**
 * 获取通知分页列表
 * @returns Route
 */
export function noticePage(params: PageNoticeReq):Promise<Result<PageResponse<Array<PageNoticeRes>>>> {
  return request.get<PageResponse<Array<PageNoticeRes>>>({
    url: Api.page,
    params
  });
}

/**
 * 获取通知详情信息
 * @returns Notice
 */
export function noticeInfo(noticeId: string):Promise<Result<Notice>> {
  return request.get<Notice>({
    url: `${Api.notice}/${noticeId}`,
  });
}

/**
 * 修改
 */
export function updateNotice(data: Notice):Promise<Result> {
  return request.put({
    url: Api.notice,
    data
  });
}
/**
 * 新增
 */
export function addNotice(data: Notice):Promise<Result> {
  return request.post({
    url: Api.notice,
    data
  });
}
/**
 * 删除
 */
export function delNotice(ids: string | string[]):Promise<Result> {
  return request.delete({
    url: `/${Api.notice}/${ids}`,
  });
}