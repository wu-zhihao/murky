import { request } from '@/utils/request';

const Api = {
  language: '/i18n/language',
};

/**
 * 获取语言包
 * @returns Route
 */
export function getLanguage(i18nTag: string, language: string) {
  return request.get({
    url: Api.language,
    params: {
      i18nTag,
      language
    }
  });
}