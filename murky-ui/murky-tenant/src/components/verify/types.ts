export interface ImageSize {
    imgHeight: number,
    imgWidth: number,
    barHeight: number,
    barWidth: number
}

export interface Point {
    x: number,
    y: number,
    secretKey?:string
}