import { SocketD } from "@noear/socket.d";
import { Client } from "@noear/socket.d/transport/client/Client";

/**
 * 创建socketd连接
 * @param serverUrl 服务端地址含协议头例如:sd:ws://127.0.0.1:8602/?u=a&p=2
 * @returns Client对象
 */
export const createSocketd = async (serverUrl:string):Promise<Client>=>{
   return await SocketD.createClient(serverUrl);
}