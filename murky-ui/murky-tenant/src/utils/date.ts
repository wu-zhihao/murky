// 获取常用时间
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone' // dependent on utc plugin
import { useUserStore } from '@/store';
dayjs.extend(utc)
dayjs.extend(timezone)

export const LAST_7_DAYS = [
  dayjs().subtract(7, 'day').format('YYYY-MM-DD'),
  dayjs().subtract(1, 'day').format('YYYY-MM-DD'),
];

export const LAST_30_DAYS = [
  dayjs().subtract(30, 'day').format('YYYY-MM-DD'),
  dayjs().subtract(1, 'day').format('YYYY-MM-DD'),
];


/**
 * 计算当前设定的时区格式化时间
 * @param time 时间
 * @returns 格式化后的时间
 */
export const dateFormatByMomentTimezone = (time:string|Date) => {
  return formatByMomentTimezone(time,'YYYY-MM-DD HH:mm:ss')
}

/**
 * 计算当前设定的时区格式化时间
 * @param time 时间
 * @param formate 时间格式
 * @returns 格式化后的时间
 */
export const formatByMomentTimezone = (time:string|Date,formate:string) => {
  const { userInfo } = useUserStore();
// dayjs.tz.guess() // 当前时区
  const resDate = dayjs(time).utcOffset(userInfo.zoneOffset || dayjs.tz.guess()).format(formate);
  return resDate || '';
}