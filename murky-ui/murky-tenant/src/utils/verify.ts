import CryptoJS from 'crypto-js'


/**
 * @word 要加密的内容
 * @keyWord String  服务器随机返回的关键字
 **/
export const aesEncrypt = (word:string,keyWord="XwKsGlMcdPMEhR1B")=>{
  var key = CryptoJS.enc.Utf8.parse(keyWord);
  var srcs = CryptoJS.enc.Utf8.parse(word);
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
  return encrypted.toString();
}


export const resetSize = (vm:any) => {
  let img_width=0, img_height=0, bar_width=0, bar_height=0;	//图片的宽度、高度，移动条的宽度、高度
  // @ts-ignore
  var parentWidth:number = vm.$el.parentNode.offsetWidth || window.offsetWidth
  // @ts-ignore
  var parentHeight:number = vm.$el.parentNode.offsetHeight || window.offsetHeight
  if (vm.imgSize.width.indexOf('%') != -1) {
      img_width = vm.imgSize.width / 100 * parentWidth
  } else {
      img_width = Number(vm.imgSize.width.replace('px', ''))
  }

  if (vm.imgSize.height.indexOf('%') != -1) {
      img_height = vm.imgSize.height / 100 * parentHeight
  } else {
      img_height = Number(vm.imgSize.height.replace('px', ''))
  }

  if (vm.barSize.width.indexOf('%') != -1) {
      bar_width = vm.barSize.width / 100 * parentWidth
  } else {
      bar_width = Number(vm.barSize.width.replace('px', ''))
  }

  if (vm.barSize.height.indexOf('%') != -1) {
      bar_height = vm.barSize.height / 100 * parentHeight
  } else {
      bar_height = Number(vm.barSize.height.replace('px', ''))
  }

  return {imgWidth: img_width, imgHeight: img_height, barWidth: bar_width, barHeight: bar_height}
}

export const _code_chars = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
export const _code_color1 = ['#fffff0', '#f0ffff', '#f0fff0', '#fff0f0']
export const _code_color2 = ['#FF0033', '#006699', '#993366', '#FF9900', '#66CC66', '#FF33CC']