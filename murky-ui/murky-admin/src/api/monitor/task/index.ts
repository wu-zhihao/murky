import {PageResponse} from "@/api/types";
import {request} from '@/utils/request';
import {PageTask, Task, PageJobLog, JobLog} from "./types";

const Api = {
  getMonitor: "/monitor/job/page",
  baseTask: "/monitor/job",
  changeStatus: "monitor/job/changeStatus",
  runTask: "/monitor/job/run",
  getMonitorJobLogList: "/monitor/jobLog/page",
  monitorJobLog: "/monitor/jobLog",
  deletJobLog: "/monitor/jobLog",
  cleanJobLog: "/monitor/jobLog/clean"
};


/**
 * 任务分页列表
 */
export function getMonitorList(params?: PageTask) {
  return request.get<PageResponse<Array<Task>>>({
    url: Api.getMonitor,
    params
  });
}

/**
 * 定时任务详情
 * @param id 任务id
 */
export function taskInfo(id: string) {
  return request.get<Task>({
    url: `${Api.baseTask}/${id}`
  });
}

/**
 * 新增定时任务
 */
export function addTask(data: Task) {
  return request.post({
    url: Api.baseTask,
    data
  });
}

/**
 * 编辑定时任务
 */
export function editTask(data: Task) {
  return request.put({
    url: Api.baseTask,
    data
  });
}

/**
 * 修改定时任务状态
 */
export function changeStatus(data: Task) {
  return request.put({
    url: Api.changeStatus,
    data
  });
}

/**
 * 删除定时任务
 */
export function deletTask(id: string) {
  return request.delete({
    url: `${Api.baseTask}/${id}`
  });
}

/**
 * 立即执行定时任务一次
 */
export function runTask(data: Task) {
  return request.post({
    url: Api.runTask,
    data
  });
}

/**
 * 调度日志分页列表
 */
export function getMonitorJobLogList(params?: PageJobLog) {
  return request.get<PageResponse<Array<Task>>>({
    url: Api.getMonitorJobLogList,
    params
  });
}


/**
 * 根据调度编号获取详细信息
 */
export function monitorJobLog(jobLogId?: string | number) {
  return request.get<JobLog>({
    url: `${Api.monitorJobLog}/${jobLogId}`
  });
}


/**
 * 删除调度日志
 */
export function deletJobLog(jobLogIds: Array<string | number>) {
  return request.delete({
    url: `${Api.deletJobLog}/${jobLogIds}`
  });
}


/**
 * 清空调度日志
 */
export function cleanJobLog() {
  return request.delete({
    url: `${Api.cleanJobLog}`
  });
}














