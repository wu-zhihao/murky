import { BaseApiType, PageRequest } from "@/api/types";


/**
 * @param jobName 任务名
 * @param jobGroup 任务组
 * @param status 状态
 */
export interface PageTask extends PageRequest {
    jobName?: string,
    jobGroup?: number,
    status?: number
}

/**
 * @param id 任务id
 * @param jobName 任务名称
 * @param jobGroup 任务组名
 * @param invokeTarget 调用目标字符串
 * @param cronExpression 执行表达式
 * @param misfirePolicy 计划策略
 * @param concurrent 并发执行
 * @param status 任务状态
 */
export interface Task extends BaseApiType {
    taskInfoType?: boolean,
    id?: string,
    jobName: string,
    jobGroup: number,
    invokeTarget: string,
    cronExpression: string,
    misfirePolicy: number,
    concurrent: number,
    status: number
}


/**
 * @param jobName 任务名
 * @param jobGroup 任务组
 * @param status 状态
 * @param offsetDateTimes 时间范围
 */
export interface PageJobLog extends PageRequest {
  jobName?: string,
  jobGroup?: number,
  status?: number,
  offsetDateTimes?:[]
}


export interface JobLog extends BaseApiType {
  createUser?:number,
  updateUser?:number,
  id?:number,
  jobName?:string,
  jobGroup?:string,
  invokeTarget?:string,
  jobMessage?:string,
  status?:number
}
