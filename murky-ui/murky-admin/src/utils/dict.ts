
import {useDictStore} from '@/store'


const dictStore=useDictStore();


/**
 * 获取字典label
 * @param dictType 字典类型
 * @param dictValue 字典值
 * @returns 
 */
export const dicttoLabel= (dictType: string,dictValue:string|number) :string|void=>{
    dictStore.toLabel(dictType,dictValue).then(label=> {return label});
}