package cn.murky.admin.gen.config;


import lombok.Data;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

/**
 * 代码生成相关配置
 * 
 * @since ruoyi
 */
@Component
@Inject("gen")
@Data
public class GenConfig
{
    /** 作者 */
    private String author;

    /** 生成包路径 */
    private String packageName;

    /** 自动去除表前缀，默认是false */
    private Boolean autoRemovePre;

    /** 表前缀(类名不会包含表前缀) */
    private String tablePrefix;

}
