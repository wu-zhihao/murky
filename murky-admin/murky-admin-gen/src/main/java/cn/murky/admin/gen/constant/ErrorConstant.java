package cn.murky.admin.gen.constant;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstant {

    // 渲染模板失败
    public static final ErrorRecord RENDER_TEMPLATE_ERROR = new ErrorRecord(100, "渲染模板失败，表名：");

    // 同步数据失败，原表结构不存在
    public static final ErrorRecord SYNC_DATA_ERROR = new ErrorRecord(101, "同步数据失败，原表结构不存在");
    // 树编码字段不能为空
    public static final ErrorRecord TREE_CODE_NULL_ERROR  = new ErrorRecord(102, "树编码字段不能为空");
    // 树父编码字段不能为空
    public static final ErrorRecord TREE_PARENT_CODE_NULL_ERROR  = new ErrorRecord(103, "树父编码字段不能为空");
    // 树名称字段不能为空
    public static final ErrorRecord TREE_NAME_NULL_ERROR  = new ErrorRecord(104, "树名称字段不能为空");
    // 关联子表的表名不能为空
    public static final ErrorRecord SUB_TABLE_NULL_ERROR  = new ErrorRecord(105, "关联子表的表名不能为空");
    // 子表关联的外键名不能为空
    public static final ErrorRecord SUB_CHILD_NULL_ERROR  = new ErrorRecord(106, "子表关联的外键名不能为空");
}
