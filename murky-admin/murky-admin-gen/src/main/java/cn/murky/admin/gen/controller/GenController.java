package cn.murky.admin.gen.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.gen.domain.GenTable;
import cn.murky.admin.gen.domain.GenTableColumn;
import cn.murky.admin.gen.service.IGenTableColumnService;
import cn.murky.admin.gen.service.IGenTableService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.DownloadedFile;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代码生成 操作处理
 * 
 * @auth hans
 */
@Controller
@Valid
@Mapping("gen")
public class GenController extends BaseController<IGenTableService>
{

    @Inject
    private IGenTableColumnService genTableColumnService;

    /**
     * 查询代码生成列表
     * @param genTable GenTable
     */
//    @SaCheckPermission("tool:gen:list")
    @Mapping("/list")
    @Get
    public ApiResult<List<GenTable>> genList(GenTable genTable)
    {
        List<GenTable> list = baseService.selectGenTableList(genTable);
        return ApiResult.ok(list);
    }

    /**
     * 修改代码生成业务
     */
//    @SaCheckPermission("tool:gen:query")
    @Mapping(value = "/{tableId}")
    @Get
    public ApiResult<Map<String, Object>> getInfo(Long tableId)
    {
        GenTable table = baseService.selectGenTableById(tableId);
        List<GenTable> tables = baseService.selectGenTableAll();
        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(tableId);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("info", table);
        map.put("rows", list);
        map.put("tables", tables);
        return ApiResult.ok(map);
    }

    /**
     * 查询数据库列表
     */
//    @SaCheckPermission("tool:gen:list")
    @Mapping("/db/list")
    @Get
    public ApiResult dataList(GenTable genTable)
    {
        List<GenTable> list = baseService.selectDbTableList(genTable);
        return ApiResult.ok(list);
    }

    /**
     * 查询数据表字段列表
     */
    @Mapping(value = "/column/{tableId}")
    @Get
    public ApiResult<List<GenTableColumn>> columnList(Long tableId)
    {

        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(tableId);
        return ApiResult.ok(list);
    }

    /**
     * 导入表结构（保存）
     */
//    @SaCheckPermission("tool:gen:import")
//    @Log(title = "代码生成", businessType = BusinessType.IMPORT)
    @Mapping("/importTable")
    @Post
    public ApiResult importTableSave(String tables)
    {
        String[] tableNames = tables.split(",");
        // 查询表信息
        List<GenTable> tableList = baseService.selectDbTableListByNames(tableNames);
        baseService.importGenTable(tableList);
        return ApiResult.ok();
    }

    /**
     * 修改保存代码生成业务
     */
//    @SaCheckPermission("tool:gen:edit")
//    @Log(title = "代码生成", businessType = BusinessType.UPDATE)
    @Mapping
    @Put
    public ApiResult editSave(@Validated @Body GenTable genTable)
    {
        baseService.validateEdit(genTable);
        baseService.updateGenTable(genTable);
        return ApiResult.ok();
    }

    /**
     * 删除代码生成
     */
//    @SaCheckPermission("tool:gen:remove")
//    @Log(title = "代码生成", businessType = BusinessType.DELETE)
    @Mapping("/{tableIds}")
    @Delete
    public ApiResult remove(Long[] tableIds)
    {
        baseService.deleteGenTableByIds(tableIds);
        return ApiResult.ok();
    }

    /**
     * 预览代码
     * @param tableId 表格id
     */
//    @SaCheckPermission("tool:gen:preview")
    @Mapping("/preview/{tableId}")
    @Get
    public ApiResult<Map<String, String>> preview(Long tableId) throws IOException
    {
        Map<String, String> dataMap = baseService.previewCode(tableId);
        return ApiResult.ok(dataMap);
    }

    /**
     * 生成代码（下载方式）
     * @param tableName 表格名称
     */
    @SaCheckPermission("tool:gen:code")
//    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @Mapping("/download/{tableName}")
    @Get
    public DownloadedFile download(String tableName) throws IOException
    {
        byte[] data = baseService.downloadCode(tableName);
        return new DownloadedFile("application/octet-stream", data, "murky.zip");
    }

    /**
     * 生成代码（自定义路径）
     * @param tableName 表格名称
     */
//    @SaCheckPermission("tool:gen:code")
//    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @Mapping("/genCode/{tableName}")
    @Get
    public ApiResult genCode(String tableName)
    {
        baseService.generatorCode(tableName);
        return ApiResult.ok();
    }

    /**
     * 同步数据库
     * @param tableName 表格名称
     */
//    @SaCheckPermission("tool:gen:edit")
//    @Log(title = "代码生成", businessType = BusinessType.UPDATE)
    @Mapping("/synchDb/{tableName}")
    @Get
    public ApiResult synchDb(String tableName)
    {
        baseService.synchDb(tableName);
        return ApiResult.ok();
    }

    /**
     * 批量生成代码
     * @param tables 表格名称组
     */
//    @SaCheckPermission("tool:gen:code")
//    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @Mapping("/batchGenCode")
    @Get
    public DownloadedFile batchGenCode(String tables) throws IOException
    {
        String[] tableNames = tables.split(",");
        byte[] data = baseService.downloadCode(tableNames);
        return new DownloadedFile("application/octet-stream", data, "murky.zip");
    }

}
