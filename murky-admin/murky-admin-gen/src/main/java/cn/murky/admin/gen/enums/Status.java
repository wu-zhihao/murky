package cn.murky.admin.gen.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

/**
 * 代码生成模块通用状态枚举 0否 1是
 *
 * @since hans
 */
@AllArgsConstructor
@Getter
public enum Status {
    NO(0,"否"),
    YES(1,"是");
    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }

}
