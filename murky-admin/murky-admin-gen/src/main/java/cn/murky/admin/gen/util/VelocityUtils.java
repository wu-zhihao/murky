package cn.murky.admin.gen.util;

import cn.murky.admin.gen.constant.GenConstants;
import cn.murky.admin.gen.domain.GenTable;
import cn.murky.admin.gen.domain.GenTableColumn;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.json.JSONObject;
import org.noear.snack.ONode;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 模板工具类
 *
 * @auth ruoyi
 */
public class VelocityUtils {
    /**
     * 项目空间路径
     */
    private static final String PROJECT_PATH = "main/java";

    /**
     * mybatis空间路径
     */
    private static final String MYBATIS_PATH = "main/resources/mapper";

    /**
     * 默认上级菜单，系统工具
     */
    private static final String DEFAULT_PARENT_MENU_ID = "3";

    /**
     * 设置模板变量信息
     *
     * @return 模板列表
     */
    public static VelocityContext prepareContext(GenTable genTable) {
        String moduleName = genTable.getModuleName();
        String businessName = genTable.getBusinessName();
        String packageName = genTable.getPackageName();
        String tplCategory = genTable.getTplCategory();
        String functionName = genTable.getFunctionName();

        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("tplCategory", genTable.getTplCategory());
        velocityContext.put("tableName", genTable.getTableName());
        velocityContext.put("functionName", StringUtils.isNotEmpty(functionName) ? functionName : "【请填写功能名称】");
        velocityContext.put("ClassName", genTable.getClassName());
        velocityContext.put("className", StringUtils.uncapitalize(genTable.getClassName()));
        velocityContext.put("moduleName", genTable.getModuleName());
        velocityContext.put("BusinessName", StringUtils.capitalize(genTable.getBusinessName()));
        velocityContext.put("businessName", genTable.getBusinessName());
        velocityContext.put("basePackage", getPackagePrefix(packageName));
        velocityContext.put("packageName", packageName);
        velocityContext.put("author", genTable.getFunctionAuthor());
        velocityContext.put("datetime", OffsetDateTime.now());
        velocityContext.put("pkColumn", genTable.getPkColumn());
        velocityContext.put("importList", getImportList(genTable));
        velocityContext.put("permissionPrefix", getPermissionPrefix(moduleName, businessName));
        velocityContext.put("columns", genTable.getColumns());
        velocityContext.put("table", genTable);
        velocityContext.put("dicts", getDicts(genTable));
        setMenuVelocityContext(velocityContext, genTable);
        if (GenConstants.TPL_TREE.equals(tplCategory)) {
            setTreeVelocityContext(velocityContext, genTable);
        }
        if (GenConstants.TPL_SUB.equals(tplCategory)) {
            setSubVelocityContext(velocityContext, genTable);
        }
        return velocityContext;
    }

    public static void setMenuVelocityContext(VelocityContext context, GenTable genTable) {
        String options = genTable.getOptions();
        ONode oNode = ONode.loadStr(options);
        String parentMenuId = getParentMenuId(oNode);
        context.put("parentMenuId", parentMenuId);
    }

    public static void setTreeVelocityContext(VelocityContext context, GenTable genTable) {
        String options = genTable.getOptions();
        ONode oNode = ONode.loadStr(options);
        String treeCode = getTreecode(oNode);
        String treeParentCode = getTreeParentCode(oNode);
        String treeName = getTreeName(oNode);

        context.put("treeCode", treeCode);
        context.put("treeParentCode", treeParentCode);
        context.put("treeName", treeName);
        context.put("expandColumn", getExpandColumn(genTable));
        if (oNode.contains(GenConstants.TREE_PARENT_CODE)) {
            context.put("tree_parent_code", oNode.get(GenConstants.TREE_PARENT_CODE).getString());
        }
        if (oNode.contains(GenConstants.TREE_NAME)) {
            context.put("tree_name", oNode.get(GenConstants.TREE_NAME).getString());
        }
    }

    public static void setSubVelocityContext(VelocityContext context, GenTable genTable) {
        GenTable subTable = genTable.getSubTable();
        String subTableName = genTable.getSubTableName();
        String subTableFkName = genTable.getSubTableFkName();
        String subClassName = genTable.getSubTable().getClassName();
        String subTableFkClassName = StringUtils.lowerCase(subTableFkName);

        context.put("subTable", subTable);
        context.put("subTableName", subTableName);
        context.put("subTableFkName", subTableFkName);
        context.put("subTableFkClassName", subTableFkClassName);
        context.put("subTableFkclassName", StringUtils.uncapitalize(subTableFkClassName));
        context.put("subClassName", subClassName);
        context.put("subclassName", StringUtils.uncapitalize(subClassName));
        context.put("subImportList", getImportList(genTable.getSubTable()));
    }

    /**
     * 获取模板信息
     *
     * @return 模板列表
     */
    public static List<String> getTemplateList(String tplCategory) {
        List<String> templates = new ArrayList<String>();
        templates.add("vm/java/domain.java.vm");
        templates.add("vm/java/mapper.java.vm");
        templates.add("vm/java/service.java.vm");
        templates.add("vm/java/serviceImpl.java.vm");
        templates.add("vm/java/controller.java.vm");
        templates.add("vm/xml/mapper.xml.vm");
        templates.add("vm/sql/sql.vm");
        templates.add("vm/js/api.js.vm");
        if (GenConstants.TPL_CRUD.equals(tplCategory)) {
            templates.add("vm/vue/index.vue.vm");
        } else if (GenConstants.TPL_TREE.equals(tplCategory)) {
            templates.add("vm/vue/index-tree.vue.vm");
        } else if (GenConstants.TPL_SUB.equals(tplCategory)) {
            templates.add("vm/vue/index.vue.vm");
            templates.add("vm/java/sub-domain.java.vm");
        }
        return templates;
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, GenTable genTable) {
        // 文件名称
        String fileName = "";
        // 包路径
        String packageName = genTable.getPackageName();
        // 模块名
        String moduleName = genTable.getModuleName();
        // 大写类名
        String className = genTable.getClassName();
        // 业务名称
        String businessName = genTable.getBusinessName();

        String javaPath = PROJECT_PATH + "/" + StringUtils.replace(packageName, ".", "/");
        String mybatisPath = MYBATIS_PATH + "/" + moduleName;
        String vuePath = "vue";

        if (template.contains("domain.java.vm")) {
            fileName = STR."""
                    \{javaPath}/domain/\{className}.java
                 """;
        }
        if (template.contains("sub-domain.java.vm") && StringUtils.equals(GenConstants.TPL_SUB, genTable.getTplCategory())) {
            fileName = STR."""
                    \{javaPath}/domain/\{genTable.getSubTable().getClassName()}.java
                 """;
        } else if (template.contains("mapper.java.vm")) {
            fileName = STR."""
                    \{javaPath}/mapper/\{className}Mapper.java
                 """;
        } else if (template.contains("service.java.vm")) {
            fileName = STR."""
                    \{javaPath}/service/I\{className}Service.java
                 """;
        } else if (template.contains("serviceImpl.java.vm")) {
            fileName = STR."""
                    \{javaPath}/service/impl/\{className}ServiceImpl.java
                 """;
        } else if (template.contains("controller.java.vm")) {
            fileName = STR."""
                    \{javaPath}/controller/impl/\{className}Controller.java
                 """;
        }
//        else if (template.contains("mapper.xml.vm"))
//        {
//            fileName = STR."""
//                    \{javaPath}/controller/impl/\{className}Controller.java
//                 """;
//            fileName = StringUtils.format("{}/{}Mapper.xml", mybatisPath, className);
//        }
        else if (template.contains("sql.vm")) {
            fileName = businessName + "Menu.sql";
        } else if (template.contains("api.js.vm")) {
            fileName = STR."""
                    \{vuePath}/api/\{moduleName}/\{businessName}.js
                 """;
        } else if (template.contains("index.vue.vm")) {
            fileName = STR."""
                    \{vuePath}/views/\{moduleName}/\{businessName}/index.vue
                 """;
        } else if (template.contains("index-tree.vue.vm")) {
            fileName = STR."""
                    \{vuePath}/views/\{moduleName}/\{businessName}/index.vue
                 """;
        }
        return fileName;
    }

    /**
     * 获取包前缀
     *
     * @param packageName 包名称
     * @return 包前缀名称
     */
    public static String getPackagePrefix(String packageName) {
        int lastIndex = packageName.lastIndexOf(".");
        return StringUtils.substring(packageName, 0, lastIndex);
    }

    /**
     * 根据列类型获取导入包
     *
     * @param genTable 业务表对象
     * @return 返回需要导入的包列表
     */
    public static HashSet<String> getImportList(GenTable genTable) {
        List<GenTableColumn> columns = genTable.getColumns();
        GenTable subGenTable = genTable.getSubTable();
        HashSet<String> importList = new HashSet<String>();
        if (subGenTable != null) {
            importList.add("java.util.List");
        }
        for (GenTableColumn column : columns) {
            if (!column.isSuperColumn() && GenConstants.TYPE_DATE.equals(column.getJavaType())) {
                importList.add("java.util.Date");
                importList.add("com.fasterxml.jackson.annotation.JsonFormat");
            } else if (!column.isSuperColumn() && GenConstants.TYPE_BIGDECIMAL.equals(column.getJavaType())) {
                importList.add("java.math.BigDecimal");
            }
        }
        return importList;
    }

    /**
     * 根据列类型获取字典组
     *
     * @param genTable 业务表对象
     * @return 返回字典组
     */
    public static String getDicts(GenTable genTable) {
        List<GenTableColumn> columns = genTable.getColumns();
        Set<String> dicts = new HashSet<String>();
        addDicts(dicts, columns);
        if (null != genTable.getSubTable()) {
            List<GenTableColumn> subColumns = genTable.getSubTable().getColumns();
            addDicts(dicts, subColumns);
        }
        return StringUtils.join(dicts, ", ");
    }

    /**
     * 添加字典列表
     *
     * @param dicts   字典列表
     * @param columns 列集合
     */
    public static void addDicts(Set<String> dicts, List<GenTableColumn> columns) {
        for (GenTableColumn column : columns) {
            if (!column.isSuperColumn() && StringUtils.isNotEmpty(column.getDictType()) && StringUtils.equalsAny(
                    column.getHtmlType(),
                    new String[]{GenConstants.HTML_SELECT, GenConstants.HTML_RADIO, GenConstants.HTML_CHECKBOX})) {
                dicts.add("'" + column.getDictType() + "'");
            }
        }
    }

    /**
     * 获取权限前缀
     *
     * @param moduleName   模块名称
     * @param businessName 业务名称
     * @return 返回权限前缀
     */
    public static String getPermissionPrefix(String moduleName, String businessName) {
        return STR."""
                \{moduleName}:\{businessName}
                """;
    }

    /**
     * 获取上级菜单ID字段
     *
     * @param oNode 生成其他选项
     * @return 上级菜单ID字段
     */
    public static String getParentMenuId(ONode oNode) {
        if(!oNode.isNull() && oNode.contains(GenConstants.PARENT_MENU_ID)){
            return oNode.get(GenConstants.PARENT_MENU_ID).getString();
        }
        return DEFAULT_PARENT_MENU_ID;
    }

    /**
     * 获取树编码
     *
     * @param oNode 生成其他选项
     * @return 树编码
     */
    public static String getTreecode(ONode oNode) {
        if (oNode.contains(GenConstants.TREE_CODE)) {
            return StringUtils.lowerCase(oNode.get(GenConstants.TREE_CODE).getString());
        }
        return StringUtils.EMPTY;
    }

    /**
     * 获取树父编码
     *
     * @param oNode 生成其他选项
     * @return 树父编码
     */
    public static String getTreeParentCode(ONode oNode) {
        if (oNode.contains(GenConstants.TREE_PARENT_CODE)) {
            return StringUtils.lowerCase(oNode.get(GenConstants.TREE_PARENT_CODE).getString());
        }
        return StringUtils.EMPTY;
    }

    /**
     * 获取树名称
     *
     * @param oNode 生成其他选项
     * @return 树名称
     */
    public static String getTreeName(ONode oNode) {
        if (oNode.contains(GenConstants.TREE_NAME)) {
            return StringUtils.lowerCase(oNode.get(GenConstants.TREE_NAME).getString());
        }
        return StringUtils.EMPTY;
    }

    /**
     * 获取需要在哪一列上面显示展开按钮
     *
     * @param genTable 业务表对象
     * @return 展开按钮列序号
     */
    public static int getExpandColumn(GenTable genTable) {
        String options = genTable.getOptions();
        ONode oNode = ONode.loadStr(options);
        String treeName = oNode.get(GenConstants.TREE_NAME).getString();
        int num = 0;
        for (GenTableColumn column : genTable.getColumns()) {
            if (column.isList()) {
                num++;
                String columnName = column.getColumnName();
                if (columnName.equals(treeName)) {
                    break;
                }
            }
        }
        return num;
    }
}
