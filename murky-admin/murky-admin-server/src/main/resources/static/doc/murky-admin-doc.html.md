# Java project for solon

| Version | Update Time | Status | Author | Description |
|---------|-------------|--------|--------|-------------|
|v2024-06-24 19:23:36|2024-06-24 19:23:36|auto|@Administrator|Created by smart-doc|



# default
## 
### 
**URL:** /captcha/get

**Type:** POST

**Author:** noear

**Content-Type:** application/x-www-form-urlencoded

**Description:** 

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X POST -i '/captcha/get' --data 'token=""&browserInfo=""&originalImageBase64=""&point.secretKey=""&point.x=0&jigsawImageBase64=""&captchaVerification=""&captchaFontSize=0&clientUid=""&ts=0&captchaOriginalPath=""&captchaType=""&point.y=0&secretKey=""&wordList="",""&projectCode=""&captchaFontType=""&captchaId=""&result=true&pointJson=""'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|repCode|string|No comments found.|-|
|repMsg|string|No comments found.|-|
|repData|object|No comments found.|-|

**Response-example:**
```json
{
  "repCode": "",
  "repMsg": "",
  "repData": {}
}
```

### 
**URL:** /captcha/check

**Type:** POST

**Author:** noear

**Content-Type:** application/x-www-form-urlencoded

**Description:** 

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X POST -i '/captcha/check' --data 'browserInfo=""&captchaFontType=""&ts=0&captchaFontSize=0&token=""&captchaId=""&result=true&captchaOriginalPath=""&wordList="",""&point.x=0&projectCode=""&jigsawImageBase64=""&captchaType=""&point.secretKey=""&pointJson=""&secretKey=""&originalImageBase64=""&clientUid=""&point.y=0&captchaVerification=""'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|repCode|string|No comments found.|-|
|repMsg|string|No comments found.|-|
|repData|object|No comments found.|-|

**Response-example:**
```json
{
  "repCode": "",
  "repMsg": "",
  "repData": {}
}
```

# 授权模块
## 个人信息控制器
### 修改语言偏好
**URL:** /profile/language/{language}

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 修改语言偏好

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|language|string|false|语言|-|

**Request-example:**
```bash
curl -X POST -i '/profile/language/{language}' --data '=c57ftm'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 获取用户展示信息
**URL:** /profile/

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取用户展示信息

**Request-example:**
```bash
curl -X GET -i '/profile/'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─userName|string|用户名|-|
|└─email|string|邮箱|-|
|└─sex|enum|性别<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─roleName|string|所属角色|-|
|└─deptNameList|array|所属部门|-|
|└─createTime|string|创建日期|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "userName": "",
    "email": "",
    "sex": "MAN",
    "roleName": "",
    "deptNameList": [
      ""
    ],
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
  }
}
```

### 修改用户信息
**URL:** /profile/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改用户信息

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─userName|string|true|用户名|-|
|└─email|string|true|邮箱|-|
|└─sex|enum|true|性别<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/profile/' --data '{
  "userName": "",
  "email": "",
  "sex": "MAN"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改密码
**URL:** /profile/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改密码

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─oldPassword|string|true|旧密码|-|
|└─password|string|true|新密码|-|
|└─surePassword|string|true|确定新密码|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/profile/' --data '{
  "oldPassword": "",
  "password": "",
  "surePassword": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 安全控制器
### 
**URL:** /auth/captcha

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X POST -i '/auth/captcha' --data 'point.x=0&ts=0&result=true&projectCode=""&pointJson=""&captchaOriginalPath=""&captchaFontSize=0&secretKey=""&captchaVerification=""&captchaType=""&captchaId=""&wordList="",""&token=""&originalImageBase64=""&clientUid=""&point.secretKey=""&point.y=0&jigsawImageBase64=""&browserInfo=""&captchaFontType=""'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─any object|object|any object.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 获取图形验证码信息
**URL:** /auth/captcha

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取图形验证码信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|captchaId|string|false|验证码id(后台申请)|-|
|projectCode|string|false|No comments found.|-|
|captchaType|string|false|验证码类型:(clickWord,blockPuzzle)|-|
|captchaOriginalPath|string|false|No comments found.|-|
|captchaFontType|string|false|No comments found.|-|
|captchaFontSize|int32|false|No comments found.|-|
|secretKey|string|false|No comments found.|-|
|originalImageBase64|string|false|原生图片base64|-|
|point|object|false|滑块点选坐标|-|
|└─secretKey|string|false|No comments found.|-|
|└─x|int32|false|No comments found.|-|
|└─y|int32|false|No comments found.|-|
|jigsawImageBase64|string|false|滑块图片base64|-|
|wordList|array|false|点选文字|-|
|pointList|array|false|点选坐标|-|
|pointJson|string|false|点坐标(base64加密传输)|-|
|token|string|false|UUID(每次请求的验证码唯一标识)|-|
|result|boolean|false|校验结果|-|
|captchaVerification|string|false|后台二次校验参数|-|
|clientUid|string|false|客户端UI组件id,组件初始化时设置一次，UUID|-|
|ts|int64|false|客户端的请求时间，预留字段|-|
|browserInfo|string|false|客户端ip+userAgent|-|

**Request-example:**
```bash
curl -X GET -i '/auth/captcha?captchaFontSize=0&x=0&y=0&wordList=,&result=true&ts=0&point.x=0&point.y=0&browserInfo=&captchaFontType=&projectCode=&secretKey=&originalImageBase64=&jigsawImageBase64=&token=&point.secretKey=&pointJson=&captchaType=&captchaVerification=&captchaOriginalPath=&captchaId=&clientUid='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─any object|object|any object.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 登录
**URL:** /auth/login

**Type:** POST


**Content-Type:** application/json

**Description:** 登录

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─account|string|true|账号|-|
|└─password|string|true|密码|-|
|└─captchaVerification|string|false|图形验证码|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/auth/login' --data '{
  "account": "",
  "password": "",
  "captchaVerification": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─any object|object|any object.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 登出
**URL:** /auth/logout

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 登出

**Request-example:**
```bash
curl -X POST -i '/auth/logout'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 获取菜单
**URL:** /auth/menu

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取菜单

**Request-example:**
```bash
curl -X GET -i '/auth/menu'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─type|enum|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─name|string|菜单名称|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─auth|string|权限字符|-|
|└─path|string|路由地址|-|
|└─sort|int16|排序|-|
|└─parentId|int64|上级菜单id|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|
|└─children|array|下级菜单|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "label": "",
      "type": "DIRECTORY",
      "name": "",
      "openType": "CURRENT",
      "isCache": "ON",
      "isDisplay": "DISPLAY",
      "isOutside": "ON",
      "auth": "",
      "path": "",
      "sort": 0,
      "parentId": 0,
      "component": "",
      "icon": "",
      "query": "",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 获取用户信息
**URL:** /auth/info

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取用户信息

**Request-example:**
```bash
curl -X GET -i '/auth/info'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─userId|int64|用户id|-|
|└─userName|string|用户名|-|
|└─token|string|登录token|-|
|└─admin|boolean|是否是超级管理员|-|
|└─email|string|邮箱|-|
|└─language|string|语言偏好|-|
|└─zoneOffset|string|时区|-|
|└─fkDeptId|int64|所属部门id|-|
|└─dataScope|enum|所属部门的数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkRoleId|int64|角色ID集合|-|
|└─roleCode|string|角色code集合|-|
|└─permissions|array|权限码|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "userId": 0,
    "userName": "",
    "token": "",
    "admin": true,
    "email": "",
    "language": "",
    "zoneOffset": "",
    "fkDeptId": 0,
    "dataScope": "ALL",
    "fkRoleId": 0,
    "roleCode": "",
    "permissions": [
      ""
    ]
  }
}
```

# 系统管理模块
## 用户管理
### 用户分页
**URL:** /user/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 用户分页

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|userName|string|false|用户名|-|
|email|string|false|邮箱|-|
|sex|enum|false|性别<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|fkDeptId|int64|false|部门id|-|

**Request-example:**
```bash
curl -X GET -i '/user/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&sex=MAN&fkDeptId=0&email=&userName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|用户id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─userName|string|用户名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─account|string|账号|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─password|string|密码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─sex|enum|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─email|string|邮箱|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─fkDeptId|string|部门id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─deptName|string|部门名称|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "id": 0,
        "userName": "",
        "account": "",
        "password": "",
        "sex": "MAN",
        "email": "",
        "fkDeptId": "",
        "deptName": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 用户详情
**URL:** /user/{userId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 用户详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|userId|int64|false|用户id|-|

**Request-example:**
```bash
curl -X GET -i '/user/{userId}?userId=0&=jfmujv'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|用户id|-|
|└─userName|string|用户名|-|
|└─account|string|账号|-|
|└─password|string|密码|-|
|└─sex|enum|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─email|string|邮箱|-|
|└─fkDeptId|int64|部门id|-|
|└─language|string|语言|-|
|└─salt|string|密码加密盐值|-|
|└─fkRoleId|int64|角色Id|-|
|└─zoneOffset|string|时区|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "userName": "",
    "account": "",
    "password": "",
    "sex": "MAN",
    "email": "",
    "fkDeptId": 0,
    "language": "",
    "salt": "",
    "fkRoleId": 0,
    "zoneOffset": ""
  }
}
```

### 新增用户
**URL:** /user/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增用户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|用户id|-|
|└─userName|string|false|用户名|-|
|└─account|string|false|账号|-|
|└─sex|enum|false|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─email|string|false|邮箱<br/>Validation[Email() ]|-|
|└─fkDeptId|int64|false|部门id|-|
|└─fkRoleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/user/' --data '{
  "id": 0,
  "userName": "",
  "account": "",
  "sex": "MAN",
  "email": "",
  "fkDeptId": 0,
  "fkRoleId": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改用户
**URL:** /user/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改用户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|用户id|-|
|└─userName|string|false|用户名|-|
|└─account|string|false|账号|-|
|└─sex|enum|false|性别 0:男性 1:女性 2:其他<br/>[Enum: MAN(0,"男性")<br/>, WOMAN(1,"女性")<br/>, OTHER(2,"其他")<br/>]|-|
|└─email|string|false|邮箱<br/>Validation[Email() ]|-|
|└─fkDeptId|int64|false|部门id|-|
|└─fkRoleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/user/' --data '{
  "id": 0,
  "userName": "",
  "account": "",
  "sex": "MAN",
  "email": "",
  "fkDeptId": 0,
  "fkRoleId": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 重置密码
**URL:** /user/restPassword

**Type:** PUT


**Content-Type:** application/json

**Description:** 重置密码

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|用户id|-|
|└─password|string|true|密码|-|
|└─confirmPassword|string|true|二次确定密码|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/user/restPassword' --data '{
  "id": 0,
  "password": "",
  "confirmPassword": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除用户
**URL:** /user/{userId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除用户

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|userId|int64|false|用户id|-|

**Request-example:**
```bash
curl -X DELETE -i '/user/{userId}?userId=0&=py49c9'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 通知管理
### 获取通知公告分页列表
**URL:** /sysNotice/page

**Type:** GET

**Author:** hans

**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取通知公告分页列表

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|title|string|false|标题|-|
|operators|string|false|操作人员|-|
|type|enum|false|公告类型（0通知 1公告）<br/>[Enum: NOTICE(0,"通知")<br/>, PROCLAMATION(1,"公告")<br/>]|-|
|target|enum|false|通知目标（0admin 1tenant）<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|expire|string|false|到期时间|-|

**Request-example:**
```bash
curl -X GET -i '/sysNotice/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&type=NOTICE&target=ADMIN&expire=yyyy-MM-ddTHH:mm:ss.SSSXXX&title=&operators='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|菜单id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─title|string|标题|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─type|enum|公告类型（0通知 1公告）<br/>[Enum: NOTICE(0,"通知")<br/>, PROCLAMATION(1,"公告")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─target|enum|通知目标（0admin 1tenant）<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─content|string|内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─status|enum|通用状态（0正常 1停用）<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─remark|string|备注|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─userName|string|操作人员|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─expire|string|到期时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "id": 0,
        "title": "",
        "type": "NOTICE",
        "target": "ADMIN",
        "content": "",
        "status": "NORMAL",
        "remark": "",
        "userName": "",
        "expire": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 根据通知公告编号获取详细信息
**URL:** /sysNotice/{id}

**Type:** GET

**Author:** hans

**Content-Type:** application/x-www-form-urlencoded

**Description:** 根据通知公告编号获取详细信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|id|int64|false|No comments found.|-|

**Request-example:**
```bash
curl -X GET -i '/sysNotice/{id}?id=0&=rgp82e'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|菜单id|-|
|└─title|string|标题|-|
|└─type|enum|公告类型（0通知 1公告）<br/>[Enum: NOTICE(0,"通知")<br/>, PROCLAMATION(1,"公告")<br/>]|-|
|└─target|enum|通知目标（0admin 1tenant）<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─content|string|内容|-|
|└─status|enum|通用状态（0正常 1停用）<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─remark|string|备注|-|
|└─expire|string|到期时间|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "title": "",
    "type": "NOTICE",
    "target": "ADMIN",
    "content": "",
    "status": "NORMAL",
    "remark": "",
    "expire": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
  }
}
```

### 新增通知公告
**URL:** /sysNotice/

**Type:** POST

**Author:** hans

**Content-Type:** application/json

**Description:** 新增通知公告

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|菜单id|-|
|└─title|string|true|标题|-|
|└─type|enum|true|公告类型（0通知 1公告）<br/>[Enum: NOTICE(0,"通知")<br/>, PROCLAMATION(1,"公告")<br/>]|-|
|└─target|enum|true|通知目标（0admin 1tenant）<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─content|string|false|内容|-|
|└─status|enum|true|通用状态（0正常 1停用）<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─remark|string|false|备注|-|
|└─expire|string|false|到期时间|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/sysNotice/' --data '{
  "id": 0,
  "title": "",
  "type": "NOTICE",
  "target": "ADMIN",
  "content": "",
  "status": "NORMAL",
  "remark": "",
  "expire": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改通知公告
**URL:** /sysNotice/

**Type:** PUT

**Author:** hans

**Content-Type:** application/json

**Description:** 修改通知公告

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|菜单id|-|
|└─title|string|true|标题|-|
|└─type|enum|true|公告类型（0通知 1公告）<br/>[Enum: NOTICE(0,"通知")<br/>, PROCLAMATION(1,"公告")<br/>]|-|
|└─target|enum|true|通知目标（0admin 1tenant）<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─content|string|false|内容|-|
|└─status|enum|true|通用状态（0正常 1停用）<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─remark|string|false|备注|-|
|└─expire|string|false|到期时间|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/sysNotice/' --data '{
  "id": 0,
  "title": "",
  "type": "NOTICE",
  "target": "ADMIN",
  "content": "",
  "status": "NORMAL",
  "remark": "",
  "expire": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除通知公告
**URL:** /sysNotice/{ids}

**Type:** DELETE

**Author:** hans

**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除通知公告

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|ids|array|false|No comments found.,[array of int64]|-|

**Request-example:**
```bash
curl -X DELETE -i '/sysNotice/{ids}?ids=7876ob&ids=7876ob'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 字典数据管理
### 字典数据分页查询
**URL:** /dictData/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 字典数据分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|dictLabel|string|true|字典标签|-|
|dictType|string|true|字典类型|-|
|status|enum|false|字典状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|

**Request-example:**
```bash
curl -X GET -i '/dictData/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&status=NORMAL&dictType=&dictLabel='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictCode|int64|字典编码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictSort|int16|字典排序|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictLabel|string|字典标签|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictValue|string|字典值|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictType|string|字典类型|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─status|enum|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─remark|string|备注|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "dictCode": 0,
        "dictSort": 0,
        "dictLabel": "",
        "dictValue": "",
        "dictType": "",
        "status": "NORMAL",
        "remark": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 获取指定字典数据
**URL:** /dictData/dict/{dictType}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取指定字典数据

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|dictType|string|false|字典类型|-|

**Request-example:**
```bash
curl -X GET -i '/dictData/dict/{dictType}?=467r2a'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─dictCode|int64|字典编码|-|
|└─dictSort|int16|字典排序|-|
|└─dictLabel|string|字典标签|-|
|└─dictValue|string|字典值|-|
|└─dictType|string|字典类型|-|
|└─status|enum|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─remark|string|备注|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "createUser": 0,
      "updateUser": 0,
      "dictCode": 0,
      "dictSort": 0,
      "dictLabel": "",
      "dictValue": "",
      "dictType": "",
      "status": "NORMAL",
      "remark": ""
    }
  ]
}
```

### 字典数据详情信息
**URL:** /dictData/{dictCode}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 字典数据详情信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|dictCode|int64|false|字典id|-|

**Request-example:**
```bash
curl -X GET -i '/dictData/{dictCode}?dictCode=0&=eo8x75'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 新增字典数据
**URL:** /dictData/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增字典数据

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─dictCode|int64|false|字典编码|-|
|└─dictSort|int16|false|字典排序|-|
|└─dictType|string|false|字典类型|-|
|└─dictLabel|string|false|字典标签|-|
|└─dictValue|string|false|字典值|-|
|└─status|enum|false|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─editI18n|boolean|false|是否需要修改对应国际化语言|-|
|└─sysI18nFromDTO|object|false|字典国际化|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nKey|string|false|i18n_key编码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nTag|enum|false|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nInputs|array|false|i18n参数|-|
|└─remark|string|false|备注|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/dictData/' --data '{
  "dictCode": 0,
  "dictSort": 0,
  "dictType": "",
  "dictLabel": "",
  "dictValue": "",
  "status": "NORMAL",
  "editI18n": true,
  "sysI18nFromDTO": {
    "i18nKey": "",
    "i18nTag": "COMMON",
    "i18nInputs": [
      {
        "id": 0,
        "language": "",
        "i18nValue": ""
      }
    ]
  },
  "remark": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改字典数据
**URL:** /dictData/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改字典数据

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─dictCode|int64|true|字典编码|-|
|└─dictSort|int16|false|字典排序|-|
|└─dictType|string|false|字典类型|-|
|└─dictLabel|string|false|字典标签|-|
|└─dictValue|string|false|字典值|-|
|└─status|enum|false|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─editI18n|boolean|false|是否需要修改对应国际化语言|-|
|└─sysI18nFromDTO|object|false|字典国际化|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nKey|string|false|i18n_key编码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nTag|enum|false|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nInputs|array|false|i18n参数|-|
|└─remark|string|false|备注|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/dictData/' --data '{
  "dictCode": 0,
  "dictSort": 0,
  "dictType": "",
  "dictLabel": "",
  "dictValue": "",
  "status": "NORMAL",
  "editI18n": true,
  "sysI18nFromDTO": {
    "i18nKey": "",
    "i18nTag": "COMMON",
    "i18nInputs": [
      {
        "id": 0,
        "language": "",
        "i18nValue": ""
      }
    ]
  },
  "remark": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除字典数据
**URL:** /dictData/{dictDataId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除字典数据

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|dictDataId|int64|false|数据字典id|-|

**Request-example:**
```bash
curl -X DELETE -i '/dictData/{dictDataId}?dictDataId=0&=jjpvy1'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 字典类型管理
### 字典类型分页查询
**URL:** /dictType/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 字典类型分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|dictName|string|false|字典名称|-|
|dictType|string|false|字典类型|-|
|status|enum|false|字典状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|

**Request-example:**
```bash
curl -X GET -i '/dictType/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&status=NORMAL&dictType=&dictName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|主键|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictName|string|字典名称|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dictType|string|字典类型|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─status|enum|字典状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─remark|string|备注|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "dictName": "",
        "dictType": "",
        "status": "NORMAL",
        "remark": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 字典类型详情信息
**URL:** /dictType/{dictTypeId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 字典类型详情信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|dictTypeId|int64|false|字典类型id|-|

**Request-example:**
```bash
curl -X GET -i '/dictType/{dictTypeId}?dictTypeId=0&=obp2i8'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 新增字典类型
**URL:** /dictType/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增字典类型

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|主键|-|
|└─dictName|string|false|字典名称|-|
|└─dictType|string|false|字典类型|-|
|└─status|enum|false|字典状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─editI18n|boolean|false|是否需要修改对应国际化语言|-|
|└─sysI18nFromDTO|object|false|字典国际化|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nKey|string|false|i18n_key编码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nTag|enum|false|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nInputs|array|false|i18n参数|-|
|└─remark|string|false|备注|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/dictType/' --data '{
  "id": 0,
  "dictName": "",
  "dictType": "",
  "status": "NORMAL",
  "editI18n": true,
  "sysI18nFromDTO": {
    "i18nKey": "",
    "i18nTag": "COMMON",
    "i18nInputs": [
      {
        "id": 0,
        "language": "",
        "i18nValue": ""
      }
    ]
  },
  "remark": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改字典类型
**URL:** /dictType/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改字典类型

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|主键|-|
|└─dictName|string|false|字典名称|-|
|└─dictType|string|false|字典类型|-|
|└─status|enum|false|字典状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─editI18n|boolean|false|是否需要修改对应国际化语言|-|
|└─sysI18nFromDTO|object|false|字典国际化|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nKey|string|false|i18n_key编码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nTag|enum|false|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nInputs|array|false|i18n参数|-|
|└─remark|string|false|备注|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/dictType/' --data '{
  "id": 0,
  "dictName": "",
  "dictType": "",
  "status": "NORMAL",
  "editI18n": true,
  "sysI18nFromDTO": {
    "i18nKey": "",
    "i18nTag": "COMMON",
    "i18nInputs": [
      {
        "id": 0,
        "language": "",
        "i18nValue": ""
      }
    ]
  },
  "remark": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除字典类型
**URL:** /dictType/{dictTypeId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除字典类型

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|dictTypeId|int64|false|字典类型id|-|

**Request-example:**
```bash
curl -X DELETE -i '/dictType/{dictTypeId}?dictTypeId=0&=ssq3ew'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 刷新字典
**URL:** /dictType/refresh

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 刷新字典

**Request-example:**
```bash
curl -X POST -i '/dictType/refresh'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 菜单管理
### 获取用户全量菜单
**URL:** /menu/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取用户全量菜单

**Request-example:**
```bash
curl -X GET -i '/menu/list'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─type|enum|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─name|string|菜单名称|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─auth|string|权限字符|-|
|└─path|string|路由地址|-|
|└─sort|int16|排序|-|
|└─parentId|int64|上级菜单id|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|
|└─children|array|下级菜单|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "label": "",
      "type": "DIRECTORY",
      "name": "",
      "openType": "CURRENT",
      "isCache": "ON",
      "isDisplay": "DISPLAY",
      "isOutside": "ON",
      "auth": "",
      "path": "",
      "sort": 0,
      "parentId": 0,
      "component": "",
      "icon": "",
      "query": "",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 菜单详情
**URL:** /menu/{menuId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 菜单详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|menuId|int64|false|菜单id|-|

**Request-example:**
```bash
curl -X GET -i '/menu/{menuId}?menuId=0&=p0bt2a'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─name|string|菜单名称|-|
|└─path|string|路由地址|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|权限字符|-|
|└─parentId|int64|上级菜单id|-|
|└─type|enum|菜单类型 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|排序|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "label": "",
    "name": "",
    "path": "",
    "openType": "CURRENT",
    "auth": "",
    "parentId": 0,
    "type": "DIRECTORY",
    "isCache": "ON",
    "isDisplay": "DISPLAY",
    "isOutside": "ON",
    "sort": 0,
    "component": "",
    "icon": "",
    "query": ""
  }
}
```

### 新增菜单
**URL:** /menu/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增菜单

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|菜单id|-|
|└─label|string|false|菜单标题|-|
|└─name|string|false|菜单名称|-|
|└─path|string|false|路由地址|-|
|└─openType|enum|false|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|false|权限码|-|
|└─parentId|int64|false|上级菜单id|-|
|└─type|enum|false|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|false|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|false|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|false|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|false|排序|-|
|└─component|string|false|组件路径|-|
|└─icon|string|false|图标|-|
|└─query|string|false|路由参数|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/menu/' --data '{
  "id": 0,
  "label": "",
  "name": "",
  "path": "",
  "openType": "CURRENT",
  "auth": "",
  "parentId": 0,
  "type": "DIRECTORY",
  "isCache": "ON",
  "isDisplay": "DISPLAY",
  "isOutside": "ON",
  "sort": 0,
  "component": "",
  "icon": "",
  "query": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改菜单
**URL:** /menu/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改菜单

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|菜单id|-|
|└─label|string|false|菜单标题|-|
|└─name|string|false|菜单名称|-|
|└─path|string|false|路由地址|-|
|└─openType|enum|false|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|false|权限码|-|
|└─parentId|int64|false|上级菜单id|-|
|└─type|enum|false|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|false|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|false|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|false|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|false|排序|-|
|└─component|string|false|组件路径|-|
|└─icon|string|false|图标|-|
|└─query|string|false|路由参数|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/menu/' --data '{
  "id": 0,
  "label": "",
  "name": "",
  "path": "",
  "openType": "CURRENT",
  "auth": "",
  "parentId": 0,
  "type": "DIRECTORY",
  "isCache": "ON",
  "isDisplay": "DISPLAY",
  "isOutside": "ON",
  "sort": 0,
  "component": "",
  "icon": "",
  "query": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 菜单拖动
**URL:** /menu/drop

**Type:** PUT


**Content-Type:** application/json

**Description:** 菜单拖动

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─parentId|int64|false|父级菜单id|-|
|└─menuIds|array|true|菜单id集合|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/menu/drop' --data '{
  "parentId": 0,
  "menuIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除菜单
**URL:** /menu/{menuId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除菜单

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|menuId|int64|false|菜单id|-|

**Request-example:**
```bash
curl -X DELETE -i '/menu/{menuId}?menuId=0&=f5vw28'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 角色管理
### 角色列表分页查询
**URL:** /role/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 角色列表分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|roleName|string|false|角色名|-|
|roleCode|string|false|角色码|-|

**Request-example:**
```bash
curl -X GET -i '/role/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&roleName=&roleCode='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|主键|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─roleName|string|角色名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─roleCode|string|角色码|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dataScope|enum|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─fkDeptId|int64|部门Id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─describe|string|描述|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "roleName": "",
        "roleCode": "",
        "dataScope": "ALL",
        "fkDeptId": 0,
        "describe": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 角色列表查询
**URL:** /role/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 角色列表查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|roleName|string|false|角色名|-|
|roleCode|string|false|角色码|-|

**Request-example:**
```bash
curl -X GET -i '/role/list?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&roleCode=&roleName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|主键|-|
|└─roleName|string|角色名|-|
|└─roleCode|string|角色码|-|
|└─dataScope|enum|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkDeptId|int64|部门Id|-|
|└─describe|string|描述|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "createUser": 0,
      "updateUser": 0,
      "id": 0,
      "roleName": "",
      "roleCode": "",
      "dataScope": "ALL",
      "fkDeptId": 0,
      "describe": ""
    }
  ]
}
```

### 角色详情
**URL:** /role/{roleId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 角色详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|roleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X GET -i '/role/{roleId}?roleId=0&=1qi3nw'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─id|int64|id|-|
|└─roleName|string|角色名|-|
|└─roleCode|string|角色码,全局唯一|-|
|└─describe|string|描述|-|
|└─dataScope|enum|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkMenuIds|array|菜单id集合|-|
|└─fkDeptIds|array|部门Id集合|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "id": 0,
    "roleName": "",
    "roleCode": "",
    "describe": "",
    "dataScope": "ALL",
    "fkMenuIds": [
      0
    ],
    "fkDeptIds": [
      0
    ]
  }
}
```

### 新增角色
**URL:** /role/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增角色

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|角色id|-|
|└─roleName|string|false|角色名|-|
|└─roleCode|string|false|角色码|-|
|└─describe|string|false|描述|-|
|└─dataScope|enum|false|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkMenuIds|array|false|所属菜单id|-|
|└─fkDeptIds|array|false|自定义权限部门Id|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/role/' --data '{
  "id": 0,
  "roleName": "",
  "roleCode": "",
  "describe": "",
  "dataScope": "ALL",
  "fkMenuIds": [
    0
  ],
  "fkDeptIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改角色
**URL:** /role/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改角色

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|角色id|-|
|└─roleName|string|false|角色名|-|
|└─roleCode|string|false|角色码|-|
|└─describe|string|false|描述|-|
|└─dataScope|enum|false|数据权限<br/>[Enum: ALL(0,"全部数据权限")<br/>, CUSTOMIZE(1,"自定数据权限")<br/>, DEPARTMENT_BELOW(2,"本部门及以下数据权限")<br/>, DEPARTMENT(3,"本部门数据权限")<br/>, ONESELF(4,"仅本人")<br/>]|-|
|└─fkMenuIds|array|false|所属菜单id|-|
|└─fkDeptIds|array|false|自定义权限部门Id|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/role/' --data '{
  "id": 0,
  "roleName": "",
  "roleCode": "",
  "describe": "",
  "dataScope": "ALL",
  "fkMenuIds": [
    0
  ],
  "fkDeptIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除角色
**URL:** /role/{roleId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除角色

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|roleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X DELETE -i '/role/{roleId}?roleId=0&=36w3jc'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 部门管理
### 获取部门树形菜单
**URL:** /dept/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取部门树形菜单

**Request-example:**
```bash
curl -X GET -i '/dept/list'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|部门id|-|
|└─deptName|string|部门名称|-|
|└─parentId|int64|上级部门id|-|
|└─sort|int32|排序|-|
|└─createTime|string|创建事件|-|
|└─children|array|子部门|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "deptName": "",
      "parentId": 0,
      "sort": 0,
      "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 获取部门信息详情
**URL:** /dept/{deptId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取部门信息详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|deptId|int64|true|部门id|-|

**Request-example:**
```bash
curl -X GET -i '/dept/{deptId}?deptId=0'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|部门id|-|
|└─deptName|string|部门名称|-|
|└─parentId|int64|父级部门id|-|
|└─sort|int32|排序|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "deptName": "",
    "parentId": 0,
    "sort": 0
  }
}
```

### 拖动排序
**URL:** /dept/drop

**Type:** PUT


**Content-Type:** application/x-www-form-urlencoded

**Description:** 拖动排序

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|deptIds|array|false|部门id集合,[array of int64]|-|

**Request-example:**
```bash
curl -X PUT -i '/dept/drop' --data 'deptIds=sa6hg4&deptIds=sa6hg4'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 添加部门
**URL:** /dept/

**Type:** POST


**Content-Type:** application/json

**Description:** 添加部门

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|部门id|-|
|└─deptName|string|true|部门名称|-|
|└─parentId|int64|false|父级部门id|-|
|└─sort|int32|false|排序|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/dept/' --data '{
  "id": 0,
  "deptName": "",
  "parentId": 0,
  "sort": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改部门
**URL:** /dept/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改部门

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|部门id|-|
|└─deptName|string|false|部门名称|-|
|└─parentId|int64|false|父级部门id|-|
|└─sort|int32|false|排序|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/dept/' --data '{
  "id": 0,
  "deptName": "",
  "parentId": 0,
  "sort": 0
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除部门
**URL:** /dept/{deptId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除部门

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|deptId|int64|false|部门id|-|

**Request-example:**
```bash
curl -X DELETE -i '/dept/{deptId}?deptId=0&=nv6gq4'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 系统配置管理
### 系统配置分页查询
**URL:** /systemParameter/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 系统配置分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|key|string|false|配置|-|

**Request-example:**
```bash
curl -X GET -i '/systemParameter/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&key='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─key|string|参数key|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─value|string|参数值|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─describe|string|描述|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "key": "",
        "value": "",
        "describe": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 系统配置详情
**URL:** /systemParameter/{id}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 系统配置详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|id|int64|false|配置id|-|

**Request-example:**
```bash
curl -X GET -i '/systemParameter/{id}?id=0&=jfhk0n'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|id|-|
|└─key|string|参数key|-|
|└─value|string|参数值|-|
|└─describe|string|描述|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "key": "",
    "value": "",
    "describe": ""
  }
}
```

### 刷新系统配置缓存
**URL:** /systemParameter/refresh

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 刷新系统配置缓存

**Request-example:**
```bash
curl -X POST -i '/systemParameter/refresh'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 新增系统配置
**URL:** /systemParameter/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增系统配置

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|id|-|
|└─key|string|false|参数key|-|
|└─value|string|false|参数值|-|
|└─describe|string|false|描述|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/systemParameter/' --data '{
  "id": 0,
  "key": "",
  "value": "",
  "describe": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改系统配置
**URL:** /systemParameter/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改系统配置

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|id|-|
|└─key|string|false|参数key|-|
|└─value|string|false|参数值|-|
|└─describe|string|false|描述|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/systemParameter/' --data '{
  "id": 0,
  "key": "",
  "value": "",
  "describe": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除系统配置
**URL:** /systemParameter/{id}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除系统配置

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|id|int64|false|配置id|-|

**Request-example:**
```bash
curl -X DELETE -i '/systemParameter/{id}?id=0&=w07jvw'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 国际化语言包管理
### 分页查询
**URL:** /i18n/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|i18nTag|enum|true|i18n标签(字典：i18n:tag)<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|i18nValue|string|false|i18nValue|-|
|i18nKey|string|true|i18nKey|-|

**Request-example:**
```bash
curl -X GET -i '/i18n/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&i18nTag=COMMON&i18nKey=&i18nValue='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─mapKey|object|A map key.|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "mapKey": {}
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 详情
**URL:** /i18n/info

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|i18nTag|enum|false|i18n标签(字典：i18n:tag)<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|i18nValue|string|false|i18nValue|-|
|i18nKey|string|true|i18nKey|-|

**Request-example:**
```bash
curl -X GET -i '/i18n/info?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&i18nTag=COMMON&i18nKey=&i18nValue='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─id|int64|id|-|
|└─i18nKey|string|i18n_key编码|-|
|└─i18nTag|enum|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|└─i18nInputs|array|i18n参数|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|i18n字典|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─language|string|i18n字典|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nValue|string|i18n值|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "id": 0,
    "i18nKey": "",
    "i18nTag": "COMMON",
    "i18nInputs": [
      {
        "id": 0,
        "language": "",
        "i18nValue": ""
      }
    ]
  }
}
```

### 刷新缓存
**URL:** /i18n/refresh

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 刷新缓存

**Request-example:**
```bash
curl -X POST -i '/i18n/refresh'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─mapKey|string|A map key.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "mapKey1": "",
    "mapKey2": ""
  }
}
```

### 获取指定语言包
**URL:** /i18n/language

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取指定语言包

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|i18nTag|array|true|语言包标签,[array of string]|-|
|language|string|true|语言|-|

**Request-example:**
```bash
curl -X GET -i '/i18n/language?language=&i18nTag=0ndi0a&i18nTag=0ndi0a'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─mapKey|string|A map key.|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "mapKey1": "",
    "mapKey2": ""
  }
}
```

### 新增国际化语言数据
**URL:** /i18n/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增国际化语言数据

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─i18nKey|string|false|i18n_key编码|-|
|└─i18nTag|enum|false|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|└─i18nInputs|array|false|i18n参数|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|false|id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─language|string|false|i18n字典|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nValue|string|false|i18n值|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/i18n/' --data '{
  "i18nKey": "",
  "i18nTag": "COMMON",
  "i18nInputs": [
    {
      "id": 0,
      "language": "",
      "i18nValue": ""
    }
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改国际化语言数据
**URL:** /i18n/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改国际化语言数据

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─i18nKey|string|false|i18n_key编码|-|
|└─i18nTag|enum|false|i18n_tag标签<br/>[Enum: COMMON("common","公共")<br/>, ADMIN("admin","管理端")<br/>, TENANT("tenant","租户端")<br/>]|-|
|└─i18nInputs|array|false|i18n参数|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|false|id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─language|string|false|i18n字典|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─i18nValue|string|false|i18n值|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/i18n/' --data '{
  "i18nKey": "",
  "i18nTag": "COMMON",
  "i18nInputs": [
    {
      "id": 0,
      "language": "",
      "i18nValue": ""
    }
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除国际化语言数据
**URL:** /i18n/{i18nKey}/{i18nTag}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除国际化语言数据

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|i18nKey|string|false|对应语言key的id|-|
|i18nTag|enum|false|对应语言key的TAG<br/>[Enum: COMMON(common,公共)<br/>, ADMIN(admin,管理端)<br/>, TENANT(tenant,租户端)<br/>]|-|

**Request-example:**
```bash
curl -X DELETE -i '/i18n/{i18nKey}/{i18nTag}?i18nTag=COMMON&=b4t9gm'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

# 租户管理模块
## 租户菜单管理
### 租户菜单列表查询
**URL:** /tenantMenu/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户菜单列表查询

**Request-example:**
```bash
curl -X GET -i '/tenantMenu/list'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─id|int64|租户菜单id|-|
|└─label|string|菜单标题|-|
|└─type|enum|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─name|string|菜单名称|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─auth|string|权限字符|-|
|└─path|string|路由地址|-|
|└─sort|int16|排序|-|
|└─parentId|int64|上级租户菜单id|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|
|└─children|array|下级菜单|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "id": 0,
      "label": "",
      "type": "DIRECTORY",
      "name": "",
      "openType": "CURRENT",
      "isCache": "ON",
      "isDisplay": "DISPLAY",
      "isOutside": "ON",
      "auth": "",
      "path": "",
      "sort": 0,
      "parentId": 0,
      "component": "",
      "icon": "",
      "query": "",
      "children": [
        {
          "$ref": ".."
        }
      ]
    }
  ]
}
```

### 租户菜单详情
**URL:** /tenantMenu/{tenantMenuId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户菜单详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|tenantMenuId|int64|false|tenantMenuId|-|

**Request-example:**
```bash
curl -X GET -i '/tenantMenu/{tenantMenuId}?tenantMenuId=0&=zb5lnw'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|菜单id|-|
|└─label|string|菜单标题|-|
|└─name|string|菜单名称|-|
|└─path|string|路由地址|-|
|└─openType|enum|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|权限字符|-|
|└─parentId|int64|上级菜单id|-|
|└─type|enum|菜单类型 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|排序|-|
|└─component|string|组件路径|-|
|└─icon|string|图标|-|
|└─query|string|路由参数|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "label": "",
    "name": "",
    "path": "",
    "openType": "CURRENT",
    "auth": "",
    "parentId": 0,
    "type": "DIRECTORY",
    "isCache": "ON",
    "isDisplay": "DISPLAY",
    "isOutside": "ON",
    "sort": 0,
    "component": "",
    "icon": "",
    "query": ""
  }
}
```

### 新增菜单
**URL:** /tenantMenu/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增菜单

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|租户菜单id|-|
|└─label|string|false|菜单标题|-|
|└─name|string|false|菜单名称|-|
|└─path|string|false|路由地址|-|
|└─openType|enum|false|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|false|权限码|-|
|└─parentId|int64|false|上级菜单id|-|
|└─type|enum|false|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|false|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|false|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|false|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|false|排序|-|
|└─component|string|false|组件路径|-|
|└─icon|string|false|图标|-|
|└─query|string|false|路由参数|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/tenantMenu/' --data '{
  "id": 0,
  "label": "",
  "name": "",
  "path": "",
  "openType": "CURRENT",
  "auth": "",
  "parentId": 0,
  "type": "DIRECTORY",
  "isCache": "ON",
  "isDisplay": "DISPLAY",
  "isOutside": "ON",
  "sort": 0,
  "component": "",
  "icon": "",
  "query": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改菜单
**URL:** /tenantMenu/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改菜单

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|租户菜单id|-|
|└─label|string|false|菜单标题|-|
|└─name|string|false|菜单名称|-|
|└─path|string|false|路由地址|-|
|└─openType|enum|false|打开方式 1:当前窗口  2:新窗口<br/>[Enum: CURRENT(1,"当前窗口")<br/>, NEW(2,"新窗口")<br/>]|-|
|└─auth|string|false|权限码|-|
|└─parentId|int64|false|上级菜单id|-|
|└─type|enum|false|菜单类型 0:目录 1:侧边菜单 2:按钮<br/>[Enum: DIRECTORY(0,"目录")<br/>, MENU(1,"菜单")<br/>, BUTTON(2,"按钮")<br/>]|-|
|└─isCache|enum|false|是否开启缓存 0:关闭  1:开启<br/>[Enum: ON(0,"开启缓存")<br/>, OFF(1,"关闭缓存")<br/>]|-|
|└─isDisplay|enum|false|是否显示在菜单  0:显示  1:隐藏<br/>[Enum: DISPLAY(0,"显示")<br/>, HIDDEN(1,"隐藏")<br/>]|-|
|└─isOutside|enum|false|是否使用外链  0:否  1:是<br/>[Enum: ON(0,"不使用外链")<br/>, YES(1,"使用外链")<br/>]|-|
|└─sort|int16|false|排序|-|
|└─component|string|false|组件路径|-|
|└─icon|string|false|图标|-|
|└─query|string|false|路由参数|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/tenantMenu/' --data '{
  "id": 0,
  "label": "",
  "name": "",
  "path": "",
  "openType": "CURRENT",
  "auth": "",
  "parentId": 0,
  "type": "DIRECTORY",
  "isCache": "ON",
  "isDisplay": "DISPLAY",
  "isOutside": "ON",
  "sort": 0,
  "component": "",
  "icon": "",
  "query": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 菜单拖动
**URL:** /tenantMenu/drop

**Type:** PUT


**Content-Type:** application/json

**Description:** 菜单拖动

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─parentId|int64|false|租户父级菜单id|-|
|└─menuIds|array|true|菜单id集合,按顺序排列|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/tenantMenu/drop' --data '{
  "parentId": 0,
  "menuIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除租户菜单
**URL:** /tenantMenu/{tenantMenuId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除租户菜单

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|tenantMenuId|int64|false|tenantMenuId|-|

**Request-example:**
```bash
curl -X DELETE -i '/tenantMenu/{tenantMenuId}?tenantMenuId=0&=h99tby'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 租户管理
### 租户列表分页查询
**URL:** /tenant/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户列表分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|tenantName|string|false|租户名称|-|

**Request-example:**
```bash
curl -X GET -i '/tenant/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&tenantName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|租户id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─fkGroupId|int64|权限组id|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─groupName|string|权限组名称|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─tenantName|string|租户名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─adminUser|int64|租户管理员|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─expires|string|到期时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─describe|string|描述|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─status|enum|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "id": 0,
        "fkGroupId": 0,
        "groupName": "",
        "tenantName": "",
        "adminUser": 0,
        "expires": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "describe": "",
        "status": "NORMAL",
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 租户详情
**URL:** /tenant/{tenantId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|tenantId|int64|false|租户id|-|

**Request-example:**
```bash
curl -X GET -i '/tenant/{tenantId}?tenantId=0&=tprsdp'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─id|int64|租户id|-|
|└─fkGroupId|int64|权限组id|-|
|└─groupName|string|权限组名称|-|
|└─tenantName|string|租户名|-|
|└─adminUser|int64|租户管理员|-|
|└─expires|string|到期时间|-|
|└─describe|string|描述|-|
|└─status|enum|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|
|└─createTime|string|创建时间|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "id": 0,
    "fkGroupId": 0,
    "groupName": "",
    "tenantName": "",
    "adminUser": 0,
    "expires": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "describe": "",
    "status": "NORMAL",
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
  }
}
```

### 新增租户
**URL:** /tenant/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增租户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|租户id|-|
|└─fkGroupId|int64|true|权限组id|-|
|└─tenantName|string|true|租户名|-|
|└─expires|string|true|到期时间|-|
|└─describe|string|false|描述|-|
|└─account|string|true|账号|-|
|└─password|string|true|密码|-|
|└─confirmPassword|string|true|二次确认密码|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/tenant/' --data '{
  "id": 0,
  "fkGroupId": 0,
  "tenantName": "",
  "expires": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "describe": "",
  "account": "",
  "password": "",
  "confirmPassword": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|boolean|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": true
}
```

### 修改租户
**URL:** /tenant/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改租户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|租户id|-|
|└─fkGroupId|int64|false|权限组id|-|
|└─tenantName|string|false|租户名|-|
|└─expires|string|false|到期时间|-|
|└─describe|string|false|描述|-|
|└─account|string|false|账号|-|
|└─password|string|false|密码|-|
|└─confirmPassword|string|false|二次确认密码|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/tenant/' --data '{
  "id": 0,
  "fkGroupId": 0,
  "tenantName": "",
  "expires": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "describe": "",
  "account": "",
  "password": "",
  "confirmPassword": ""
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|boolean|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": true
}
```

### 停用/启用租户
**URL:** /tenant/{tenantId}

**Type:** POST


**Content-Type:** application/x-www-form-urlencoded

**Description:** 停用/启用租户

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|tenantId|int64|false|tenantId|-|

**Request-example:**
```bash
curl -X POST -i '/tenant/{tenantId}' --data '=cx2fxr'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|租户id|-|
|└─fkGroupId|int64|权限组id|-|
|└─tenantName|string|租户名|-|
|└─adminUser|int64|租户管理员|-|
|└─expires|string|到期时间|-|
|└─describe|string|描述|-|
|└─status|enum|状态<br/>[Enum: NORMAL(0, "正常")<br/>, STOP(1, "停用")<br/>]|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "fkGroupId": 0,
    "tenantName": "",
    "adminUser": 0,
    "expires": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "describe": "",
    "status": "NORMAL"
  }
}
```

## 租户权限组管理
### 租户权限组列表分页查询
**URL:** /permissionGroup/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户权限组列表分页查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|groupName|string|false|权限组名|-|

**Request-example:**
```bash
curl -X GET -i '/permissionGroup/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&groupName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|主键|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─groupName|string|权限组名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─describe|string|描述|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "groupName": "",
        "describe": ""
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 租户权限组列表查询
**URL:** /permissionGroup/list

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户权限组列表查询

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|groupName|string|false|权限组名|-|

**Request-example:**
```bash
curl -X GET -i '/permissionGroup/list?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&groupName='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|array|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|主键|-|
|└─groupName|string|权限组名|-|
|└─describe|string|描述|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": [
    {
      "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
      "createUser": 0,
      "updateUser": 0,
      "id": 0,
      "groupName": "",
      "describe": ""
    }
  ]
}
```

### 租户权限组详情
**URL:** /permissionGroup/{roleId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 租户权限组详情

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|roleId|int64|false|角色id|-|

**Request-example:**
```bash
curl -X GET -i '/permissionGroup/{roleId}?roleId=0&=bu4nr3'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─groupId|int64|主键|-|
|└─groupName|string|角色名|-|
|└─describe|string|描述|-|
|└─tenantMenuIds|array|菜单id集合|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "groupId": 0,
    "groupName": "",
    "describe": "",
    "tenantMenuIds": [
      0
    ]
  }
}
```

### 新增租户权限组
**URL:** /permissionGroup/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增租户权限组

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|false|权限组id|-|
|└─groupName|string|false|权限组名|-|
|└─describe|string|false|描述|-|
|└─tenantMenuIds|array|false|所属菜单id|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/permissionGroup/' --data '{
  "id": 0,
  "groupName": "",
  "describe": "",
  "tenantMenuIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改租户权限组
**URL:** /permissionGroup/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改租户权限组

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─id|int64|true|权限组id|-|
|└─groupName|string|false|权限组名|-|
|└─describe|string|false|描述|-|
|└─tenantMenuIds|array|false|所属菜单id|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/permissionGroup/' --data '{
  "id": 0,
  "groupName": "",
  "describe": "",
  "tenantMenuIds": [
    0
  ]
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除租户权限组
**URL:** /permissionGroup/{groupId}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除租户权限组

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|groupId|int64|false|权限组id|-|

**Request-example:**
```bash
curl -X DELETE -i '/permissionGroup/{groupId}?groupId=0&=dac2vi'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

# 定时任务管理
## 调度任务信息操作处理
### 查询定时任务列表
**URL:** /monitor/job/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 查询定时任务列表

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|jobName|string|true|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|jobGroup|enum|false|任务组名<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|invokeTarget|string|true|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|status|enum|false|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|

**Request-example:**
```bash
curl -X GET -i '/monitor/job/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&jobGroup=ADMIN&status=NORMAL&jobName=&invokeTarget='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|任务ID|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─jobName|string|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─jobGroup|enum|任务分组<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─invokeTarget|string|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─cronExpression|string|cron执行表达式<br/>Validation[Size(min=0, max=255, message=Cron执行表达式不能超过255个字符) ]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─misfirePolicy|enum|cron计划策略<br/>[Enum: DEFAULT(0,"默认")<br/>, IMMEDIATELY(1,"立即触发执行")<br/>, IMMEDIATELY_ONCE(2,"触发一次执行")<br/>, NOT_TIGER_IMMEDIATE(3,"不触发立即执行")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─concurrent|enum|是否并发执行（0允许 1禁止）<br/>[Enum: ALLOW(0,"允许")<br/>, PROHIBIT(1,"禁止")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─status|enum|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "jobName": "",
        "jobGroup": "ADMIN",
        "invokeTarget": "",
        "cronExpression": "",
        "misfirePolicy": "DEFAULT",
        "concurrent": "ALLOW",
        "status": "NORMAL"
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 获取定时任务详细信息
**URL:** /monitor/job/{jobId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 获取定时任务详细信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|jobId|int64|false|No comments found.|-|

**Request-example:**
```bash
curl -X GET -i '/monitor/job/{jobId}?jobId=0&=wedrto'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|任务ID|-|
|└─jobName|string|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|└─jobGroup|enum|任务分组<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─invokeTarget|string|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|└─cronExpression|string|cron执行表达式<br/>Validation[Size(min=0, max=255, message=Cron执行表达式不能超过255个字符) ]|-|
|└─misfirePolicy|enum|cron计划策略<br/>[Enum: DEFAULT(0,"默认")<br/>, IMMEDIATELY(1,"立即触发执行")<br/>, IMMEDIATELY_ONCE(2,"触发一次执行")<br/>, NOT_TIGER_IMMEDIATE(3,"不触发立即执行")<br/>]|-|
|└─concurrent|enum|是否并发执行（0允许 1禁止）<br/>[Enum: ALLOW(0,"允许")<br/>, PROHIBIT(1,"禁止")<br/>]|-|
|└─status|enum|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "jobName": "",
    "jobGroup": "ADMIN",
    "invokeTarget": "",
    "cronExpression": "",
    "misfirePolicy": "DEFAULT",
    "concurrent": "ALLOW",
    "status": "NORMAL"
  }
}
```

### 新增定时任务
**URL:** /monitor/job/

**Type:** POST


**Content-Type:** application/json

**Description:** 新增定时任务

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─queryWrapper|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─with|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─queryTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dataSource|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─hint|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─selectColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joins|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joinTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─whereQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─groupByColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─havingQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─orderBys|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─unions|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitOffset|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitRows|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─endFragments|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─context|map|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─any object|object|false|any object.|-|
|└─createTime|string|false|创建时间|-|
|└─updateTime|string|false|修改时间|-|
|└─createUser|int64|false|创建人|-|
|└─updateUser|int64|false|修改人|-|
|└─id|int64|false|任务ID|-|
|└─jobName|string|true|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|└─jobGroup|enum|false|任务分组<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─invokeTarget|string|true|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|└─cronExpression|string|true|cron执行表达式<br/>Validation[Size(min=0, max=255, message=Cron执行表达式不能超过255个字符) ]|-|
|└─misfirePolicy|enum|false|cron计划策略<br/>[Enum: DEFAULT(0,"默认")<br/>, IMMEDIATELY(1,"立即触发执行")<br/>, IMMEDIATELY_ONCE(2,"触发一次执行")<br/>, NOT_TIGER_IMMEDIATE(3,"不触发立即执行")<br/>]|-|
|└─concurrent|enum|false|是否并发执行（0允许 1禁止）<br/>[Enum: ALLOW(0,"允许")<br/>, PROHIBIT(1,"禁止")<br/>]|-|
|└─status|enum|false|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/monitor/job/' --data '{
  "queryWrapper": {
    "with": {
      "recursive": true,
      "withItems": [
        {
          "$ref": "..."
        }
      ]
    },
    "queryTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "dataSource": "",
    "hint": "",
    "selectColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "joins": [
      {
        "type": "",
        "queryTable": {
          "$ref": "..."
        },
        "on": {
          "$ref": "..."
        },
        "effective": true
      }
    ],
    "joinTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "whereQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "groupByColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "havingQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "orderBys": [
      {
        "queryColumn": {
          "$ref": "..."
        },
        "orderType": "",
        "nullsFirst": true,
        "nullsLast": true
      }
    ],
    "unions": [
      {
        "key": "",
        "queryWrapper": {
          "$ref": "..."
        }
      }
    ],
    "limitOffset": 0,
    "limitRows": 0,
    "endFragments": [
      ""
    ],
    "context": {
      "mapKey": {}
    }
  },
  "createTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "updateTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "createUser": 0,
  "updateUser": 0,
  "id": 0,
  "jobName": "",
  "jobGroup": "ADMIN",
  "invokeTarget": "",
  "cronExpression": "",
  "misfirePolicy": "DEFAULT",
  "concurrent": "ALLOW",
  "status": "NORMAL"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 修改定时任务
**URL:** /monitor/job/

**Type:** PUT


**Content-Type:** application/json

**Description:** 修改定时任务

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─queryWrapper|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─with|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─queryTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dataSource|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─hint|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─selectColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joins|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joinTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─whereQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─groupByColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─havingQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─orderBys|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─unions|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitOffset|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitRows|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─endFragments|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─context|map|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─any object|object|false|any object.|-|
|└─createTime|string|false|创建时间|-|
|└─updateTime|string|false|修改时间|-|
|└─createUser|int64|false|创建人|-|
|└─updateUser|int64|false|修改人|-|
|└─id|int64|false|任务ID|-|
|└─jobName|string|true|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|└─jobGroup|enum|false|任务分组<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─invokeTarget|string|true|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|└─cronExpression|string|true|cron执行表达式<br/>Validation[Size(min=0, max=255, message=Cron执行表达式不能超过255个字符) ]|-|
|└─misfirePolicy|enum|false|cron计划策略<br/>[Enum: DEFAULT(0,"默认")<br/>, IMMEDIATELY(1,"立即触发执行")<br/>, IMMEDIATELY_ONCE(2,"触发一次执行")<br/>, NOT_TIGER_IMMEDIATE(3,"不触发立即执行")<br/>]|-|
|└─concurrent|enum|false|是否并发执行（0允许 1禁止）<br/>[Enum: ALLOW(0,"允许")<br/>, PROHIBIT(1,"禁止")<br/>]|-|
|└─status|enum|false|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/monitor/job/' --data '{
  "queryWrapper": {
    "with": {
      "recursive": true,
      "withItems": [
        {
          "$ref": "..."
        }
      ]
    },
    "queryTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "dataSource": "",
    "hint": "",
    "selectColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "joins": [
      {
        "type": "",
        "queryTable": {
          "$ref": "..."
        },
        "on": {
          "$ref": "..."
        },
        "effective": true
      }
    ],
    "joinTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "whereQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "groupByColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "havingQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "orderBys": [
      {
        "queryColumn": {
          "$ref": "..."
        },
        "orderType": "",
        "nullsFirst": true,
        "nullsLast": true
      }
    ],
    "unions": [
      {
        "key": "",
        "queryWrapper": {
          "$ref": "..."
        }
      }
    ],
    "limitOffset": 0,
    "limitRows": 0,
    "endFragments": [
      ""
    ],
    "context": {
      "mapKey": {}
    }
  },
  "createTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "updateTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "createUser": 0,
  "updateUser": 0,
  "id": 0,
  "jobName": "",
  "jobGroup": "ADMIN",
  "invokeTarget": "",
  "cronExpression": "",
  "misfirePolicy": "DEFAULT",
  "concurrent": "ALLOW",
  "status": "NORMAL"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 定时任务状态修改
**URL:** /monitor/job/changeStatus

**Type:** PUT


**Content-Type:** application/json

**Description:** 定时任务状态修改

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─queryWrapper|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─with|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─queryTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dataSource|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─hint|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─selectColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joins|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joinTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─whereQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─groupByColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─havingQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─orderBys|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─unions|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitOffset|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitRows|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─endFragments|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─context|map|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─any object|object|false|any object.|-|
|└─createTime|string|false|创建时间|-|
|└─updateTime|string|false|修改时间|-|
|└─createUser|int64|false|创建人|-|
|└─updateUser|int64|false|修改人|-|
|└─id|int64|false|任务ID|-|
|└─jobName|string|true|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|└─jobGroup|enum|false|任务分组<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─invokeTarget|string|true|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|└─cronExpression|string|true|cron执行表达式<br/>Validation[Size(min=0, max=255, message=Cron执行表达式不能超过255个字符) ]|-|
|└─misfirePolicy|enum|false|cron计划策略<br/>[Enum: DEFAULT(0,"默认")<br/>, IMMEDIATELY(1,"立即触发执行")<br/>, IMMEDIATELY_ONCE(2,"触发一次执行")<br/>, NOT_TIGER_IMMEDIATE(3,"不触发立即执行")<br/>]|-|
|└─concurrent|enum|false|是否并发执行（0允许 1禁止）<br/>[Enum: ALLOW(0,"允许")<br/>, PROHIBIT(1,"禁止")<br/>]|-|
|└─status|enum|false|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|

**Request-example:**
```bash
curl -X PUT -H 'Content-Type: application/json' -i '/monitor/job/changeStatus' --data '{
  "queryWrapper": {
    "with": {
      "recursive": true,
      "withItems": [
        {
          "$ref": "..."
        }
      ]
    },
    "queryTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "dataSource": "",
    "hint": "",
    "selectColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "joins": [
      {
        "type": "",
        "queryTable": {
          "$ref": "..."
        },
        "on": {
          "$ref": "..."
        },
        "effective": true
      }
    ],
    "joinTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "whereQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "groupByColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "havingQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "orderBys": [
      {
        "queryColumn": {
          "$ref": "..."
        },
        "orderType": "",
        "nullsFirst": true,
        "nullsLast": true
      }
    ],
    "unions": [
      {
        "key": "",
        "queryWrapper": {
          "$ref": "..."
        }
      }
    ],
    "limitOffset": 0,
    "limitRows": 0,
    "endFragments": [
      ""
    ],
    "context": {
      "mapKey": {}
    }
  },
  "createTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "updateTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "createUser": 0,
  "updateUser": 0,
  "id": 0,
  "jobName": "",
  "jobGroup": "ADMIN",
  "invokeTarget": "",
  "cronExpression": "",
  "misfirePolicy": "DEFAULT",
  "concurrent": "ALLOW",
  "status": "NORMAL"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 定时任务立即执行一次
**URL:** /monitor/job/run

**Type:** POST


**Content-Type:** application/json

**Description:** 定时任务立即执行一次

**Body-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|code|int32|false|响应码|-|
|message|string|false|消息内容|-|
|result|object|false|响应数据|-|
|└─queryWrapper|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─with|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─queryTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─dataSource|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─hint|string|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─selectColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joins|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─joinTables|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─whereQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─groupByColumns|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─havingQueryCondition|object|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─orderBys|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─unions|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitOffset|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─limitRows|int64|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─endFragments|array|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─context|map|false|No comments found.|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─any object|object|false|any object.|-|
|└─createTime|string|false|创建时间|-|
|└─updateTime|string|false|修改时间|-|
|└─createUser|int64|false|创建人|-|
|└─updateUser|int64|false|修改人|-|
|└─id|int64|false|任务ID|-|
|└─jobName|string|true|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|└─jobGroup|enum|false|任务分组<br/>[Enum: ADMIN(0,"管理端")<br/>, TENANT(1,"租户端")<br/>]|-|
|└─invokeTarget|string|true|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|└─cronExpression|string|true|cron执行表达式<br/>Validation[Size(min=0, max=255, message=Cron执行表达式不能超过255个字符) ]|-|
|└─misfirePolicy|enum|false|cron计划策略<br/>[Enum: DEFAULT(0,"默认")<br/>, IMMEDIATELY(1,"立即触发执行")<br/>, IMMEDIATELY_ONCE(2,"触发一次执行")<br/>, NOT_TIGER_IMMEDIATE(3,"不触发立即执行")<br/>]|-|
|└─concurrent|enum|false|是否并发执行（0允许 1禁止）<br/>[Enum: ALLOW(0,"允许")<br/>, PROHIBIT(1,"禁止")<br/>]|-|
|└─status|enum|false|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|

**Request-example:**
```bash
curl -X POST -H 'Content-Type: application/json' -i '/monitor/job/run' --data '{
  "queryWrapper": {
    "with": {
      "recursive": true,
      "withItems": [
        {
          "$ref": "..."
        }
      ]
    },
    "queryTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "dataSource": "",
    "hint": "",
    "selectColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "joins": [
      {
        "type": "",
        "queryTable": {
          "$ref": "..."
        },
        "on": {
          "$ref": "..."
        },
        "effective": true
      }
    ],
    "joinTables": [
      {
        "schema": "",
        "name": "",
        "alias": ""
      }
    ],
    "whereQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "groupByColumns": [
      {
        "null": {
          "$ref": "..."
        },
        "notNull": {
          "$ref": "..."
        },
        "table": {
          "$ref": "..."
        },
        "name": "",
        "alias": "",
        "returnCopyByAsMethod": true
      }
    ],
    "havingQueryCondition": {
      "column": {
        "$ref": "..."
      },
      "logic": "",
      "value": {},
      "effective": true,
      "prev": {
        "$ref": "..."
      },
      "next": {
        "$ref": "..."
      },
      "connector": "AND",
      "empty": true
    },
    "orderBys": [
      {
        "queryColumn": {
          "$ref": "..."
        },
        "orderType": "",
        "nullsFirst": true,
        "nullsLast": true
      }
    ],
    "unions": [
      {
        "key": "",
        "queryWrapper": {
          "$ref": "..."
        }
      }
    ],
    "limitOffset": 0,
    "limitRows": 0,
    "endFragments": [
      ""
    ],
    "context": {
      "mapKey": {}
    }
  },
  "createTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "updateTime": "yyyy-MM-dd'\''T'\''HH:mm:ss.SSSXXX",
  "createUser": 0,
  "updateUser": 0,
  "id": 0,
  "jobName": "",
  "jobGroup": "ADMIN",
  "invokeTarget": "",
  "cronExpression": "",
  "misfirePolicy": "DEFAULT",
  "concurrent": "ALLOW",
  "status": "NORMAL"
}'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 删除定时任务
**URL:** /monitor/job/{jobIds}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除定时任务

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|jobIds|array|false|No comments found.,[array of int64]|-|

**Request-example:**
```bash
curl -X DELETE -i '/monitor/job/{jobIds}?jobIds=02wjod&jobIds=02wjod'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

## 调度日志操作处理
### 查询定时任务调度日志列表
**URL:** /monitor/jobLog/page

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 查询定时任务调度日志列表

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|pageNumber|int64|false|当前页码。|-|
|pageSize|int64|false|每页数据数量。|-|
|totalPage|int64|false|总页数。|-|
|totalRow|int64|false|总数据数量。|-|
|optimizeCountQuery|boolean|false|是否优化分页查询 COUNT 语句。|-|
|jobName|string|false|任务名称<br/>Validation[Size(min=0, max=64, message=任务名称不能超过64个字符) ]|-|
|jobGroup|string|false|任务组名|-|
|status|enum|false|任务状态（0正常 1暂停）<br/>[Enum: NORMAL(0,"正常")<br/>, PAUSE(1,"暂停")<br/>]|-|
|invokeTarget|string|false|调用目标字符串<br/>Validation[Size(min=0, max=500, message=调用目标字符串长度不能超过500个字符) ]|-|
|offsetDateTimes|array|false|时间范围查询<br/>Validation[Size(max=2) ]|-|

**Request-example:**
```bash
curl -X GET -i '/monitor/jobLog/page?pageNumber=0&pageSize=0&totalPage=0&totalRow=0&optimizeCountQuery=true&status=NORMAL&offsetDateTimes=,&invokeTarget=&jobName=&jobGroup='
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─records|array|分页内容|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createTime|string|创建时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateTime|string|修改时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─createUser|int64|创建人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─updateUser|int64|修改人|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─id|int64|日志序号|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─jobName|string|任务名称|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─jobGroup|string|任务组名|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─invokeTarget|string|调用目标字符串|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─jobMessage|string|日志信息|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─status|enum|执行状态（0正常 1失败）<br/>[Enum: SUCCESS(0,"成功")<br/>, FAIL(1,"失败")<br/>]|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─exceptionInfo|string|异常信息|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─startTime|string|开始时间|-|
|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─stopTime|string|停止时间|-|
|└─pageNumber|int64|当前页码。|-|
|└─pageSize|int64|每页数据数量。|-|
|└─totalPage|int64|总页数。|-|
|└─totalRow|int64|总数据数量。|-|
|└─optimizeCountQuery|boolean|是否优化分页查询 COUNT 语句。|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "records": [
      {
        "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "createUser": 0,
        "updateUser": 0,
        "id": 0,
        "jobName": "",
        "jobGroup": "",
        "invokeTarget": "",
        "jobMessage": "",
        "status": "SUCCESS",
        "exceptionInfo": "",
        "startTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
        "stopTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
      }
    ],
    "pageNumber": 0,
    "pageSize": 0,
    "totalPage": 0,
    "totalRow": 0,
    "optimizeCountQuery": true
  }
}
```

### 根据调度编号获取详细信息
**URL:** /monitor/jobLog/{jobLogId}

**Type:** GET


**Content-Type:** application/x-www-form-urlencoded

**Description:** 根据调度编号获取详细信息

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|jobLogId|int64|false|No comments found.|-|

**Request-example:**
```bash
curl -X GET -i '/monitor/jobLog/{jobLogId}?jobLogId=0&=3mgbrv'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|
|└─createTime|string|创建时间|-|
|└─updateTime|string|修改时间|-|
|└─createUser|int64|创建人|-|
|└─updateUser|int64|修改人|-|
|└─id|int64|日志序号|-|
|└─jobName|string|任务名称|-|
|└─jobGroup|string|任务组名|-|
|└─invokeTarget|string|调用目标字符串|-|
|└─jobMessage|string|日志信息|-|
|└─status|enum|执行状态（0正常 1失败）<br/>[Enum: SUCCESS(0,"成功")<br/>, FAIL(1,"失败")<br/>]|-|
|└─exceptionInfo|string|异常信息|-|
|└─startTime|string|开始时间|-|
|└─stopTime|string|停止时间|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {
    "createTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "updateTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "createUser": 0,
    "updateUser": 0,
    "id": 0,
    "jobName": "",
    "jobGroup": "",
    "invokeTarget": "",
    "jobMessage": "",
    "status": "SUCCESS",
    "exceptionInfo": "",
    "startTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "stopTime": "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
  }
}
```

### 删除定时任务调度日志
**URL:** /monitor/jobLog/{jobLogIds}

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 删除定时任务调度日志

**Query-parameters:**

| Parameter | Type | Required | Description | Since |
|-----------|------|----------|-------------|-------|
|jobLogIds|array|false|No comments found.,[array of int64]|-|

**Request-example:**
```bash
curl -X DELETE -i '/monitor/jobLog/{jobLogIds}?jobLogIds=1zgct3&jobLogIds=1zgct3'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```

### 清空定时任务调度日志
**URL:** /monitor/jobLog/clean

**Type:** DELETE


**Content-Type:** application/x-www-form-urlencoded

**Description:** 清空定时任务调度日志

**Request-example:**
```bash
curl -X DELETE -i '/monitor/jobLog/clean'
```
**Response-fields:**

| Field | Type | Description | Since |
|-------|------|-------------|-------|
|code|int32|响应码|-|
|message|string|消息内容|-|
|result|object|响应数据|-|

**Response-example:**
```json
{
  "code": 0,
  "message": "",
  "result": {}
}
```


