package cn.murky.admin;

import org.noear.socketd.SocketD;
import org.noear.socketd.transport.java_websocket.WsNioProvider;
import org.noear.socketd.transport.smartsocket.tcp.TcpAioProvider;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Import;
import org.noear.solon.annotation.SolonMain;
import org.noear.solon.scheduling.annotation.EnableScheduling;

@SolonMain
@Import(scanPackages = "cn.murky")
@EnableScheduling
public class App {
    public static void main(String[] args) {
        Solon.start(App.class, args, app -> {
            // 使用socketd服务
            app.enableSocketD(true);
//            TcpAioProvider tcpAioProvider = new TcpAioProvider();
//            WsNioProvider wsNioProvider = new WsNioProvider();
//            SocketD.registerServerProvider(tcpAioProvider);
//            SocketD.registerServerProvider(wsNioProvider);
//            SocketD.registerClientProvider(tcpAioProvider);
//            SocketD.registerClientProvider(wsNioProvider);
        });
    }

}
