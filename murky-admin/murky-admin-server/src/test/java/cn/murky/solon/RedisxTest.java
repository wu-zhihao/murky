package cn.murky.solon;

import cn.murky.admin.App;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.redisx.RedisClient;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;

import java.util.Properties;

@Slf4j
@SolonTest(App.class)
@ExtendWith(SolonJUnit5Extension.class)
public class RedisxTest {
    @Inject
    private RedisClient redisClient;
    @Test
    public void test() throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            int finalI = i;
            Thread.startVirtualThread(()->{
                if (redisClient.getLock("ddddd").tryLock()) {
                    System.out.println("抢到了"+ finalI);
                }else{
                    System.out.println("没抢到"+ finalI);
                }
            });
        }
        Thread.sleep(10000);
    }
}
