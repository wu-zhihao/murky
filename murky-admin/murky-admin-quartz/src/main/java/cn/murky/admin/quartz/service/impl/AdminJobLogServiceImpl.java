package cn.murky.admin.quartz.service.impl;

import cn.murky.quartz.domain.entity.SysJob;
import cn.murky.quartz.service.impl.SysJobServiceImpl;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.solon.annotation.Component;
import org.quartz.SchedulerException;

@Component
public class AdminJobLogServiceImpl extends SysJobServiceImpl {

    @Override
    /**
     * 立即运行任务
     *
     * @param job 调度信息
     * @return 结果
     */
    public boolean run(SysJob job) throws SchedulerException {
        return super.run(job);
    }
}
