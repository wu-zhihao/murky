package cn.murky.admin.quartz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.common.utils.CollectionUtils;
import cn.murky.quartz.domain.dto.SysJobLogPageDTO;
import cn.murky.quartz.domain.entity.SysJobLog;
import cn.murky.quartz.service.ISysJobLogService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import com.mybatisflex.core.constant.SqlOperator;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.row.Db;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Delete;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.validation.annotation.Valid;
import cn.murky.quartz.domain.entity.table.SysJobLogTableDef;
import java.util.Arrays;
import java.util.Optional;

/**
 * 调度日志操作处理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("monitor/jobLog")
public class SysJobLogController extends BaseController<ISysJobLogService> {

    /**
     * 查询定时任务调度日志列表
     */
    @SaCheckPermission("monitor:jobLog")
    @Mapping("/page")
    @Get
    public ApiResult<Page<SysJobLog>> list(SysJobLogPageDTO sysJobLogPageDTO) {
        SysJobLogTableDef SYS_JOB_LOG = SysJobLogTableDef.SYS_JOB_LOG;
        QueryCondition when = QueryCondition.create(SYS_JOB_LOG.CREATE_TIME,
                        SqlOperator.BETWEEN,
                        sysJobLogPageDTO.getOffsetDateTimes())
                .when(CollectionUtils.isNotEmpty(sysJobLogPageDTO.getOffsetDateTimes()));
        QueryWrapper queryWrapper = QueryWrapper.create().where(SYS_JOB_LOG.JOB_NAME.likeRight(sysJobLogPageDTO.getJobName(),If::hasText)
                .and(SYS_JOB_LOG.JOB_GROUP.eq(sysJobLogPageDTO.getJobGroup(),If::hasText))
                .and(SYS_JOB_LOG.STATUS.eq(sysJobLogPageDTO.getStatus(),If::notNull))
                .and(SYS_JOB_LOG.INVOKE_TARGET.like(sysJobLogPageDTO.getInvokeTarget(),If::hasText))
                .and(when));
        Page<SysJobLog> page = baseService.page(sysJobLogPageDTO, queryWrapper);
        return ApiResult.ok(page);
    }


    /**
     * 根据调度编号获取详细信息
     */
    @SaCheckPermission("monitor:jobLog")
    @Mapping(value = "/{jobLogId}")
    @Get
    public ApiResult<SysJobLog> getInfo(Long jobLogId) {
        return ApiResult.ok(baseService.getById(jobLogId));
    }


    /**
     * 删除定时任务调度日志
     */
    @SaCheckPermission("monitor:job:remove")
    @Mapping("/{jobLogIds}")
    @Delete
    public ApiResult<?> remove(Long[] jobLogIds) {
        return ApiResult.ok(baseService.removeByIds(Arrays.stream(jobLogIds).toList()));
    }

    /**
     * 清空定时任务调度日志
     */
    @SaCheckPermission("monitor:job:remove")
    @Mapping("/clean")
    @Delete
    public ApiResult<?> clean() {
        Db.deleteBySql("TRUNCATE "+SysJobLogTableDef.SYS_JOB_LOG.getTableName());
        return ApiResult.ok();
    }
}
