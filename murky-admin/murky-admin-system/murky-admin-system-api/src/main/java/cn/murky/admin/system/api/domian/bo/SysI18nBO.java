package cn.murky.admin.system.api.domian.bo;

import cn.murky.admin.system.api.enums.I18nTag;
import cn.murky.common.domain.entity.BaseEntity;
import lombok.Data;

@Data
public class SysI18nBO extends BaseEntity {
    private Long id;

    /**
     * i18n编码
     */
    private String i18nKey;

    /**
     * i18n值
     */
    private String i18nValue;

    /**
     * 地区编码(字典:i18n:language)
     */
    private String language;

    /**
     * 标签(字典:i18n:tag)
     */
    private I18nTag i18nTag;
}
