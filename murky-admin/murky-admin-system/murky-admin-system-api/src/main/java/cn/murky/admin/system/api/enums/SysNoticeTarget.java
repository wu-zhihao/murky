package cn.murky.admin.system.api.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

/**
 * 系统通知  通知目标
 *
 * @auth hans
 */
@AllArgsConstructor
@Getter
public enum SysNoticeTarget {
    ADMIN(0,"管理端"),
    TENANT(1,"租户端"),
    ;

    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    public Integer getCode(){
        return this.code;
    }

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
