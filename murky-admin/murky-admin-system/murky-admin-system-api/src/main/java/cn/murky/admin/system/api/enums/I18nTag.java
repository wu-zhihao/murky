package cn.murky.admin.system.api.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;
/**
 * 国际化语言标签
 * @auth hans
 */
@AllArgsConstructor
@Getter
public enum I18nTag {
    COMMON("common","公共"),
    ADMIN("admin","管理端"),
    TENANT("tenant","租户端"),
    ;
    @EnumValue
    @ONodeAttr
    private final String code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
