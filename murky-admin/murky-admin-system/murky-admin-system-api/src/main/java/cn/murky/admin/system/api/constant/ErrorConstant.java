package cn.murky.admin.system.api.constant;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstant {
    public static final ErrorRecord OLD_PASSWORD_ERROR = new ErrorRecord(5000, "system.user.oldPasswordError");
    public static final ErrorRecord LANGUAGE_NOT_SUPPORT = new ErrorRecord(5001, "system.i18n.supportedLanguage");
    public static final ErrorRecord DEPT_HAS_CHILD = new ErrorRecord(5002, "system.dept.hasChildDeptNotRemove");
    public static final ErrorRecord DEPT_IS_USED = new ErrorRecord(5003, "system.dept.usingNotRemove");
    public static final ErrorRecord I18N_KEY_ALREADY = new ErrorRecord(5004, "system.i18n.i18nKeyAlready");
    public static final ErrorRecord I18N_KEY_EXIT = new ErrorRecord(5004, "system.i18n.i18nKeyExit");
    public static final ErrorRecord ILLEGAL_LANGUAGE = new ErrorRecord(5005, "system.i18n.i18nLanguageIllegal");
    public static final ErrorRecord DEFAULT_LANGUAGE_NOT_SET = new ErrorRecord(5006, "system.i18n.defaultLanguageNotSet");
    public static final ErrorRecord MENU_HAS_CHILD = new ErrorRecord(5007, "system.menu.hasChildMenuNotRemove");
    public static final ErrorRecord MENU_IS_USED = new ErrorRecord(5008, "system.menu.usingNotRemove");
    public static final ErrorRecord ROLE_CODE_ALREADY = new ErrorRecord(5009, "system.role.roleCodeAlready");
    public static final ErrorRecord ROLE_NAME_ALREADY = new ErrorRecord(5010, "system.role.roleNameAlready");
    public static final ErrorRecord ACCOUNT_ALREADY = new ErrorRecord(5011, "system.user.AccountAlready");
    public static final ErrorRecord CONFIRM_PASSWORD_ERROR = new ErrorRecord(5012, "system.user.surePasswordError");
    public static final ErrorRecord USER_NOT_EXIST = new ErrorRecord(5013, "system.user.userNotExist");
    // query参数格式不正确
    public static final ErrorRecord QUERY_PARAMS_FORMATE_ERROR = new ErrorRecord(5014, "system.job.query.params.formate");
    // 该key已存在
    public static final ErrorRecord KEY_ALREADY_EXISTS_ERROR = new ErrorRecord(5014, "system.params.key.already");
}
