package cn.murky.admin.system.api.constant;

import cn.murky.common.record.ErrorRecord;

public class SysNoticeError {
    // 通知公告不存在
    public static final ErrorRecord SYS_NOTICE_NOT_EXIST = new ErrorRecord(6000, "system.notice.notExist");

}
