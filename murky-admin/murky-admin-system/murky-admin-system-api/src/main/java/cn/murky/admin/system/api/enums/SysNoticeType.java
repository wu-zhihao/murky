package cn.murky.admin.system.api.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

/**
 * 系统通知 公告类型
 *
 * @auth hans
 */
@AllArgsConstructor
@Getter
public enum SysNoticeType {
    NOTICE(0,"通知"),
    PROCLAMATION(1,"公告"),
    ;

    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
