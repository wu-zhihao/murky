package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.api.enums.SysNoticeTarget;
import cn.murky.admin.system.api.enums.SysNoticeType;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import cn.murky.admin.system.biz.domain.vo.SysNoticePageVO;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/**
 * 公告管理分页DTO实体类
 * @author hans
 */
@Data
@Accessors(chain = true)
public class SysNoticePageDTO extends Page<SysNoticePageVO> {
    /**
     * 标题
     */
    private String title;

    /**
     * 操作人员
     */
    private String operators;

    /**
     * 公告类型（0通知 1公告）
     */
    private SysNoticeType type;

    /**
     * 通知目标（0admin 1tenant）
     */
    private SysNoticeTarget target;

    /**
     * 到期时间
     */
    private OffsetDateTime expire;
}
