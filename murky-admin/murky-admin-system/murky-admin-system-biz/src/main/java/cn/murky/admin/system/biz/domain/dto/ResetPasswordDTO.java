package cn.murky.admin.system.biz.domain.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * 用户密码重置类
 */
@Data
@Accessors(chain = true)
public class ResetPasswordDTO {
    /**
     * 用户id
     */
    @NotNull
    private Long id;

    /**
     * 密码
     */
    @NotBlank
    private String password;

    /**
     * 二次确定密码
     */
    @NotBlank
    private String confirmPassword;
}
