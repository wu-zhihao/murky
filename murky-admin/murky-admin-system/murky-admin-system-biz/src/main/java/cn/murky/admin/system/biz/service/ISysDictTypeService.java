package cn.murky.admin.system.biz.service;

import cn.murky.admin.system.biz.domain.dto.SysDictTypeFromDTO;
import cn.murky.admin.system.biz.domain.entity.SysDictType;
import cn.murky.admin.system.biz.domain.vo.SysDictTypeInfoVO;
import com.mybatisflex.core.service.IService;

/**
 * 字典类型Service
 *
 * @auth hans
 */
public interface ISysDictTypeService extends IService<SysDictType> {

    /**
     * 刷新缓存
     */
    void refreshDict();

    /**
     * 重写添加方法
     */
    boolean save(SysDictTypeFromDTO sysDictTypeFromDTO);

    /**
     * 重写修改方法
     */
    boolean edit(SysDictTypeFromDTO sysDictTypeFromDTO);

    /**
     * 重写删除方法
     */
    boolean removeById(Long id);

    /**
     * 获取字典类型详情
     * @param id
     */
    SysDictTypeInfoVO info(Long id);
}
