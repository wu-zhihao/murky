package cn.murky.admin.system.biz.mapper;

import cn.murky.admin.system.biz.domain.entity.SysNotice;
import com.mybatisflex.core.BaseMapper;

/**
 * 系统通知Mapper
 * @author hans
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {
}
