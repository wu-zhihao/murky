package cn.murky.admin.system.biz.domain.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 系统参数实体类
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table("system_parameter")
public class SystemParameter extends BaseEntity<SystemParameter> {
    /**
     * id
     */
    @Id
    private Long id;

    /**
     * 参数key
     */
    private String key;

    /**
     * 参数值
     */
    private String value;

    /**
     * 描述
     */
    private String describe;

}
