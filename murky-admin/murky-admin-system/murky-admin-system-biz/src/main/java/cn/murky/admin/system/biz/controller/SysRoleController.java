package cn.murky.admin.system.biz.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.system.biz.domain.dto.SysRoleFromDTO;
import cn.murky.admin.system.biz.domain.dto.SysRolePageDTO;
import cn.murky.admin.system.biz.domain.entity.table.SysRoleTableDef;
import cn.murky.admin.system.biz.domain.vo.SysRoleVO;
import cn.murky.admin.system.biz.contant.SystemContant;
import cn.murky.admin.system.biz.domain.entity.SysRole;
import cn.murky.admin.system.biz.service.ISysRoleService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import cn.murky.admin.core.utils.SecurityUtils;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryWrapper;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/***
 * 角色管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("role")
public class SysRoleController extends BaseController<ISysRoleService> {
    /**
     * 角色列表分页查询
     * @param SysRolePageDTO SysRolePageDTO
     * @return Page<SysRole>
     */
    @Get
    @Mapping("page")
    @SaCheckPermission("role")
    public ApiResult<Page<SysRole>> page(SysRolePageDTO SysRolePageDTO) {
        SysRoleTableDef SYS_ROLE = SysRoleTableDef.SYS_ROLE;
        Page<SysRole> result = baseService.page(SysRolePageDTO,
                QueryWrapper.create()
                        .and(SYS_ROLE.ROLE_CODE.like(SysRolePageDTO.getRoleCode(), If::hasText))
                        .and(SYS_ROLE.ROLE_NAME.like(SysRolePageDTO.getRoleName(), If::hasText))
                        .and(SYS_ROLE.ROLE_CODE.ne(SystemContant.ADMIN_ROLE_CODE))
                        .and(SYS_ROLE.ID.notIn(SecurityUtils.getUserInfo().getFkRoleId()))
                        .orderBy(SYS_ROLE.CREATE_TIME.asc())
        );
        return ApiResult.ok(result);
    }

    /**
     * 角色列表查询
     * @param SysRolePageDTO SysRolePageDTO
     * @return List<SysRole>
     */
    @Get
    @Mapping("list")
    public ApiResult<List<SysRole>> list(SysRolePageDTO SysRolePageDTO) {
        SysRoleTableDef SYS_ROLE = SysRoleTableDef.SYS_ROLE;
        List<SysRole> result = baseService.list(
                QueryWrapper.create()
                .and(SYS_ROLE.ROLE_CODE.ne(SystemContant.ADMIN_ROLE_CODE))
                        .orderBy(SYS_ROLE.CREATE_TIME.asc())
        );
        return ApiResult.ok(result);
    }

    /**
     * 角色详情
     * @param roleId 角色id
     * @return SysRoleVo
     */
    @Get
    @Mapping("{roleId}")
    @SaCheckPermission("role")
    public ApiResult<SysRoleVO> info(Long roleId) {
        return ApiResult.ok(baseService.info(roleId));
    }

    /**
     * 新增角色
     * @param sysRoleFromDTO sysRoleFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("role:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) SysRoleFromDTO sysRoleFromDTO) {
        return toResult(baseService.save(sysRoleFromDTO));
    }

    /**
     * 修改角色
     * @param sysRoleFromDTO sysRoleFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("role:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) SysRoleFromDTO sysRoleFromDTO) {
        boolean result = baseService.update(sysRoleFromDTO);
        return toResult(result);
    }

    /**
     * 删除角色
     * @param roleId 角色id
     */
    @Delete
    @Mapping("/{roleId}")
    @SaCheckPermission("role:remove")
    public ApiResult<?> remove(Long roleId) {
        boolean result = baseService.removeById(roleId);
        return toResult(result);
    }
}
