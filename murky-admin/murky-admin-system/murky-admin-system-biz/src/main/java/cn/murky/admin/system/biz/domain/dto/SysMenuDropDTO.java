package cn.murky.admin.system.biz.domain.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotEmpty;

import java.util.List;

/**
 * 菜单拖动接口参数实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysMenuDropDTO {

    /**
     * 父级菜单id
     */
    private Long parentId;

    /**
     * 菜单id集合
     */
    @NotEmpty
    private List<Long> menuIds;
}
