package cn.murky.admin.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.system.biz.domain.dto.SysMenuDropDTO;
import cn.murky.admin.system.biz.domain.dto.SysMenuFromDTO;
import cn.murky.admin.system.api.domian.SysMenuTree;
import cn.murky.admin.system.api.enums.MenuType;
import cn.murky.admin.system.biz.domain.entity.SysMenu;
import cn.murky.admin.system.biz.service.ISysMenuService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import org.noear.snack.ONode;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

import static cn.murky.admin.system.api.constant.ErrorConstant.QUERY_PARAMS_FORMATE_ERROR;

/***
 * 菜单管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("menu")
public class SysMenuController extends BaseController<ISysMenuService> {

    /**
     * 获取用户全量菜单
     * @return List<SysMenuTree>
     */
    @Get
    @Mapping("list")
    @SaCheckPermission("menu")
    public ApiResult<List<SysMenuTree>> list(){
        List<SysMenuTree> result = baseService.treeSysMenu(Arrays.asList(MenuType.MENU,MenuType.DIRECTORY,MenuType.BUTTON));
        return ApiResult.ok(result);
    }

    /**
     * 菜单详情
     * @param menuId 菜单id
     * @return SysMenu
     */
    @Get
    @Mapping("{menuId}")
    @SaCheckPermission("menu")
    public ApiResult<SysMenu> info(Long menuId){
        return ApiResult.ok(baseService.getById(menuId));
    }

    /**
     * 新增菜单
     * @param sysMenuFromDTO sysMenuFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("menu:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) SysMenuFromDTO sysMenuFromDTO){
        if(!ONode.loadStr(sysMenuFromDTO.getQuery()).isUndefined()&&!ONode.loadStr(sysMenuFromDTO.getQuery()).isObject()){
            return ApiResult.fail(QUERY_PARAMS_FORMATE_ERROR);
        }
        SysMenu sysMenu = sysMenuFromDTO.toEntity();
        if(MenuType.DIRECTORY== sysMenu.getType()){
            sysMenu.setComponent("LAYOUT");
        }
        return toResult(baseService.save(sysMenu));
    }

    /**
     * 修改菜单
     * @param sysMenuFromDTO sysMenuFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("menu:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) SysMenuFromDTO sysMenuFromDTO){
        if(!ONode.loadStr(sysMenuFromDTO.getQuery()).isUndefined()&&!ONode.loadStr(sysMenuFromDTO.getQuery()).isObject()){
            return ApiResult.fail(QUERY_PARAMS_FORMATE_ERROR);
        }
        SysMenu sysMenu = sysMenuFromDTO.toEntity();
        if(MenuType.DIRECTORY== sysMenu.getType()){
            sysMenu.setComponent("LAYOUT");
        }
        boolean result = baseService.updateById(sysMenu);
        return toResult(result);
    }

    /**
     * 菜单拖动
     * @param sysMenuDropDTO sysMenuDropDTO
     */
    @Put
    @Mapping("drop")
    @SaCheckPermission("menu:edit")
    public ApiResult<?> drop(@Body @Validated SysMenuDropDTO sysMenuDropDTO){
        return toResult(baseService.drop(sysMenuDropDTO));
    }

    /**
     * 删除菜单
     * @param menuId 菜单id
     */
    @Delete
    @Mapping("/{menuId}")
    @SaCheckPermission("menu:remove")
    public ApiResult<?> remove(Long menuId){
        boolean result = baseService.removeById(menuId);
        return toResult(result);
    }

}
