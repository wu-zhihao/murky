package cn.murky.admin.system.biz.domain.vo;

import cn.murky.admin.system.api.enums.SysNoticeTarget;
import cn.murky.admin.system.api.enums.SysNoticeType;
import cn.murky.common.enums.CommonStatus;
import com.mybatisflex.annotation.Id;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/**
 * 通知分页视图类
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysNoticePageVO {
    /**
     * 菜单id
     */
    @Id
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 公告类型（0通知 1公告）
     */
    private SysNoticeType type;

    /**
     * 通知目标（0admin 1tenant）
     */
    private SysNoticeTarget target;

    /**
     * 内容
     */
    private String content;

    /**
     * 通用状态（0正常 1停用）
     */
    private CommonStatus status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 操作人员
     */
    private String userName;

    /**
     * 到期时间
     */
    private OffsetDateTime expire;

    /**
     * 创建时间
     */
    private OffsetDateTime createTime;
}
