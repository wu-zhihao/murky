package cn.murky.admin.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import cn.murky.admin.system.api.enums.I18nTag;
import cn.murky.admin.system.biz.domain.dto.SysI18nFromDTO;
import cn.murky.admin.system.biz.domain.vo.SysI18nVO;
import cn.murky.admin.system.biz.domain.dto.SysI18nDTO;
import cn.murky.admin.system.biz.service.ISysI18nService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Info;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import com.mybatisflex.core.paginate.Page;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;
import java.util.Map;

/***
 * 国际化语言包管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("i18n")
public class SysI18nController extends BaseController<ISysI18nService> {

    /**
     * 分页查询
     * @param sysI18nDTO sysI18nDTO
     * @return 根据字典国际化语言包动态新增字段
     */
    @Get
    @Mapping("page")
    @SaCheckPermission("i18n")
    public ApiResult<Page<Map>> page(SysI18nDTO sysI18nDTO) {
        return ApiResult.ok(baseService.page(sysI18nDTO));
    }

    /**
     * 详情
     * @param sysI18nDTO sysI18nDTO
     * @return SysI18nVo
     */
    @Get
    @Mapping("info")
    @SaCheckPermission("i18n")
    public ApiResult<SysI18nVO> info(@Validated(Info.class) SysI18nDTO sysI18nDTO) {
        return ApiResult.ok(baseService.info(sysI18nDTO));
    }

    /**
     * 刷新缓存
     */
    @Post
    @Mapping("refresh")
    @SaCheckPermission(value = {"i18n:add","i18n:edit","i18n:remove"},mode = SaMode.OR)
    public ApiResult<Map<String, String>> refresh() {
        baseService.refresh();
        baseService.refreshLocal();
        return ApiResult.ok();
    }

    /**
     * 获取指定语言包
     * @param i18nTag 语言包标签
     * @param language 语言
     */
    @Get
    @Mapping("language")
    @Valid
    public ApiResult<Map<String, String>> language(@NotEmpty List<String> i18nTag, @NotBlank String language) {
        return ApiResult.ok(baseService.language(i18nTag, language));
    }

    /**
     * 新增国际化语言数据
     * @param sysI18nFromDTO sysI18nFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("i18n:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) SysI18nFromDTO sysI18nFromDTO) {
        return toResult(baseService.save(sysI18nFromDTO));
    }

    /**
     * 修改国际化语言数据
     * @param sysI18nFromDTO sysI18nFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("i18n:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) SysI18nFromDTO sysI18nFromDTO) {
        return toResult(baseService.edit(sysI18nFromDTO));
    }

    /**
     * 删除国际化语言数据
     * @param i18nKey 对应语言key的id
     * @param i18nTag 对应语言key的TAG
     */
    @Delete
    @Mapping("{i18nKey}/{i18nTag}")
    @SaCheckPermission("i18n:remove")
    public ApiResult<?> remove(String i18nKey, I18nTag i18nTag) {
        return toResult(baseService.remove(i18nKey,i18nTag));
    }
}
