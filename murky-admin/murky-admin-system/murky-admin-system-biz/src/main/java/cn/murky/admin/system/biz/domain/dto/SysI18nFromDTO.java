package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.api.enums.I18nTag;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

import java.util.List;

/**
 * i18n表单DTO实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysI18nFromDTO {

    /**
     * i18n_key编码
     */
    @NotBlank
    private String i18nKey;

    /**
     * i18n_tag标签
     */
    @NotNull
    private I18nTag i18nTag;

    /**
     * i18n参数
     */
    @NotNull
    private List<I18nInput> i18nInputs;

    /**
     * i18n参数实体
     */
    @Data
    @Accessors(chain = true)
    public class I18nInput{
        /**
         * id
         */
        private Long id;

        /**
         * i18n字典
         */
        private String language;

        /**
         * i18n值
         */
        private String i18nValue;
    }
}
