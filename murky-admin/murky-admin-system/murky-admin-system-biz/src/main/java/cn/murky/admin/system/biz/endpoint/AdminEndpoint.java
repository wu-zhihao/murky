package cn.murky.admin.system.biz.endpoint;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import cn.murky.common.utils.StringUtils;
import cn.murky.socketd.SdResult;
import cn.murky.socketd.constants.SocetdConstants;
import cn.murky.socketd.constants.SocetdEventConstants;
import lombok.extern.slf4j.Slf4j;
import org.noear.socketd.transport.core.Message;
import org.noear.socketd.transport.core.Session;
import org.noear.socketd.transport.core.listener.EventListener;
import org.noear.solon.net.annotation.ServerEndpoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cn.murky.socketd.constants.SocetdConstants.ORIGIN_COLUMN;
import static cn.murky.socketd.constants.SocetdConstants.SD_TOKEN_VALUE;

@ServerEndpoint("/admin")
@Slf4j
public class AdminEndpoint extends EventListener {
    List<Session> tenantSession = new ArrayList<>();


    public AdminEndpoint() {
        doOnOpen(s -> {
            // 连接的合法性鉴定
            Map<String, String> paramMap = s.handshake().paramMap();
            String token = paramMap.get(SocetdConstants.SD_TOKEN_COLUM);
            if (StringUtils.isEmpty(token)) {
                log.info("[NoticeEndpoint]----->doOnOpen---->非法的连接{}", paramMap.get(ORIGIN_COLUMN));
                s.close();
            }
            if (!SD_TOKEN_VALUE.equals(token)) {
                log.info("[NoticeEndpoint]----->doOnOpen---->非法的连接{}", paramMap.get(ORIGIN_COLUMN));
                s.close();
            }
            tenantSession.add(s);
        });
        doOnClose(s -> {
            log.debug("[NoticeEndpoint]----->doOnClose---->{}断开连接", s.sessionId());
            tenantSession.remove(s);
        });
        // 挂载事件
//        doOn(SYS_NOTICE_EVENT, this::doOnNotice);
//        doOn(READ_EVENT, this::doOnRead);
//        doOn(UNREAD_EVENT, this::doOnUnread);
    }

    /**
     * 发送系统通知到租户服务端然后由服务端进行转发
     */
    public void sendSysNotice(SysNotice sysNotice) {
        for (Session session : tenantSession) {
            try {
                SdResult.send(sysNotice, session, SocetdEventConstants.SYS_NOTICE_EVENT);
            } catch (IOException e) {
                e.fillInStackTrace();
            }
        }
    }
}
