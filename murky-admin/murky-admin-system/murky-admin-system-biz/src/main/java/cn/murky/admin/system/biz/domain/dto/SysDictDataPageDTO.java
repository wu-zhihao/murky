package cn.murky.admin.system.biz.domain.dto;

import cn.murky.common.enums.CommonStatus;
import cn.murky.admin.system.biz.domain.entity.SysDictData;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;

/**
 * 字典数据分页DTO
 */
@Data
@Accessors(chain = true)
public class SysDictDataPageDTO extends Page<SysDictData> {
    /**
     * 字典标签
     */
    @NotBlank
    private String dictLabel;

    /**
     * 字典类型
     */
    @NotBlank
    private String dictType;

    /**
     * 字典状态
     */
    private CommonStatus status;
}
