package cn.murky.admin.system.biz.domain.entity;

import cn.murky.common.domain.entity.BaseEntity;
import cn.murky.common.enums.DataScope;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 角色实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table("sys_role")
public class SysRole extends BaseEntity<SysRole> {
    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色码
     */
    private String roleCode;

    /**
     * 数据权限
     */
    private DataScope dataScope;

    /**
     * 部门Id
     */
    private Long fkDeptId;

    /**
     * 描述
     */
    private String describe;
}
