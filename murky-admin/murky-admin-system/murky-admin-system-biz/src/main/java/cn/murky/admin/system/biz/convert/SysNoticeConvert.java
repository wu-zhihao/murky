package cn.murky.admin.system.biz.convert;

import cn.murky.admin.system.biz.domain.dto.SysNoticeFromDTO;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * SysNotice实体转化接口
 *
 * @auth hans
 */
@Mapper
public interface SysNoticeConvert {
    SysNoticeConvert INSTANCES = Mappers.getMapper(SysNoticeConvert.class);

    @Mapping(target = "updateUser", ignore = true)
    @Mapping(target = "updateTime", ignore = true)
    @Mapping(target = "createUser", ignore = true)
    @Mapping(target = "createTime", ignore = true)
    SysNotice toEntity(SysNoticeFromDTO sysNoticeFromDTO);
}
