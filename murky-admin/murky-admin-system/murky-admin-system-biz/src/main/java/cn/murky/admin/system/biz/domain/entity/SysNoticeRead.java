package cn.murky.admin.system.biz.domain.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 通知已读记录表实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table(value = "sys_notice_read")
public class SysNoticeRead extends BaseEntity<SysNoticeRead> {
    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 通知id
     */
    private Long fkNoticeId;

}
