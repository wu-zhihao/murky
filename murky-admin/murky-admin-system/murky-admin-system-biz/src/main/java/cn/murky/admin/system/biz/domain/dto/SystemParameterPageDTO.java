package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.biz.domain.entity.SystemParameter;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 系统配置分页查询类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SystemParameterPageDTO extends Page<SystemParameter> {
    /**
     * 配置
     */
    private String key;
}
