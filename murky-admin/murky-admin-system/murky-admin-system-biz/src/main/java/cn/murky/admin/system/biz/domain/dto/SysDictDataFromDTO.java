package cn.murky.admin.system.biz.domain.dto;

import cn.murky.common.enums.CommonStatus;
import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * 字典数据表单DTO实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysDictDataFromDTO {

    /**
     * 字典编码
     */
    @NotNull(groups = Update.class)
    private Long dictCode;

    /**
     * 字典排序
     */
    private Short dictSort;

    /**
     * 字典类型
     */
    @NotBlank
    private String dictType;

    /**
     * 字典标签
     */
    @NotBlank
    private String dictLabel;

    /**
     * 字典值
     */
    @NotBlank
    private String dictValue;

    /**
     * 状态
     */
    private CommonStatus status;

    /**
     * 是否需要修改对应国际化语言
     */
    private Boolean editI18n;

    /**
     * 字典国际化
     */
    private SysI18nFromDTO sysI18nFromDTO;
    /**
     * 备注
     */
    private String remark;
}
