package cn.murky.admin.system.biz.domain.entity;

import cn.murky.common.domain.entity.BaseEntity;
import cn.murky.common.enums.CommonStatus;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 字典类型实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table(value = "sys_dict_type")
public class SysDictType extends BaseEntity<SysDictType> implements Serializable {

    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典状态
     */
    private CommonStatus status;

    /**
     * 备注
     */
    private String remark;
}
