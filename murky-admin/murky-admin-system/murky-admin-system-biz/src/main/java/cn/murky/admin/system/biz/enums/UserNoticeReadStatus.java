package cn.murky.admin.system.biz.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

/**
 * 用户通知读取状态
 * 0：已读
 * 1：未读
 */
@AllArgsConstructor
@Getter
public enum UserNoticeReadStatus {
    READ(0,"已读"),
    UNREAD(1,"未读"),
    ;
    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
