package cn.murky.admin.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.system.biz.convert.SysNoticeConvert;
import cn.murky.admin.system.biz.domain.dto.SysNoticeFromDTO;
import cn.murky.admin.system.biz.domain.dto.SysNoticePageDTO;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import cn.murky.admin.system.biz.domain.entity.table.SysNoticeTableDef;
import cn.murky.admin.system.biz.domain.entity.table.SysUserTableDef;
import cn.murky.admin.system.biz.domain.vo.SysNoticePageVO;
import cn.murky.admin.system.biz.service.ISysNoticeService;
import cn.murky.common.utils.CollectionUtils;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryWrapper;
import org.noear.folkmq.client.MqClient;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

import static cn.murky.admin.system.api.constant.SysNoticeError.SYS_NOTICE_NOT_EXIST;

/***
 * 通知管理
 *
 * @author hans
 */
@Valid
@Mapping("sysNotice")
@Controller
public class SysNoticeController extends BaseController<ISysNoticeService> {
    @Inject
    private MqClient mqClient;
    /**
     * 获取通知公告分页列表
     */
    @SaCheckPermission("sys:notice")
    @Mapping("/page")
    @Get
    public ApiResult<Page<SysNoticePageVO>> list(SysNoticePageDTO sysNoticePageDTO)
    {
        SysNoticeTableDef SYS_NOTICE = SysNoticeTableDef.SYS_NOTICE;
        SysUserTableDef SYS_USER = SysUserTableDef.SYS_USER;
        QueryWrapper queryWrapper = QueryWrapper.create()
                .leftJoin(SYS_USER).on(SYS_NOTICE.CREATE_USER.eq(SYS_USER.ID))
                .and(SYS_NOTICE.TITLE.like(sysNoticePageDTO.getTitle(), If::hasText))
                .and(SYS_NOTICE.TYPE.eq(sysNoticePageDTO.getType(), If::notNull))
                .and(SYS_NOTICE.TARGET.eq(sysNoticePageDTO.getTarget(), If::notNull))
                .and(SYS_NOTICE.EXPIRE.le(sysNoticePageDTO.getExpire(), If::notNull))
                .and(SYS_USER.USER_NAME.like(sysNoticePageDTO.getOperators(), If::hasText))
                .orderBy(SYS_NOTICE.CREATE_TIME.desc());
        Page<SysNoticePageVO> result = baseService.pageAs(sysNoticePageDTO, queryWrapper, SysNoticePageVO.class);
        return ApiResult.ok(result);
    }

    /**
     * 根据通知公告编号获取详细信息
     */
    @SaCheckPermission("sys:notice")
    @Mapping(value = "/{id}")
    @Get
    public ApiResult<SysNotice> info(Long id)
    {
        return ApiResult.ok(baseService.getById(id));
    }

    /**
     * 新增通知公告
     */
    @SaCheckPermission("sys:notice:add")
    @Mapping
    @Post
    public ApiResult<?> add(@Body @Validated SysNoticeFromDTO sysNoticeFromDTO)
    {
        SysNotice entity = SysNoticeConvert.INSTANCES.toEntity(sysNoticeFromDTO);
        return toResult(baseService.save(entity));
    }

    /**
     * 修改通知公告
     */
    @SaCheckPermission("sys:notice:edit")
    @Mapping
    @Put
    public ApiResult<?> edit(@Validated @Body SysNoticeFromDTO sysNoticeFromDTO)
    {
        SysNotice entity = SysNoticeConvert.INSTANCES.toEntity(sysNoticeFromDTO);
        return toResult(baseService.updateById(entity));
    }

    /**
     * 删除通知公告
     */
    @SaCheckPermission("sys:notice:remove")
    @Mapping("/{ids}")
    @Delete
    public ApiResult<?> remove(Long[] ids)
    {
        List<Long> list = Arrays.asList(ids);
        List<SysNotice> sysNotices = baseService.listByIds(list);
        if (CollectionUtils.isEmpty(sysNotices)){
            return ApiResult.fail(SYS_NOTICE_NOT_EXIST);
        }
        return toResult(baseService.removeByIds(list));
    }
}
