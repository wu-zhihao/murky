package cn.murky.admin.system.biz.domain.dto;

import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * 系统参数DTO实体类
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SystemParameterDTO {
    /**
     * id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 参数key
     */
    @NotBlank
    private String key;

    /**
     * 参数值
     */
    @NotBlank
    private String value;

    /**
     * 描述
     */
    private String describe;
}
