package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.api.enums.SysNoticeTarget;
import cn.murky.admin.system.api.enums.SysNoticeType;
import cn.murky.common.enums.CommonStatus;
import cn.murky.core.validat.Update;
import com.mybatisflex.annotation.Id;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotNull;

import java.time.OffsetDateTime;

/**
 * 通知表单类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysNoticeFromDTO {
    /**
     * 菜单id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 标题
     */
    @NotNull
    private String title;

    /**
     * 公告类型（0通知 1公告）
     */
    @NotNull
    private SysNoticeType type;

    /**
     * 通知目标（0admin 1tenant）
     */
    @NotNull
    private SysNoticeTarget target;

    /**
     * 内容
     */
    private String content;

    /**
     * 通用状态（0正常 1停用）
     */
    @NotNull
    private CommonStatus status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 到期时间
     */
    private OffsetDateTime expire;
}
