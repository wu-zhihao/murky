package cn.murky.admin.system.biz.convert;

import cn.murky.admin.system.api.domian.bo.SysI18nBO;
import cn.murky.admin.system.biz.domain.entity.SysDictData;
import cn.murky.admin.system.biz.domain.entity.SysI18n;
import cn.murky.common.domain.bo.SysDictDataBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SysI18nConvert {
    SysI18nConvert INSTANCES = Mappers.getMapper(SysI18nConvert.class);


    SysI18nBO toEntity(SysI18n sysI18n);
    List<SysI18nBO> toEntitys(List<SysI18n> sysI18n);

}
