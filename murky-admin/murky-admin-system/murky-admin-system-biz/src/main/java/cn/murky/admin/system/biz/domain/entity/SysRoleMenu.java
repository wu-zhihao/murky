package cn.murky.admin.system.biz.domain.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/***
 * 角色菜单关系实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table("sys_role_menu")
public class SysRoleMenu {
    /**
     * 角色id
     */
    private Long fkRoleId;

    /**
     * 菜单id
     */
    private Long fkMenuId;

}
