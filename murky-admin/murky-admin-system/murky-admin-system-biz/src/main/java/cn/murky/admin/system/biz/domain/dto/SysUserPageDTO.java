package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.biz.domain.entity.SysUser;
import cn.murky.admin.system.api.enums.Sex;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户分页查询类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysUserPageDTO extends Page<SysUser> {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Sex sex;

    /**
     * 部门id
     */
    private Long fkDeptId;
}
