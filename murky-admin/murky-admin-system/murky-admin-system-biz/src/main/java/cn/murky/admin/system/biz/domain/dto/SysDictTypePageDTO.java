package cn.murky.admin.system.biz.domain.dto;

import cn.murky.common.enums.CommonStatus;
import cn.murky.admin.system.biz.domain.entity.SysDictType;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典类型分页DTO实体
 */
@Data
@Accessors(chain = true)
public class SysDictTypePageDTO extends Page<SysDictType> {

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典状态
     */
    private CommonStatus status;
}
