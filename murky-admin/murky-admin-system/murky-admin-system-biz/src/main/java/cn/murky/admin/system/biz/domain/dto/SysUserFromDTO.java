package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.biz.convert.SysUserConvert;
import cn.murky.admin.system.api.enums.Sex;
import cn.murky.admin.system.biz.domain.entity.SysUser;
import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.Email;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * 用户表单类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysUserFromDTO {
    /**
     * 用户id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 用户名
     */
    @NotBlank
    private String userName;

    /**
     * 账号
     */
    @NotBlank
    private String account;

    /**
     * 性别 0:男性 1:女性 2:其他
     */
    @NotNull
    private Sex sex;

    /**
     * 邮箱
     */
    @Email
    private String email;

    /**
     * 部门id
     */
    private Long fkDeptId;

    /**
     * 角色id
     */
    private Long fkRoleId;

    public SysUser toEntity(){
        return SysUserConvert.INSTANCES.toEntity(this);
    }

}
