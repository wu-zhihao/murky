package cn.murky.admin.system.biz.service;

import cn.murky.admin.system.biz.domain.dto.UserNoticePageDTO;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import cn.murky.admin.system.biz.domain.vo.UserNoticePageVO;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;

public interface ISysNoticeService extends IService<SysNotice> {


    /**
     * 获取有效通知
     *
     * @return 有效通知列表
     */
    Page<UserNoticePageVO> getEffectiveList(UserNoticePageDTO dto);

    /**
     * 添加通知
     *
     * @return 是否添加成功
     */
    boolean save(SysNotice sysNotice);

    /**
     * 修改通知
     *
     * @return 是否修改成功
     */
    boolean updateById(SysNotice sysNotice);
}
