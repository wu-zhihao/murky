package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.biz.convert.SysDeptConvert;
import cn.murky.admin.system.biz.domain.entity.SysDept;
import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * 部门表单DTO实体类
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysDeptFromDTO {
    /**
     * 部门id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 部门名称
     */
    @NotBlank
    private String deptName;

    /**
     * 父级部门id
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;
    public SysDept toEntity() {
        return SysDeptConvert.INSTANCES.toEntity(this);
    }
}
