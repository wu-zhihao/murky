package cn.murky.admin.system.biz.service.impl;

import cn.murky.admin.system.biz.domain.bo.SysNoticeReadBO;
import cn.murky.admin.system.biz.domain.entity.SysNoticeRead;
import cn.murky.admin.system.biz.mapper.SysNoticeReadMapper;
import cn.murky.admin.system.biz.service.ISysNoticeReadService;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;

/**
 * SysNoticeReadServiceImpl
 *
 * @auth hans
 */
@Component
public class SysNoticeReadServiceImpl extends ServiceImpl<SysNoticeReadMapper, SysNoticeRead> implements ISysNoticeReadService {

    @Tran
    @Override
    public boolean read(SysNoticeReadBO sysNoticeReadBO) {
        List<SysNoticeRead> list = sysNoticeReadBO
                .getFkNoticeIds()
                .stream()
                .map(noticeId -> {
                            SysNoticeRead sysNoticeRead = new SysNoticeRead();
                            sysNoticeRead.setFkNoticeId(noticeId);
                            sysNoticeRead.setCreateUser(sysNoticeReadBO.getFkUserId());
                            return sysNoticeRead;
                        }
                ).toList();
        return saveBatch(list);
    }

    @Override
    public boolean unread(SysNoticeReadBO sysNoticeReadBO) {
        int i = mapper.deleteNoticeIdAndCreateUser(sysNoticeReadBO.getFkNoticeIds(), sysNoticeReadBO.getFkUserId());
        return i > 0;
    }

    @Override
    public boolean unread(Long noticeId) {
        int i = mapper.deleteNoticeId(noticeId);
        return i > 0;
    }
}
