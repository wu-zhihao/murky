package cn.murky.admin.system.biz.service.impl;

import cn.murky.admin.system.api.enums.I18nTag;
import cn.murky.admin.system.biz.convert.SysDictConvert;
import cn.murky.admin.system.biz.domain.dto.SysDictDataFromDTO;
import cn.murky.admin.system.biz.domain.dto.SysI18nDTO;
import cn.murky.admin.system.biz.domain.vo.SysDictDataInfoVO;
import cn.murky.admin.system.biz.domain.vo.SysI18nVO;
import cn.murky.admin.system.biz.mapper.SysI18nMapper;
import cn.murky.admin.system.biz.service.ISysDictDataService;
import cn.murky.admin.system.biz.service.ISysI18nService;
import cn.murky.common.constant.DictContant;
import cn.murky.admin.system.biz.domain.entity.SysDictData;
import cn.murky.admin.system.biz.mapper.SysDictDataMapper;
import cn.murky.admin.system.biz.mapper.SysDictTypeMapper;
import cn.murky.admin.system.biz.service.ISysDictTypeService;
import cn.murky.common.domain.bo.SysDictDataBO;
import cn.murky.common.utils.StringUtils;
import cn.murky.core.exception.ServiceException;
import com.mybatisflex.core.util.SqlUtil;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.redisx.RedisClient;
import org.noear.redisx.plus.RedisHash;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.annotation.Tran;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static cn.murky.core.constant.ErrorConstant.DELETE_ERROR;

/**
 * 字典数据Service
 *
 * @auth hans
 */
@Component
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements ISysDictDataService {
    @Inject
    private RedisClient redisClient;
    @Inject
    private SysDictTypeMapper sysDictTypeMapper;
    @Inject
    private ISysDictTypeService iSysDictTypeService;
    @Inject
    private ISysI18nService iSysI18nService;
    @Inject
    private SysI18nMapper sysI18nMapper;
    /**
     * 重写save方法加入缓存机制
     *
     * @param sysDictData
     * @return
     */
    @Tran
    @Override
    public boolean save(SysDictData sysDictData) {
        boolean bool = SqlUtil.toBool(this.getMapper().insert(sysDictData, true));

        if (bool) {
            RedisHash redisHash = redisClient.getHash(DictContant.DICT_CACHE_KEY);
            List<SysDictDataBO> dictList = redisHash.getAndDeserialize(sysDictData.getDictType(), (new ArrayList<SysDictDataBO>() {
            }).getClass());
            dictList = Optional.ofNullable(dictList).orElseGet(ArrayList::new);
            SysDictDataBO bo = SysDictConvert.INSTANCES.toBO(sysDictData);
            dictList.add(bo);
            dictList.sort(Comparator.comparing(SysDictDataBO::getDictSort));
            redisHash.putAndSerialize(sysDictData.getDictType(), dictList);
        }
        return bool;
    }

    /**
     * 重写修改方法加入缓存机制
     *
     * @param sysDictData
     * @return
     */
    @Tran
    @Override
    public boolean updateById(SysDictData sysDictData) {
        String dictType = sysDictData.getDictType();
        sysDictData.setDictType(null);
        boolean bool = this.updateById(sysDictData, true);
        if (bool) {
            RedisHash redisHash = redisClient.getHash(DictContant.DICT_CACHE_KEY);
            List<SysDictDataBO> dictList = redisHash.getAndDeserialize(dictType, (new ArrayList<SysDictDataBO>() {
            }).getClass());
            dictList.forEach(item -> {
                if (item.getDictCode().equals(sysDictData.getDictCode())) {
                    SysDictDataBO bo = SysDictConvert.INSTANCES.toBO(sysDictData);
                    item = bo;
                }
            });
            dictList.sort(Comparator.comparing(SysDictDataBO::getDictSort));
            redisHash.putAndSerialize(dictType, dictList);
        }
        return bool;
    }


    @Tran
    @Override
    public boolean removeById(Serializable id) {
        SysDictData sysDictData = mapper.selectOneById(id);
        boolean bool = SqlUtil.toBool(this.getMapper().deleteById(id));
        if (bool) {
            RedisHash redisHash = redisClient.getHash(DictContant.DICT_CACHE_KEY);
            List<SysDictData> dictList = redisHash.getAndDeserialize(sysDictData.getDictType(), (new ArrayList<SysDictData>() {
            }).getClass());
            dictList = dictList.stream().filter(item -> !item.getDictCode().equals(id)).collect(Collectors.toList());
            redisHash.putAndSerialize(sysDictData.getDictType(), dictList);
        }
        return bool;
    }

    @Override
    public List<SysDictDataBO> getI18nDict() {
        return getDict(DictContant.I18N_LANGUAGE_DICT_KEY);
    }

    @Override
    public SysDictDataInfoVO info(Long id) {
        SysDictData sysDictData = mapper.selectOneById(id);
        SysDictDataInfoVO vo = SysDictConvert.INSTANCES.toVO(sysDictData);
        SysI18nVO i18n = iSysI18nService.info(new SysI18nDTO().setI18nKey(sysDictData.getDictLabel()).setI18nTag(I18nTag.COMMON));
        vo.setSysI18nVO(i18n);
        return vo;
    }

    @Override
    public List<SysDictDataBO> getDict(String dictType) {
        RedisHash redisHash = redisClient.getHash(DictContant.DICT_CACHE_KEY);
        if (redisHash != null) {
            return redisHash.getAndDeserialize(dictType, (new ArrayList<SysDictDataBO>() {
            }).getClass());
        }
        return sysDictTypeMapper.selectSysDict(dictType).getSysDictDataList();
    }

    @Override
    public List<SysDictDataBO> getAllDict() {
        RedisHash redisHash = redisClient.getHash(DictContant.DICT_CACHE_KEY);
        List<SysDictDataBO> result = new ArrayList<>();
        if (redisHash != null) {
            for (String key : redisHash.keySet()) {
                List<SysDictDataBO> sysDictDataList = redisHash.getAndDeserialize(key, (new ArrayList<SysDictDataBO>() {
                }).getClass());
                result.addAll(sysDictDataList);
            }
        }
        return result;
    }

    @Tran
    @Override
    public boolean updateDict(SysDictDataFromDTO dto) {
        SysDictData sysDictData = SysDictConvert.INSTANCES.toEntity(dto);
        Optional<SysDictData> entity = getByIdOpt(dto.getDictCode());
        boolean b = updateById(sysDictData);
        if (b) {
            if(StringUtils.isNotEmpty(dto.getSysI18nFromDTO().getI18nKey())){
                iSysI18nService.edit(dto.getSysI18nFromDTO());
            }else{
                sysI18nMapper.updateI18nKeyByKey(entity.get().getDictLabel(), dto.getDictLabel());
                iSysI18nService.remove(dto.getDictLabel(), dto.getSysI18nFromDTO().getI18nTag());
            }
            iSysDictTypeService.refreshDict();
        }
        return b;
    }

    @Tran
    @Override
    public boolean addDict(SysDictDataFromDTO sysDictDataFromDTO) {
        SysDictData sysDictData = SysDictConvert.INSTANCES.toEntity(sysDictDataFromDTO);
        boolean b = save(sysDictData);
        if (b) {
            if(StringUtils.isNotEmpty(sysDictDataFromDTO.getSysI18nFromDTO().getI18nKey())){
                iSysI18nService.save(sysDictDataFromDTO.getSysI18nFromDTO());
            }
            iSysDictTypeService.refreshDict();
        }
        return b;
    }

    @Tran
    @Override
    public boolean removeDict(Long id) {
        SysDictData sysDictData = getById(id);
        Optional.ofNullable(sysDictData).orElseThrow(() -> new ServiceException(DELETE_ERROR));
        boolean b = removeById(id);
        if (b) {
            iSysI18nService.remove(sysDictData.getDictLabel(), I18nTag.COMMON);
            iSysDictTypeService.refreshDict();
        }
        return b;
    }
}
