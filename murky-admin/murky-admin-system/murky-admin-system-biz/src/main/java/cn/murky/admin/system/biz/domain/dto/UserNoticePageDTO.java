package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.api.enums.SysNoticeTarget;
import cn.murky.admin.system.api.enums.SysNoticeType;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import cn.murky.admin.system.biz.domain.vo.UserNoticePageVO;
import cn.murky.admin.system.biz.enums.UserNoticeReadStatus;
import cn.murky.common.enums.CommonStatus;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/**
 * 通知中心请求参数实体
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class UserNoticePageDTO extends Page<UserNoticePageVO> {
    /**
     * 标题
     */
    private String title;

    /**
     * 公告类型（0通知 1公告）
     */
    private SysNoticeType type;

    /**
     * 用户通知读取状态（0已读 1未读）
     */
    private UserNoticeReadStatus readStatus;

}
