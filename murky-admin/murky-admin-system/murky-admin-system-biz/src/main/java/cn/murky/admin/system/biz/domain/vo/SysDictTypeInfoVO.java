package cn.murky.admin.system.biz.domain.vo;

import cn.murky.common.enums.CommonStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

@Data
@Accessors(chain = true)
public class SysDictTypeInfoVO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典状态
     */
    private CommonStatus status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 国际化信息
     */
    private SysI18nVO sysI18nVO;

    /**
     * 创建时间
     */
    protected OffsetDateTime createTime;

    /**
     * 修改时间
     */
    protected OffsetDateTime updateTime;

    /**
     * 创建人
     */
    protected Long createUser;

    /**
     * 修改人
     */
    protected Long updateUser;
}
