package cn.murky.admin.system.biz.mapper;

import cn.murky.admin.system.api.enums.I18nTag;
import cn.murky.admin.system.biz.domain.entity.table.SysI18nTableDef;
import cn.murky.admin.system.biz.domain.vo.SysI18nVO;
import cn.murky.admin.system.biz.domain.dto.SysI18nDTO;
import cn.murky.admin.system.biz.domain.entity.SysI18n;
import cn.murky.admin.system.biz.domain.query.SysI18nPageQuery;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.util.UpdateEntity;

import java.util.List;
import java.util.Map;

import static com.mybatisflex.core.query.QueryMethods.count;

public interface SysI18nMapper extends BaseMapper<SysI18n> {

    /**
     * I18n分页mapper
     */
    default Page<Map> page(SysI18nPageQuery sysI18nPageQuery) {
        SysI18nDTO sysI18nDTO = sysI18nPageQuery.getSysI18nDTO();
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        QueryWrapper queryWrapper = QueryWrapper.create().from(SYS_I18N)
                .where(SYS_I18N.I18N_TAG.eq(sysI18nDTO.getI18nTag()))
                .and(SYS_I18N.I18N_KEY.like(sysI18nDTO.getI18nKey(), If::hasText))
                .and(SYS_I18N.I18N_VALUE.like(sysI18nDTO.getI18nValue(), If::hasText))
                .groupBy(SYS_I18N.I18N_KEY, SYS_I18N.I18N_TAG).orderBy(SYS_I18N.I18N_KEY.desc())
                .select(SYS_I18N.I18N_KEY.as("i18nKey"), SYS_I18N.I18N_TAG.as("i18nTag"));
        for (String i18nKey : sysI18nPageQuery.getI18nKeys()) {
            String sql = STR."""
                            STRING_AGG(case when language = '\{i18nKey}' then i18n_value else null end,',' ) as "\{i18nKey}"
                            """;
            queryWrapper.select(sql);
        }
        return paginateAs(sysI18nDTO.getPageNumber(), sysI18nDTO.getPageSize(), queryWrapper, Map.class);
    }


    /**
     * I18n详情数据
     */
    default SysI18nVO info(SysI18nPageQuery sysI18nPageQuery) {
        SysI18nDTO sysI18nDTO = sysI18nPageQuery.getSysI18nDTO();
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        QueryWrapper queryWrapper = QueryWrapper.create().from(SYS_I18N)
                .where(SYS_I18N.I18N_TAG.eq(sysI18nDTO.getI18nTag()))
                .and(SYS_I18N.I18N_KEY.eq(sysI18nDTO.getI18nKey()));

        List<SysI18n> sysI18nList = selectListByQuery(queryWrapper);
        SysI18nVO sysI18nVo = new SysI18nVO();
        if (!sysI18nList.isEmpty()) {
            sysI18nVo.setId(sysI18nList.getFirst().getId())
                    .setI18nKey(sysI18nList.getFirst().getI18nKey())
                    .setI18nTag(sysI18nList.getFirst().getI18nTag())
                    .setI18nInputs(sysI18nList);
        }
        return sysI18nVo;
    }

    /**
     * 根据i18nKey, i18nTag查询数量
     */
    default long selectByKeyAndExceptTag(String i18nKey, I18nTag i18nTag) {
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(count(SYS_I18N.ID))
                .where(SYS_I18N.I18N_KEY.eq(i18nKey))
                .and(SYS_I18N.I18N_TAG.in(i18nTag, I18nTag.COMMON))
                ;
        return selectCountByQuery(queryWrapper);
    }


    /**
     * 根据i18n,i18nKey,i18nTag修改i18nValue
     *
     * @return 受影响行
     */
    default int updateI18nValue(String key, SysI18n sysI18n) {
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        return updateByQuery(sysI18n, QueryWrapper.create()
                .where(SYS_I18N.I18N_KEY.eq(key)).and(SYS_I18N.LANGUAGE.eq(sysI18n.getLanguage())))
                ;
    }

    /**
     * 根据key修改key
     *
     * @return 受影响行
     */
    default int updateI18nKeyByKey(String key1, String key2) {
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        SysI18n sysI18n = UpdateEntity.of(SysI18n.class);
        sysI18n.setI18nKey(key2);
        return updateByQuery(sysI18n,QueryWrapper.create()
                .where(SYS_I18N.I18N_KEY.eq(key1)))
                ;
    }

    /**
     * 根据 i18nKey,i18nTag删除
     *
     * @return 受影响行
     */
    default int deleteByI18nKeyAndTag(String i18nKey,I18nTag i18nTag) {
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        return this.deleteByQuery(QueryWrapper.create()
                .where(SYS_I18N.I18N_KEY.eq(i18nKey)).and(SYS_I18N.I18N_TAG.eq(i18nTag)));
    }


    /**
     * 根据Language和Tag查询
     * @return 相关数据
     */
    default List<SysI18n> selectByLanguageAndTag(String i18nTag, String language) {
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        return this.selectListByQuery(QueryWrapper.create()
                .where(SYS_I18N.LANGUAGE.eq(language))
                .and(SYS_I18N.I18N_TAG.eq(i18nTag))
        );
    }

    /**
     * 根据i18nKey和Tag查询
     * @param i18nKey key
     * @param i18nTag 标签
     * @return 相关数据
     */
    default List<SysI18n> selectByTagAndKey(String i18nKey, I18nTag i18nTag) {
        SysI18nTableDef SYS_I18N = SysI18nTableDef.SYS_I18N;
        return this.selectListByQuery(QueryWrapper.create()
                .where(SYS_I18N.I18N_KEY.eq(i18nKey))
                .and(SYS_I18N.I18N_TAG.eq(i18nTag))
        );
    }

}
