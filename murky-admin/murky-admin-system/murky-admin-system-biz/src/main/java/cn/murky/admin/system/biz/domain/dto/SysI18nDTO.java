package cn.murky.admin.system.biz.domain.dto;

import cn.murky.admin.system.api.enums.I18nTag;
import cn.murky.admin.system.biz.domain.entity.SysI18n;
import cn.murky.core.validat.Info;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * i18n分页DTO实体
 */
@Data
@Accessors(chain = true)
public class SysI18nDTO extends Page<SysI18n> {
    /**
     * i18n标签(字典：i18n:tag)
     */
    @NotNull
    private I18nTag i18nTag;

    /**
     * i18nValue
     */
    private String i18nValue;

    /**
     * i18nKey
     */
    @NotBlank(groups = Info.class)
    private String i18nKey;

}
