package cn.murky.admin.system.biz.domain.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotEmpty;

import java.util.List;


/**
 * 部门拖动排序接口参数实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class SysDeptDropDTO {
    /**
     * 父级部门id
     */
    private Long parentId;

    /**
     * 部门id集合
     */
    @NotEmpty
    private List<Long> deptIds;
}
