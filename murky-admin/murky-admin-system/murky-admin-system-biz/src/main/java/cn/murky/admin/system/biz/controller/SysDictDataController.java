package cn.murky.admin.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.system.biz.domain.dto.SysDictDataFromDTO;
import cn.murky.admin.system.biz.domain.dto.SysDictDataPageDTO;
import cn.murky.admin.system.biz.convert.SysDictConvert;
import cn.murky.admin.system.biz.domain.entity.SysDictData;
import cn.murky.admin.system.biz.domain.entity.table.SysDictDataTableDef;
import cn.murky.admin.system.biz.service.ISysDictDataService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryWrapper;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
 * 字典数据管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("dictData")
public class SysDictDataController extends BaseController<ISysDictDataService> {
    /**
     * 字典数据分页查询
     * @param sysDictDataPageDTO sysDictDataPageDTO
     * @return SysDictData
     */
    @Get
    @Mapping("page")
    @SaCheckPermission("dict")
    public ApiResult<Page<SysDictData>> page(SysDictDataPageDTO sysDictDataPageDTO) {
        SysDictDataTableDef SYS_DICT_DATA = SysDictDataTableDef.SYS_DICT_DATA;
        Page<SysDictData> result = baseService.page(sysDictDataPageDTO,
                QueryWrapper.create()
                        .and(SYS_DICT_DATA.DICT_TYPE.eq(sysDictDataPageDTO.getDictType()))
                        .and(SYS_DICT_DATA.DICT_LABEL.like(sysDictDataPageDTO.getDictLabel(), If::hasText))
                        .and(SYS_DICT_DATA.STATUS.eq(sysDictDataPageDTO.getStatus(), If::notNull))
                        .orderBy(SYS_DICT_DATA.DICT_SORT.asc(),SYS_DICT_DATA.CREATE_TIME.asc())
        );
        return ApiResult.ok(result);
    }

    /**
     * 获取指定字典数据
     * @param dictType 字典类型
     * @return List<SysDictData>
     */
    @Get
    @Mapping("dict/{dictType}")
    public ApiResult<List<SysDictData>> list(String dictType) {
        List<SysDictData> result = SysDictConvert.INSTANCES.toEntity(baseService.getDict(dictType));
        return ApiResult.ok(result);
    }

    /**
     * 字典数据详情信息
     * @param id 字典id
     * @return SysDictData
     */
    @Get
    @Mapping("{id}")
    @SaCheckPermission("dict")
    public ApiResult<?> info(Long id) {
        return ApiResult.ok(baseService.info(id));
    }

    /**
     * 新增字典数据
     * @param sysDictDataFromDTO sysDictDataFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("dict:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) SysDictDataFromDTO sysDictDataFromDTO) {
        return toResult(baseService.addDict(sysDictDataFromDTO));
    }

    /**
     * 修改字典数据
     * @param sysDictDataFromDTO sysDictDataFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("dict:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) SysDictDataFromDTO sysDictDataFromDTO) {
        return toResult(baseService.updateDict(sysDictDataFromDTO));
    }

    /**
     * 删除字典数据
     * @param dictDataId 数据字典id
     */
    @Delete
    @Mapping("{dictDataId}")
    @SaCheckPermission("dict:remove")
    public ApiResult<?> remove(Long dictDataId) {
        return toResult(baseService.removeDict(dictDataId));
    }

}
