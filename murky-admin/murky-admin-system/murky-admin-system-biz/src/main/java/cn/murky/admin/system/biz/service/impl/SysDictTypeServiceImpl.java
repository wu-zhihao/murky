package cn.murky.admin.system.biz.service.impl;

import cn.murky.admin.system.api.enums.I18nTag;
import cn.murky.admin.system.biz.domain.dto.SysDictTypeFromDTO;
import cn.murky.admin.system.biz.domain.dto.SysI18nDTO;
import cn.murky.admin.system.biz.domain.vo.SysDictTypeInfoVO;
import cn.murky.admin.system.biz.domain.vo.SysI18nVO;
import cn.murky.admin.system.biz.mapper.SysI18nMapper;
import cn.murky.admin.system.biz.service.ISysI18nService;
import cn.murky.common.constant.CommonErrorConstant;
import cn.murky.common.domain.bo.SysDictBO;
import cn.murky.common.constant.DictContant;
import cn.murky.admin.system.biz.convert.SysDictConvert;
import cn.murky.admin.system.biz.domain.entity.SysDictData;
import cn.murky.admin.system.biz.domain.entity.SysDictType;
import cn.murky.admin.system.biz.mapper.SysDictTypeMapper;
import cn.murky.admin.system.biz.service.ISysDictDataService;
import cn.murky.admin.system.biz.service.ISysDictTypeService;
import cn.murky.common.domain.bo.SysDictDataBO;
import cn.murky.common.utils.CollectionUtils;
import cn.murky.common.utils.StringUtils;
import cn.murky.core.exception.ServiceException;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.noear.redisx.RedisClient;
import org.noear.redisx.plus.RedisHash;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Init;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.annotation.Tran;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cn.murky.core.constant.ErrorConstant.DELETE_ERROR;
import static cn.murky.core.constant.ErrorConstant.EDIT_ERROR;

/**
 * 字典类型service
 *
 * @auth hans
 */
@Component
@Slf4j
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements ISysDictTypeService {

    @Inject
    private RedisClient redisClient;
    @Inject
    private ISysDictTypeService iSysDictTypeService;
    @Inject
    private ISysDictDataService iSysDictDataService;
    @Inject
    private ISysI18nService iSysI18nService;
    @Inject
    private SysI18nMapper sysI18nMapper;
    /**
     * 刷新缓存
     */
    @Override
    public void refreshDict() {
        initDict();
    }

    @Tran
    @Override
    public boolean save(SysDictTypeFromDTO sysDictTypeFromDTO) {
        SysDictType entity = SysDictConvert.INSTANCES.toEntity(sysDictTypeFromDTO);
        super.save(entity);
        if (StringUtils.isNotEmpty(sysDictTypeFromDTO.getSysI18nFromDTO().getI18nKey())) {
            iSysI18nService.save(sysDictTypeFromDTO.getSysI18nFromDTO());
        }
        return true;
    }


    /**
     * 重写修改方法
     */
    @Tran
    @Override
    public boolean edit(SysDictTypeFromDTO dto) {
        SysDictType dictType = iSysDictTypeService.getById(dto.getId());
        SysDictType sysDictType = SysDictConvert.INSTANCES.toEntity(dto);
        SysDictType entity = getByIdOpt(dto.getId()).orElseThrow(() -> new ServiceException(CommonErrorConstant.FAIL));
        // 修改字典类型
        boolean b = iSysDictTypeService.updateById(sysDictType);
        if (b) {
            // 修改对应的data
            List<SysDictDataBO> dictList = iSysDictDataService.getDict(dictType.getDictType());
            if(CollectionUtils.isNotEmpty(dictList)){
                for (SysDictDataBO sysDictData : dictList) {
                    sysDictData.setDictType(sysDictType.getDictType());
                }
                List<SysDictData> sysDictDataList = SysDictConvert.INSTANCES.toEntity(dictList);
                return iSysDictDataService.updateBatch(sysDictDataList);
            }
            // 修改对应的国际化语言
            if(StringUtils.isNotEmpty(dto.getSysI18nFromDTO().getI18nKey())){
                sysI18nMapper.updateI18nKeyByKey(entity.getDictName(),sysDictType.getDictName());
                iSysI18nService.edit(dto.getSysI18nFromDTO());
            }else{
                iSysI18nService.remove(dto.getDictName(),dto.getSysI18nFromDTO().getI18nTag());

            }
            return true;
        }
        throw new ServiceException(EDIT_ERROR);
    }

    @Override
    public boolean removeById(Long id) {
        SysDictType sysDictType = getById(id);
        Optional.ofNullable(sysDictType).orElseThrow(()->new ServiceException(DELETE_ERROR));
        super.removeById(id);
        iSysI18nService.remove(sysDictType.getDictName(), I18nTag.COMMON);
        return true;
    }

    @Override
    public SysDictTypeInfoVO info(Long id) {
        SysDictType sysDictType = mapper.selectOneById(id);
        SysDictTypeInfoVO vo = SysDictConvert.INSTANCES.toVO(sysDictType);
        SysI18nVO i18n = iSysI18nService.info(new SysI18nDTO().setI18nKey(sysDictType.getDictName()).setI18nTag(I18nTag.COMMON));
        vo.setSysI18nVO(i18n);
        return vo;
    }

    /**
     * 启动服务时初始化字典换粗
     */
    @Init
    public void initDict() {
        RedisHash redisHash = redisClient.getHash(DictContant.DICT_CACHE_KEY);
        // 如果已经被初始化过则不需要在初始化

        if(!redisHash.isEmpty() && redisClient.getLock("initDictLock").isLocked()){
            return;
        }
        boolean b = redisClient.getLock("initDictLock").tryLock();
        if(b){
            List<SysDictBO> sysDictBos = mapper.selectSysDict();
            Map<String,String> hashMap = new HashMap<>();
            for (SysDictBO sysDictBo : sysDictBos) {
                sysDictBo.getSysDictDataList().forEach(item->item.setDictType(sysDictBo.getDictType()));
                hashMap.put(sysDictBo.getDictType(), ONode.serialize(sysDictBo.getSysDictDataList()));
            }
            redisHash.putAll(hashMap);
            log.info("[SysDictTypeServiceImpl] -> 初始化字典缓存");
        }
    }


}
