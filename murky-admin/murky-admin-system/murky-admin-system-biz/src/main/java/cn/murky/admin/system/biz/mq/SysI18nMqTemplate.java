package cn.murky.admin.system.biz.mq;

import cn.murky.admin.system.biz.endpoint.NoticeEndpoint;
import cn.murky.folkmq.constant.SysGroupConstant;
import cn.murky.folkmq.utils.MurkyMqUtils;
import lombok.extern.slf4j.Slf4j;
import org.noear.redisx.RedisClient;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cloud.annotation.CloudEvent;
import org.noear.solon.cloud.model.Event;

import static cn.murky.folkmq.constant.SysTopicConstant.SYS_I18N_TOPIC;
import static cn.murky.folkmq.constant.SysTopicConstant.SYS_NOTICE_TOPIC;

@Component
@Slf4j
public class SysI18nMqTemplate {
    public void publish() {
        log.info("[SysI18nMqTemplate] -> 消费发布清空本地i18n缓存消息 -> topic:{}", SYS_I18N_TOPIC);
        Event event = new Event(SYS_I18N_TOPIC, "0")
                .group(SysGroupConstant.DEFAULT);
        MurkyMqUtils.publish(event,true);
    }

}
