package cn.murky.admin.system.biz.service.impl;

import cn.murky.admin.core.utils.SecurityUtils;
import cn.murky.admin.system.api.enums.SysNoticeTarget;
import cn.murky.admin.system.biz.domain.dto.UserNoticePageDTO;
import cn.murky.admin.system.biz.domain.entity.SysNotice;
import cn.murky.admin.system.biz.domain.entity.table.SysNoticeReadTableDef;
import cn.murky.admin.system.biz.domain.entity.table.SysNoticeTableDef;
import cn.murky.admin.system.biz.domain.vo.UserNoticePageVO;
import cn.murky.admin.system.biz.enums.UserNoticeReadStatus;
import cn.murky.admin.system.biz.mapper.SysNoticeMapper;
import cn.murky.admin.system.biz.mq.SysNoticeMqTemplate;
import cn.murky.admin.system.biz.service.ISysNoticeReadService;
import cn.murky.admin.system.biz.service.ISysNoticeService;
import cn.murky.common.enums.CommonStatus;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.folkmq.client.MqClient;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.time.OffsetDateTime;

import static com.mybatisflex.core.query.QueryMethods.case_;
import static com.mybatisflex.core.query.QueryMethods.number;

/**
 * 系统通知service
 *
 * @author hans
 */
@Component
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {

    @Inject
    private SysNoticeMqTemplate sysNoticeMqTemplate;
    @Inject
    private ISysNoticeReadService iSysNoticeReadService;
    @Inject
    private MqClient mqClient;
    @Override
    public Page<UserNoticePageVO> getEffectiveList(UserNoticePageDTO dto) {
        SysNoticeTableDef SYS_NOTICE = SysNoticeTableDef.SYS_NOTICE;
        SysNoticeReadTableDef SYS_NOTICE_READ = SysNoticeReadTableDef.SYS_NOTICE_READ;
        return pageAs(dto, QueryWrapper.create()
                .select(SYS_NOTICE.ALL_COLUMNS,
                        case_().when(SYS_NOTICE_READ.ID.isNull()).then(1)
                                .when(SYS_NOTICE_READ.ID.isNotNull()).then(0).end().as(UserNoticePageVO::getReadStatus)
                )
                .leftJoin(SYS_NOTICE_READ)
                .on(SYS_NOTICE.ID.eq(SYS_NOTICE_READ.FK_NOTICE_ID))
                .where(SYS_NOTICE.STATUS.eq(CommonStatus.NORMAL)
                .and(SYS_NOTICE.EXPIRE.ge(OffsetDateTime.now())
                        .or(SYS_NOTICE.EXPIRE.isNull())))
                .and(SYS_NOTICE.TARGET.eq(SysNoticeTarget.ADMIN))
                .and(SYS_NOTICE_READ.ID.isNull(dto.getReadStatus()!=null && UserNoticeReadStatus.UNREAD==dto.getReadStatus()))
                .and(SYS_NOTICE_READ.ID.isNotNull(dto.getReadStatus()!=null && UserNoticeReadStatus.READ==dto.getReadStatus()))
                .and(SYS_NOTICE_READ.CREATE_USER.eq(SecurityUtils.getUserId(),dto.getReadStatus()!=null&&UserNoticeReadStatus.UNREAD!=dto.getReadStatus()))
                .orderBy(SYS_NOTICE.CREATE_TIME.desc()), UserNoticePageVO.class);
    }

    @Override
    public boolean save(SysNotice sysNotice) {
        boolean save = super.save(sysNotice);
        if (save) {
            sysNoticeMqTemplate.publish(sysNotice);
        }
        return save;
    }

    @Override
    public boolean updateById(SysNotice sysNotice) {
        boolean b = super.updateById(sysNotice);
        if (b) {
            iSysNoticeReadService.unread(sysNotice.getId());
            sysNoticeMqTemplate.publish(sysNotice);
        }
        return b;
    }
}
