package cn.murky.admin.auth.controller;

import cn.murky.admin.auth.domain.dto.EditPasswordDTO;
import cn.murky.admin.system.api.SysUserApi;
import cn.murky.admin.system.api.domian.UserProfile;
import cn.murky.admin.system.api.domian.dto.ProfileFromDTO;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.admin.core.utils.SecurityUtils;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;

/***
 * 个人信息控制器
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("profile")
public class MurkyProfileController extends BaseController {
    @Inject
    private SysUserApi sysUserApi;

    /**
     * 修改语言偏好
     * @param language 语言
     */
    @Post
    @Mapping("language/{language}")
    public ApiResult<?> setLanguage(String language) {
        return toResult(sysUserApi.setLanguage(language));
    }

    /**
     * 获取用户展示信息
     * @return UserProfile
     */
    @Get
    @Mapping
    public ApiResult<UserProfile> info() {
        return ApiResult.ok(sysUserApi.getProfile(SecurityUtils.getUserId()));
    }

    /**
     * 修改用户信息
     * @param profileFromDTO profileFromDTO
     */
    @Put
    @Mapping
    public ApiResult<?> editProfile(@Body ProfileFromDTO profileFromDTO) {
        return toResult(sysUserApi.setProfile(profileFromDTO));
    }

    /**
     * 修改密码
     * @param editPasswordDTO editPasswordDTO
     */
    @Put
    @Mapping
    public ApiResult<?> editProfile(@Body EditPasswordDTO editPasswordDTO) {
        return toResult(sysUserApi.setPassword(editPasswordDTO.getOldPassword(),editPasswordDTO.getPassword(),editPasswordDTO.getSurePassword()));
    }

}
