package cn.murky.admin.auth.service.impl;

import cn.murky.admin.auth.service.MurkyCaptchaService;
import com.anji.captcha.service.impl.BlockPuzzleCaptchaServiceImpl;
import org.noear.solon.annotation.Component;


@Component(name="blockPuzzle")
public class MurkyBlockPuzzleCaptchaServiceImpl extends BlockPuzzleCaptchaServiceImpl implements MurkyCaptchaService {

}
