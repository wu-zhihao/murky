package cn.murky.admin.auth.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.murky.admin.auth.domain.dto.LoginDto;
import cn.murky.admin.auth.service.IMurkyLoginService;
import cn.murky.security.utils.CaptchaUtil;
import cn.murky.common.constant.CommonErrorConstant;
import cn.murky.security.entity.SecurityUserInfo;
import cn.murky.admin.system.api.SysMenuApi;
import cn.murky.admin.system.api.domian.SysMenuTree;
import cn.murky.admin.system.api.enums.MenuType;
import cn.murky.common.web.ApiResult;
import cn.murky.admin.core.utils.SecurityUtils;
import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Context;
import org.noear.solon.validation.annotation.Valid;

import java.util.Arrays;
import java.util.List;

import static com.anji.captcha.controller.CaptchaController.getRemoteId;

/***
 * 安全控制器
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("auth")
public class MurkyAuthController {

    @Inject
    private IMurkyLoginService iMurkyLoginService;
    @Inject
    private SysMenuApi sysMenuApi;
    @Inject
    private CaptchaService captchaService;

    @Post
    @Mapping("/captcha")
    public ApiResult<Object> check(CaptchaVO data, Context request) {
        data.setBrowserInfo(getRemoteId(request));
        ResponseModel responseModel = this.captchaService.check(data);
        return CaptchaUtil.parseApiResult(responseModel);
    }

    /**
     * 获取图形验证码信息
     * @return token
     */
    @Get
    @Mapping("/captcha")
    public ApiResult<Object> getCaptcha(CaptchaVO data, Context request) {
        assert request.realIp() != null;

        data.setBrowserInfo(getRemoteId(request));

        ResponseModel responseModel = captchaService.get(data);
        return CaptchaUtil.parseApiResult(responseModel);
    }

    /**
     * 登录
     * @param loginDto loginDto
     * @return token
     */
    @Post
    @Mapping("login")
    public ApiResult<Object> login(@Body LoginDto loginDto) {
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(loginDto.getCaptchaVerification());
        ResponseModel response = captchaService.verification(captchaVO);
        ApiResult<Object> apiResult = CaptchaUtil.parseApiResult(response);
        if(CommonErrorConstant.SUCCESS.errCode().equals(apiResult.getCode())){
            SaTokenInfo tokenInfo = iMurkyLoginService.login(loginDto);
            return ApiResult.ok(tokenInfo.getTokenValue());
        }
        return apiResult;
    }

    /**
     * 登出
     */
    @Post
    @Mapping("logout")
    public ApiResult<?> logout() {
//        SecurityUtils.delUserMenu();
        SecurityUtils.delUserInfo();
        StpUtil.logout();
        return ApiResult.ok();
    }

    /**
     * 获取菜单
     * @return List<SysMenuTree>
     */
    @Get
    @Mapping("menu")
    public ApiResult<List<SysMenuTree>> menu() {
        return ApiResult.ok(sysMenuApi.treeSysMenu(Arrays.asList(MenuType.MENU, MenuType.DIRECTORY)));
    }

    /**
     * 获取用户信息
     * @return SecurityUserInfo
     */
    @Get
    @Mapping("info")
    public ApiResult<SecurityUserInfo> userInfo() {
        return ApiResult.ok(iMurkyLoginService.userInfo());
    }
}
