package cn.murky.admin.auth.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * 登录用户信息视图类
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class LoginUserInfoVO {
    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 登录token
     */
    private String token;

    /**
     * 角色ID
     */
    private Set<Long> roleId;

    /**
     * 权限码
     */
    private Set<String> permissions;

}
