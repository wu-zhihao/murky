package cn.murky.admin.auth.domain.dto;

import lombok.Data;
import org.noear.solon.validation.annotation.NotBlank;

/**
 * 登录DTO
 */
@Data
public class LoginDto {
    /**
     * 账号
     */
    @NotBlank
    private String account;

    /**
     * 密码
     */
    @NotBlank
    private String password;

    /**
     * 图形验证码
     */
    private String captchaVerification;
}
