package cn.murky.admin.tenant.api;

import cn.murky.admin.system.api.domian.bo.SysI18nBO;
import cn.murky.common.domain.bo.SysDictBO;

import java.util.List;

public interface TenantEnvApi {
    /**
     * 刷新字典
     */
    void refreshDict(List<SysDictBO> sysDictBos);
}
