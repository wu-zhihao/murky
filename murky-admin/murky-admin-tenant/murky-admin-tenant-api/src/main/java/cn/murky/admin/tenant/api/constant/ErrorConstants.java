package cn.murky.admin.tenant.api.constant;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstants {
    public static final ErrorRecord TENANT_NAME_ALREADY = new ErrorRecord(10000, "tenant.tenantNameAlready");
    public static final ErrorRecord REPEATED_PASSWORD_ERROR = new ErrorRecord(10001, "tenant.user.surePasswordError");
    public static final ErrorRecord ACCOUNT_ALREADY = new ErrorRecord(10002, "tenant.user.tenantAccountAlready");
    public static final ErrorRecord PERMISSION_GROUP_NAME_ALREADY = new ErrorRecord(10004, "tenant.tenantGroupNameAlready");
}
