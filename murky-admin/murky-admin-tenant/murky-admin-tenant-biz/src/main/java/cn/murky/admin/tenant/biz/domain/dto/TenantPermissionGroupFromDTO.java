package cn.murky.admin.tenant.biz.domain.dto;

import cn.murky.admin.tenant.biz.convert.TenantPermissionGroupConvert;
import cn.murky.admin.tenant.biz.domain.entity.TenantPermissionGroup;
import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

import java.util.List;

/**
 * 租户权限组表单类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantPermissionGroupFromDTO {
    /**
     * 权限组id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 权限组名
     */
    @NotBlank
    private String groupName;

    /**
     * 描述
     */
    private String describe;

    /**
     * 所属菜单id
     */
    private List<Long> tenantMenuIds;

    public TenantPermissionGroup toEntity() {
        return TenantPermissionGroupConvert.INSTANCES.toEntity(this);
    }
}
