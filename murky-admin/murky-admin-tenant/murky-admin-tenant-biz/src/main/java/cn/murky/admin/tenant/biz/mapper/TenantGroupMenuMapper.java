package cn.murky.admin.tenant.biz.mapper;

import cn.murky.admin.tenant.biz.domain.entity.TenantGroupMenu;
import com.mybatisflex.core.BaseMapper;

public interface TenantGroupMenuMapper extends BaseMapper<TenantGroupMenu> {
}
