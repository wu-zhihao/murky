package cn.murky.admin.tenant.biz.convert;

import cn.murky.admin.tenant.biz.domain.dto.TenantFromDTO;
import cn.murky.admin.tenant.biz.domain.entity.Tenant;
import cn.murky.common.domain.dto.SchemaInitDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Tenant实体转化接口
 *
 * @auth hans
 */
@Mapper
public interface TenantConvert {
    TenantConvert INSTANCES = Mappers.getMapper(TenantConvert.class);

    Tenant toEntity(TenantFromDTO tenantFromDTO);
    SchemaInitDTO toSchemaDTO(Tenant tenant);
}
