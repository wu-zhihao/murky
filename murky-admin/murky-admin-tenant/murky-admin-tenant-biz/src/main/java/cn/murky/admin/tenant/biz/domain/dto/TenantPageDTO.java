package cn.murky.admin.tenant.biz.domain.dto;

import cn.murky.admin.tenant.biz.domain.entity.Tenant;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 租户分页DTO实体类
 */
@Data
@Accessors(chain = true)
public class TenantPageDTO extends Page<Tenant> {

    /**
     * 租户名称
     */
    private String tenantName;
}
