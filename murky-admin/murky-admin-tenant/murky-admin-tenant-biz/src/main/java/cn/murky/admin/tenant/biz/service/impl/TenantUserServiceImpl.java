package cn.murky.admin.tenant.biz.service.impl;

import cn.murky.admin.tenant.biz.domain.entity.TenantUser;
import cn.murky.admin.tenant.biz.mapper.TenantUserMapper;
import cn.murky.admin.tenant.biz.service.ITenantUserService;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.solon.annotation.Component;

/**
 * TenantUserService
 *
 * @auth hans
 */
@Component
public class TenantUserServiceImpl extends ServiceImpl<TenantUserMapper, TenantUser> implements ITenantUserService {
}
