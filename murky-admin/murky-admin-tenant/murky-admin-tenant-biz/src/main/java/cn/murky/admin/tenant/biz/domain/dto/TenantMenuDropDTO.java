package cn.murky.admin.tenant.biz.domain.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotEmpty;

import java.util.List;

/**
 * 租户菜单拖动接口参数实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantMenuDropDTO {

    /**
     * 租户父级菜单id
     */
    private Long parentId;

    /**
     * 菜单id集合,按顺序排列
     */
    @NotEmpty
    private List<Long> menuIds;
}
