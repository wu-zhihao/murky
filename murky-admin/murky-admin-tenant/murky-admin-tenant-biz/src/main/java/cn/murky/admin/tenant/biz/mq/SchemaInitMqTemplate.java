package cn.murky.admin.tenant.biz.mq;

import cn.murky.common.domain.dto.SchemaInitDTO;
import cn.murky.folkmq.constant.SysGroupConstant;
import lombok.extern.slf4j.Slf4j;
import org.noear.redisx.RedisClient;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cloud.annotation.CloudEvent;

import static cn.murky.folkmq.constant.SysTopicConstant.TENANT_INIT_TOPIC;

/**
 * 用来处理租户模式创建后的初始化数据事件
 */
@Slf4j
@Component
@CloudEvent(topic = TENANT_INIT_TOPIC, group = SysGroupConstant.DEFAULT)
public class SchemaInitMqTemplate {
    @Inject
    private RedisClient redisClient;
    /**
     * 生产消息
     */
    public void publish(SchemaInitDTO schemaInitDTO) {
        log.info("[SchemaInitMqTemplate] -> publish 发布消息 topic:{}", TENANT_INIT_TOPIC);
        redisClient.getBus().publish(TENANT_INIT_TOPIC, ONode.serialize(schemaInitDTO));
    }

}
