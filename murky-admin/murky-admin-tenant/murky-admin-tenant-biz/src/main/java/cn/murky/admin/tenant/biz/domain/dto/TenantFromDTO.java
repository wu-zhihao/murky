package cn.murky.admin.tenant.biz.domain.dto;

import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

/**
 * 商户菜单表单类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantFromDTO {
    /**
     * 租户id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 权限组id
     */
    @NotNull
    private Long fkGroupId;

    /**
     * 租户名
     */
    @NotBlank
    private String tenantName;

    /**
     * 到期时间
     */
    @NotNull
    private OffsetDateTime expires;

    /**
     * 描述
     */
    private String describe;

    /**
     * 账号
     */
    @NotBlank(groups = Insert.class)
    private String account;

    /**
     * 密码
     */
    @NotBlank(groups = Insert.class)
    private String password;

    /**
     * 二次确认密码
     */
    @NotBlank(groups = Insert.class)
    private String confirmPassword;
}
