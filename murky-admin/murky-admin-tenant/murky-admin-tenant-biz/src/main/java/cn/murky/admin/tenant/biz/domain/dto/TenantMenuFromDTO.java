package cn.murky.admin.tenant.biz.domain.dto;

import cn.murky.admin.tenant.biz.domain.entity.TenantMenu;
import cn.murky.admin.tenant.biz.enums.*;
import cn.murky.admin.tenant.biz.convert.TenantMenuConvert;
import cn.murky.core.validat.Update;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;
/**
 * 租户菜单表单类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantMenuFromDTO {
    /**
     * 租户菜单id
     */
    @NotNull(groups = Update.class)
    private Long id;

    /**
     * 菜单标题
     */
    @NotBlank
    private String label;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 打开方式 1:当前窗口  2:新窗口
     */
    private MenuOpenType openType;

    /**
     * 权限码
     */
    private String auth;

    /**
     * 上级菜单id
     */
    private Long parentId;

    /**
     * 菜单类型 0:目录 1:侧边菜单 2:按钮
     */
    @NotNull
    private TenantMenuType type;

    /**
     * 是否开启缓存 0:关闭  1:开启
     */
    private MenuCacheType isCache;

    /**
     * 是否显示在菜单  0:显示  1:隐藏
     */
    private MenuDisplayType isDisplay;

    /**
     * 是否使用外链  0:否  1:是
     */
    private MenuOutside isOutside;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 图标
     */
    private String icon;

    /**
     * 路由参数
     */
    private String query;

    public TenantMenu toEntity() {
        return TenantMenuConvert.INSTANCES.toEntity(this);
    }
}
