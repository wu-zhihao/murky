package cn.murky.admin.tenant.biz.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 租户角色视图类,包含对应的菜单关系
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantPermissionGroupVo implements Serializable {
    /**
     * 主键
     */
    private Long groupId;

    /**
     * 角色名
     */
    private String groupName;

    /**
     * 描述
     */
    private String describe;

    /**
     * 菜单id集合
     */
    private List<Long> tenantMenuIds;
}
