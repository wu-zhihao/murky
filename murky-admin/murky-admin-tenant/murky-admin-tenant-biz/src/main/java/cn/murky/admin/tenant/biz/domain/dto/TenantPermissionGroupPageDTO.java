package cn.murky.admin.tenant.biz.domain.dto;

import cn.murky.admin.tenant.biz.domain.entity.TenantPermissionGroup;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 租户权限组分页DTO实体类
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantPermissionGroupPageDTO extends Page<TenantPermissionGroup> {
    /**
     * 权限组名
     */
    private String groupName;

}
