package cn.murky.admin.tenant.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.tenant.biz.domain.entity.TenantMenu;
import cn.murky.admin.tenant.biz.convert.TenantMenuConvert;
import cn.murky.admin.tenant.biz.domain.dto.TenantMenuDropDTO;
import cn.murky.admin.tenant.biz.domain.dto.TenantMenuFromDTO;
import cn.murky.admin.tenant.biz.domain.vo.TenantMenuTreeVo;
import cn.murky.admin.tenant.biz.enums.TenantMenuType;
import cn.murky.admin.tenant.biz.service.ITenantMenuService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import org.noear.snack.ONode;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

/**
 * 租户菜单管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("tenantMenu")
public class TenantMenuController extends BaseController<ITenantMenuService> {
    /**
     * 租户菜单列表查询
     * @return List<TenantMenuTreeVo>
     */
    @Get
    @Mapping("list")
    @SaCheckPermission("tenantMenu")
    public ApiResult<List<TenantMenuTreeVo>> list() {
        return ApiResult.ok(baseService.treeSysMenu(Arrays.asList(TenantMenuType.MENU,TenantMenuType.DIRECTORY,TenantMenuType.BUTTON)));
    }

    /**
     * 租户菜单详情
     * @param tenantMenuId tenantMenuId
     * @return TenantMenu
     */
    @Get
    @Mapping("{tenantMenuId}")
    public ApiResult<TenantMenu> info(Long tenantMenuId){
        return ApiResult.ok(baseService.getById(tenantMenuId));
    }

    /**
     * 新增菜单
     * @param tenantMenuFromDTO tenantMenuFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("tenantMenu:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) TenantMenuFromDTO tenantMenuFromDTO){
        if(!ONode.loadStr(tenantMenuFromDTO.getQuery()).isUndefined()&&!ONode.loadStr(tenantMenuFromDTO.getQuery()).isObject()){
            return ApiResult.fail("query参数格式不正确");
        }
        TenantMenu tenantMenu = TenantMenuConvert.INSTANCES.toEntity(tenantMenuFromDTO);
        if(TenantMenuType.DIRECTORY== tenantMenu.getType()){
            tenantMenu.setComponent("LAYOUT");
        }
        return toResult(baseService.save(tenantMenu));
    }

    /**
     * 修改菜单
     * @param tenantMenuFromDTO tenantMenuFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("tenantMenu:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) TenantMenuFromDTO tenantMenuFromDTO){
        if(!ONode.loadStr(tenantMenuFromDTO.getQuery()).isUndefined()&&!ONode.loadStr(tenantMenuFromDTO.getQuery()).isObject()){
            return ApiResult.fail("query参数格式不正确");
        }
        TenantMenu tenantMenu = TenantMenuConvert.INSTANCES.toEntity(tenantMenuFromDTO);
        if(TenantMenuType.DIRECTORY== tenantMenu.getType()){
            tenantMenu.setComponent("LAYOUT");
        }
        boolean result = baseService.updateById(tenantMenu);
        return toResult(result);
    }

    /**
     * 菜单拖动
     * @param tenantMenuDropDTO tenantMenuDropDTO
     */
    @Put
    @Mapping("drop")
    @SaCheckPermission("tenantMenu:edit")
    public ApiResult<?> drop(@Body @Validated TenantMenuDropDTO tenantMenuDropDTO){
        return toResult(baseService.drop(tenantMenuDropDTO));
    }

    /**
     * 删除租户菜单
     * @param tenantMenuId tenantMenuId
     */
    @Delete
    @Mapping("/{tenantMenuId}")
    @SaCheckPermission("tenantMenu:remove")
    public ApiResult<?> remove(Long tenantMenuId){
        boolean result = baseService.removeById(tenantMenuId);
        return toResult(result);
    }
}
