package cn.murky.admin.tenant.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.common.enums.CommonStatus;
import cn.murky.admin.tenant.biz.domain.dto.TenantFromDTO;
import cn.murky.admin.tenant.biz.domain.dto.TenantPageDTO;
import cn.murky.admin.tenant.biz.domain.entity.Tenant;
import cn.murky.admin.tenant.biz.domain.vo.TenantVo;
import cn.murky.admin.tenant.biz.service.ITenantService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Update;
import com.mybatisflex.core.paginate.Page;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

/**
 * 租户管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("tenant")
public class TenantController extends BaseController<ITenantService> {
    /**
     * 租户列表分页查询
     * @param tenantPageDTO tenantPageDTO
     * @return Page<TenantVo>
     */
    @Get
    @Mapping("page")
    @SaCheckPermission("tenant")
    public ApiResult<Page<TenantVo>> page(TenantPageDTO tenantPageDTO) {
        return ApiResult.ok(baseService.page(tenantPageDTO));
    }

    /**
     * 租户详情
     * @param tenantId 租户id
     * @return TenantVo
     */
    @Get
    @Mapping("{tenantId}")
    @SaCheckPermission("tenant")
    public ApiResult<TenantVo> info(Long tenantId) {
        return ApiResult.ok(baseService.info(tenantId));
    }

    /**
     * 新增租户
     * @param tenantFromDTO tenantFromDTO
     * @return Boolean
     */
    @Post
    @Mapping
    @SaCheckPermission("tenant:add")
    public ApiResult<Boolean> add( @Body TenantFromDTO tenantFromDTO) {
        return ApiResult.ok(baseService.add(tenantFromDTO));
    }

    /**
     * 修改租户
     * @param tenantFromDTO tenantFromDTO
     * @return Boolean
     */
    @Put
    @Mapping
    @SaCheckPermission("tenant:edit")
    public ApiResult<Boolean> edit(@Validated(Update.class) @Body TenantFromDTO tenantFromDTO) {
        return ApiResult.ok(baseService.edit(tenantFromDTO));
    }

    /**
     * 停用/启用租户
     * @param tenantId tenantId
     * @return Tenant
     */
    @Post
    @Mapping("{tenantId}")
    @SaCheckPermission("tenant:edit")
    public ApiResult<Tenant> edit(Long tenantId) {
        Tenant tenant = baseService.getById(tenantId);
        CommonStatus status = tenant.getStatus().cut();
        tenant.setStatus(status);
        boolean b = baseService.updateById(tenant);
        if(b){
            return ApiResult.ok(tenant);
        }
        return ApiResult.fail();
    }

}
