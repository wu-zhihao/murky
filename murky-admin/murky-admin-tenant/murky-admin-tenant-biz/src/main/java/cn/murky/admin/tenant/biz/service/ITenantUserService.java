package cn.murky.admin.tenant.biz.service;

import cn.murky.admin.tenant.biz.domain.entity.TenantUser;
import com.mybatisflex.core.service.IService;

/**
 * TenantUser Service
 *
 * @auth hans
 */
public interface ITenantUserService extends IService<TenantUser> {
}
