package cn.murky.admin.tenant.biz.domain.vo;

import cn.murky.common.enums.CommonStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/**
 * 租户视图类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
public class TenantVo {
    /**
     * 租户id
     */
    private Long id;

    /**
     * 权限组id
     */
    private Long fkGroupId;

    /**
     * 权限组名称
     */
    private String groupName;

    /**
     * 租户名
     */
    private String tenantName;

    /**
     * 租户管理员
     */
    private Long adminUser;

    /**
     * 到期时间
     */
    private OffsetDateTime expires;

    /**
     * 描述
     */
    private String describe;

    /**
     * 状态
     */
    private CommonStatus status;

    /**
     * 创建时间
     */
    private OffsetDateTime createTime;
}
