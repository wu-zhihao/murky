package cn.murky.admin.tenant.biz.domain.entity;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

/***
 * 租户权限码菜单关系实体类
 *
 * @auth hans
 */
@Data(staticConstructor = "create")
@Accessors(chain = true)
@Table("tenant_group_menu")
public class TenantGroupMenu extends Model<TenantGroupMenu> {
    /**
     * 权限组id
     */
    private Long fkGroupId;

    /**
     * 菜单id
     */
    private Long fkMenuId;

}
