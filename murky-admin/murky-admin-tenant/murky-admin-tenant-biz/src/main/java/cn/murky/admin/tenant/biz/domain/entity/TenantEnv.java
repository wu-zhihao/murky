package cn.murky.admin.tenant.biz.domain.entity;

import cn.murky.admin.tenant.api.enums.EnvTypeEnum;
import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 租户环境配置表
 */
@Data
@Accessors(chain = true)
@Table("tenant_env")
public class TenantEnv extends BaseEntity<TenantEnv> {
    /**
     * 租户id
     */
    @Id
    private Long id;

    /**
     * 环境类型
     */
    private EnvTypeEnum envType;

    /**
     * 租户id
     */
    private Long fkTenantId;

    /**
     * 环境配置
     */
    private String envConfig;


}
