package cn.murky.admin.tenant.biz.domain.entity;

import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 租户权限组实体类
 *
 * @auth hans
 */
@Data
@Accessors(chain = true)
@Table(value = "tenant_permission_group")
public class TenantPermissionGroup extends BaseEntity<TenantPermissionGroup> {

    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 权限组名
     */
    private String groupName;

    /**
     * 描述
     */
    private String describe;
}
