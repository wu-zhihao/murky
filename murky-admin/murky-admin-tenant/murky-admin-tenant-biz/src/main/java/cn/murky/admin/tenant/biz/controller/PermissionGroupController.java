package cn.murky.admin.tenant.biz.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.murky.admin.tenant.biz.domain.entity.TenantPermissionGroup;
import cn.murky.admin.tenant.biz.domain.dto.TenantPermissionGroupFromDTO;
import cn.murky.admin.tenant.biz.domain.dto.TenantPermissionGroupPageDTO;
import cn.murky.admin.tenant.biz.domain.entity.table.TenantPermissionGroupTableDef;
import cn.murky.admin.tenant.biz.domain.vo.TenantPermissionGroupVo;
import cn.murky.admin.tenant.biz.service.ITenantPermissionGroupService;
import cn.murky.common.web.ApiResult;
import cn.murky.core.web.BaseController;
import cn.murky.core.validat.Insert;
import cn.murky.core.validat.Update;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.core.query.QueryWrapper;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Valid;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/***
 * 租户权限组管理
 *
 * @auth hans
 */
@Controller
@Valid
@Mapping("permissionGroup")
public class PermissionGroupController extends BaseController<ITenantPermissionGroupService> {

    /**
     * 租户权限组列表分页查询
     * @param tenantPermissionGroupPageDTO tenantPermissionGroupPageDTO
     * @return Page<TenantPermissionGroup>
     */
    @Get
    @Mapping("page")
    @SaCheckPermission("permissionGroup")
    public ApiResult<Page<TenantPermissionGroup>> page(TenantPermissionGroupPageDTO tenantPermissionGroupPageDTO) {
        TenantPermissionGroupTableDef TENANT_PERMISSION_GROUP = TenantPermissionGroupTableDef.TENANT_PERMISSION_GROUP;
        Page<TenantPermissionGroup> result = baseService.page(tenantPermissionGroupPageDTO,
                QueryWrapper.create()
                        .and(TENANT_PERMISSION_GROUP.GROUP_NAME.like(tenantPermissionGroupPageDTO.getGroupName(), If::hasText))
                        .orderBy(TENANT_PERMISSION_GROUP.CREATE_TIME.asc())
        );
        return ApiResult.ok(result);
    }

    /**
     * 租户权限组列表查询
     * @param tenantPermissionGroupPageDTO tenantPermissionGroupPageDTO
     * @return List<TenantPermissionGroup>
     */
    @Get
    @Mapping("list")
    @SaCheckPermission("permissionGroup")
    public ApiResult<List<TenantPermissionGroup>> list(TenantPermissionGroupPageDTO tenantPermissionGroupPageDTO) {
        TenantPermissionGroupTableDef TENANT_PERMISSION_GROUP = TenantPermissionGroupTableDef.TENANT_PERMISSION_GROUP;
        List<TenantPermissionGroup> result = baseService.list(
                QueryWrapper.create()
//                .and(TENANT_PERMISSION_GROUP.ID.notIn(SecurityUtils.getUserInfo().getRoleIds()))
                        .and(TENANT_PERMISSION_GROUP.GROUP_NAME.like(tenantPermissionGroupPageDTO.getGroupName()))
                        .orderBy(TENANT_PERMISSION_GROUP.CREATE_TIME.asc())
        );
        return ApiResult.ok(result);
    }

    /**
     * 租户权限组详情
     * @param roleId 角色id
     * @return TenantPermissionGroupVo
     */
    @Get
    @Mapping("{roleId}")
    @SaCheckPermission("permissionGroup")
    public ApiResult<TenantPermissionGroupVo> info(Long roleId) {
        return ApiResult.ok(baseService.info(roleId));
    }

    /**
     * 新增租户权限组
     * @param tenantPermissionGroupFromDTO tenantPermissionGroupFromDTO
     */
    @Post
    @Mapping
    @SaCheckPermission("permissionGroup:add")
    public ApiResult<?> add(@Body @Validated(Insert.class) TenantPermissionGroupFromDTO tenantPermissionGroupFromDTO) {
        return toResult(baseService.save(tenantPermissionGroupFromDTO));
    }

    /**
     * 修改租户权限组
     * @param tenantPermissionGroupFromDTO tenantPermissionGroupFromDTO
     */
    @Put
    @Mapping
    @SaCheckPermission("permissionGroup:edit")
    public ApiResult<?> edit(@Body @Validated(Update.class) TenantPermissionGroupFromDTO tenantPermissionGroupFromDTO) {
        boolean result = baseService.update(tenantPermissionGroupFromDTO);
        return toResult(result);
    }

    /**
     * 删除租户权限组
     * @param groupId 权限组id
     */
    @Delete
    @Mapping("/{groupId}")
    @SaCheckPermission("permissionGroup:remove")
    public ApiResult<?> remove(Long groupId) {
        boolean result = baseService.removeById(groupId);
        return toResult(result);
    }
}
