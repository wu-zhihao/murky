package cn.murky.folkmq.constant;

public class SysTopicConstant {



    /**
     * 租户初始化topic
     */
    public static final String TENANT_INIT_TOPIC="topic.tenant.schema.";

    /**
     * 后台系统发起通知后的topic
     */
    public static final String SYS_NOTICE_TOPIC="topic.sys.notice.";


    /**
     * 清空本地i18n缓存消息topic
     */
    public static final String SYS_I18N_TOPIC="topic.i18n.clean";



}
