package cn.murky.folkmq;

/**
 * 使用redis实现mq消息的模板接口
 */
public interface RedisMqTemplate {

    /**
     * 发布消息
     * 一般不需要使用,自己重新实现一个即可
     */
    default void publish(){}

    /**
     * 监听消息,不需要调用,只需要实现即可
     * @return 返回值表示是否继续监听队列
     */
    default boolean subscribe(){
        return false;
    }

    /**
     * 获取消息主题
     */
    String getTopic();
}
