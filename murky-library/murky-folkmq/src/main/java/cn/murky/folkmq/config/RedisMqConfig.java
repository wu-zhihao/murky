package cn.murky.folkmq.config;


import cn.murky.folkmq.RedisMqTemplate;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import java.util.List;

@Configuration
public class RedisMqConfig {


    @Bean
    public void redisMqTemplate(@Inject List<RedisMqTemplate> redisMqTemplateList) {
        redisMqTemplateList.forEach(template -> {
            Thread.startVirtualThread(()->{
                boolean subscribe=false;
                do {
                    try {
                        subscribe = template.subscribe();
                    }catch (Exception e){
                        subscribe=true;
                    }
                }while (subscribe);
            });
        });
    }

}
