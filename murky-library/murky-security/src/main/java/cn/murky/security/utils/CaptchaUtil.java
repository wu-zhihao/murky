package cn.murky.security.utils;

import cn.murky.common.constant.CommonErrorConstant;
import cn.murky.common.record.ErrorRecord;
import cn.murky.common.web.ApiResult;
import cn.murky.security.constants.ErrorConstant;
import com.anji.captcha.model.common.ResponseModel;

import java.util.HashMap;
import java.util.Map;

public class CaptchaUtil {
    private static final Map<String, ErrorRecord> map;

    private static final String captSuccessCode="0000";
    static {
        map=new HashMap<>();
        map.put("0000", CommonErrorConstant.SUCCESS);
        map.put("0001", CommonErrorConstant.FAIL);
        map.put("9999", CommonErrorConstant.FAIL);
        map.put("0011", ErrorConstant.VERIFICATION_PARAMETERS_CANNOT_BE_EMPTY);
        map.put("0012", CommonErrorConstant.NULL_ERROR);
        map.put("0013", ErrorConstant.NOT_EXIST_ERROR);
        map.put("0014", ErrorConstant.EXIST_ERROR);
        map.put("0015", ErrorConstant.PARAM_TYPE_ERROR);
        map.put("0016", ErrorConstant.PARAM_FORMAT_ERROR);
        map.put("6110", ErrorConstant.API_CAPTCHA_INVALID);
        map.put("6111", ErrorConstant.API_CAPTCHA_COORDINATE_ERROR);
        map.put("6112", ErrorConstant.API_CAPTCHA_ERROR);
        map.put("6113", ErrorConstant.API_CAPTCHA_BASEMAP_NULL);
        map.put("6201", ErrorConstant.API_REQ_LIMIT_GET_ERROR);
        map.put("6206", ErrorConstant.API_REQ_INVALID);
        map.put("6202", ErrorConstant.API_REQ_LOCK_GET_ERROR);
        map.put("6204", ErrorConstant.API_REQ_LIMIT_CHECK_ERROR);
        map.put("6205", ErrorConstant.API_REQ_LIMIT_VERIFY_ERROR);
    }

    public static ApiResult<Object> parseApiResult(ResponseModel responseModel){
        if(captSuccessCode.equals(responseModel.getRepCode())){
            return ApiResult.ok(responseModel.getRepData());
        }
        return ApiResult.fail(map.get(responseModel.getRepCode()));
    }
}
