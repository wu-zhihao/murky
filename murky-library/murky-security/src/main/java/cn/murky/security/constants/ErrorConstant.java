package cn.murky.security.constants;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstant {
    public static final ErrorRecord NOT_EXIST_ERROR = new ErrorRecord(10001, "captcha.DB.notExist");
    public static final ErrorRecord VERIFICATION_PARAMETERS_CANNOT_BE_EMPTY = new ErrorRecord(10001, "captcha.VERIFICATION_PARAMETERS_CANNOT_BE_EMPTY");
    public static final ErrorRecord EXIST_ERROR = new ErrorRecord(10001, "captcha.DB.exist");
    public static final ErrorRecord PARAM_TYPE_ERROR = new ErrorRecord(10001, "captcha.PARAM_TYPE_ERROR");
    public static final ErrorRecord PARAM_FORMAT_ERROR = new ErrorRecord(10001, "captcha.PARAM_FORMA_ERROR");
    public static final ErrorRecord API_CAPTCHA_INVALID = new ErrorRecord(10001, "captcha.API_CAPTCHA_INVALID");
    public static final ErrorRecord API_CAPTCHA_COORDINATE_ERROR = new ErrorRecord(10001, "captcha.API_CAPTCHA_COORDINATE_ERROR");
    public static final ErrorRecord API_CAPTCHA_ERROR = new ErrorRecord(10001, "captcha.API_CAPTCHA_ERROR");
    public static final ErrorRecord API_CAPTCHA_BASEMAP_NULL = new ErrorRecord(10001, "captcha.API_CAPTCHA_BASEMAP_NULL");
    public static final ErrorRecord API_REQ_LIMIT_GET_ERROR = new ErrorRecord(10001, "captcha.API_REQ_LIMIT_GET_ERROR");
    public static final ErrorRecord API_REQ_INVALID = new ErrorRecord(10001, "captcha.API_REQ_INVALID");
    public static final ErrorRecord API_REQ_LOCK_GET_ERROR = new ErrorRecord(10001, "captcha.API_REQ_LOCK_GET_ERROR");
    public static final ErrorRecord API_REQ_LIMIT_CHECK_ERROR = new ErrorRecord(10001, "captcha.API_REQ_LIMIT_CHECK_ERROR");
    public static final ErrorRecord API_REQ_LIMIT_VERIFY_ERROR = new ErrorRecord(10001, "captcha.API_REQ_LIMIT_VERIFY_ERROR");

}
