package cn.murky.quartz.service.impl;

import cn.murky.quartz.domain.entity.SysJobLog;
import cn.murky.quartz.mapper.SysJobLogMapper;
import cn.murky.quartz.service.ISysJobLogService;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.noear.solon.annotation.Component;

@Component
public class SysJobLogServiceImpl extends ServiceImpl<SysJobLogMapper,SysJobLog> implements ISysJobLogService {
}
