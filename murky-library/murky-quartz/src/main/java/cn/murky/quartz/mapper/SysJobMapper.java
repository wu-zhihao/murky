package cn.murky.quartz.mapper;

import cn.murky.quartz.domain.entity.SysJob;
import cn.murky.quartz.domain.entity.table.SysJobTableDef;
import cn.murky.quartz.enums.SysJobGroupEnum;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.query.QueryWrapper;

import java.util.List;

public interface SysJobMapper extends BaseMapper<SysJob> {

    /**
     * 查询调度任务集合
     *
     * @param job 调度信息
     * @return 操作日志集合
     */
    default List<SysJob> selectJobList(SysJob job){
        SysJobTableDef SYS_JOB = SysJobTableDef.SYS_JOB;
        QueryWrapper queryWrapper = QueryWrapper.create().where(SYS_JOB.JOB_NAME.like(job.getJobName()))
                .and(SYS_JOB.JOB_GROUP.eq(job.getJobGroup()))
                .and(SYS_JOB.STATUS.eq(job.getStatus()))
                .and(SYS_JOB.INVOKE_TARGET.eq(job.getInvokeTarget()));
        return selectListByQuery(queryWrapper);
    }

    /**
     * 查询所有需要调度的任务
     *
     * @return 调度任务列表
     */
    default List<SysJob> selectJobAll(SysJobGroupEnum jobGroup){
        SysJobTableDef SYS_JOB = SysJobTableDef.SYS_JOB;
        QueryWrapper queryWrapper = QueryWrapper.create()
                .where(SYS_JOB.JOB_GROUP.eq(jobGroup));
        return selectListByQuery(queryWrapper);
    }
}
