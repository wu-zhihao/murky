package cn.murky.quartz.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

@AllArgsConstructor
@Getter
public enum SysJobStatus {
    NORMAL(0,"正常"),
    PAUSE(1,"暂停"),
    ;
    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
