package cn.murky.quartz.service;

import cn.murky.quartz.domain.entity.SysJobLog;
import com.mybatisflex.core.service.IService;

public interface ISysJobLogService extends IService<SysJobLog> {
}
