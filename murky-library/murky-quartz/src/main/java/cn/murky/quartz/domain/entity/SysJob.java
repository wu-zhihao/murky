package cn.murky.quartz.domain.entity;

import cn.murky.quartz.enums.ConcurrentEnum;
import cn.murky.quartz.enums.MisfirePolicyEnum;
import cn.murky.quartz.enums.SysJobGroupEnum;
import cn.murky.quartz.enums.SysJobStatus;
import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.Size;

/**
 * 定时任务调度表 sys_job
 * 
 * @auth hans
 */
@Data
@Table("sys_job")
@Accessors(chain = true)
public class SysJob extends BaseEntity<SysJob>
{

    /** 任务ID */
    @Id
    private Long id;

    /** 任务名称 */
    @NotBlank(message = "任务名称不能为空")
    @Size(min = 0, max = 64, message = "任务名称不能超过64个字符")
    private String jobName;

    /** 任务分组 */
    private SysJobGroupEnum jobGroup;

    /** 调用目标字符串 */
    @NotBlank(message = "调用目标字符串不能为空")
    @Size(min = 0, max = 500, message = "调用目标字符串长度不能超过500个字符")
    private String invokeTarget;

    /** cron执行表达式 */
    @NotBlank(message = "Cron执行表达式不能为空")
    @Size(min = 0, max = 255, message = "Cron执行表达式不能超过255个字符")
    private String cronExpression;

    /** cron计划策略 */
    private MisfirePolicyEnum misfirePolicy = MisfirePolicyEnum.DEFAULT;

    /** 是否并发执行（0允许 1禁止） */
    private ConcurrentEnum concurrent;

    /** 任务状态（0正常 1暂停） */
    private SysJobStatus status;

}
