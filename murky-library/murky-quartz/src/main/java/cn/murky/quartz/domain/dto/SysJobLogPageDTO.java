package cn.murky.quartz.domain.dto;

import cn.murky.quartz.domain.entity.SysJobLog;
import cn.murky.quartz.enums.SysJobStatus;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.Size;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * 定时任务分页DTO
 */
@Data
@Accessors(chain = true)
public class SysJobLogPageDTO extends Page<SysJobLog> {
    /** 任务名称 */
    @Size(min = 0, max = 64, message = "任务名称不能超过64个字符")
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 任务状态（0正常 1暂停） */
    private SysJobStatus status;

    /** 调用目标字符串 */
    @Size(min = 0, max = 500, message = "调用目标字符串长度不能超过500个字符")
    private String invokeTarget;

    /** 时间范围查询 */
    @Size(max = 2)
    private List<OffsetDateTime> offsetDateTimes;
}
