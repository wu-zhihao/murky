package cn.murky.quartz.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

/**
 * 任务分组
 */
@AllArgsConstructor
@Getter
public enum SysJobGroupEnum {
    ADMIN(0,"管理端"),
    TENANT(1,"租户端"),
    ;
    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
