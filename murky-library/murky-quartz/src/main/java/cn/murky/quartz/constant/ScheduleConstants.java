package cn.murky.quartz.constant;

/**
 * 任务调度通用常量
 * 
 * @auth ruoyi
 */
public class ScheduleConstants
{
    public static final String TASK_CLASS_NAME = "TASK_CLASS_NAME";

    /** 执行目标key */
    public static final String TASK_PROPERTIES = "TASK_PROPERTIES";

    /**
     * RMI 远程方法调用
     */
    public static final String LOOKUP_RMI = "rmi:";


    /**
     * LDAP 远程方法调用
     */
    public static final String LOOKUP_LDAP = "ldap:";


    /**
     * LDAPS 远程方法调用
     */
    public static final String LOOKUP_LDAPS = "ldaps:";


    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 定时任务违规的字符
     */
    public static final String[] JOB_ERROR_STR = { "java.net.URL", "javax.naming.InitialContext", "org.yaml.snakeyaml",
            "org.noear", "org.apache"};

}
