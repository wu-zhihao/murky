package cn.murky.quartz.domain.entity;


import cn.murky.quartz.enums.SysJobLogStatus;
import cn.murky.common.domain.entity.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

/**
 * 定时任务调度日志表 sys_job_log
 * 
 * @since ruoyi
 */
@Data
@Table("sys_job_log")
@Accessors(chain = true)
public class SysJobLog extends BaseEntity<SysJobLog>
{

    /** 日志序号 */
    @Id
    private Long id;

    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** 日志信息 */
    private String jobMessage;

    /** 执行状态（0正常 1失败） */
    private SysJobLogStatus status;

    /** 异常信息 */
    private String exceptionInfo;

    /** 开始时间 */
    @Column(ignore = true)
    private OffsetDateTime startTime;

    /** 停止时间 */
    @Column(ignore = true)
    private OffsetDateTime stopTime;

}
