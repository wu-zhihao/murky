package cn.murky.quartz.constant;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstants {
    public static final ErrorRecord KEY_ALREADY_EXISTS_ERROR = new ErrorRecord(3000, "system.quartz.taskNotExist");
}
