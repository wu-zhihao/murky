package cn.murky.quartz.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

@AllArgsConstructor
@Getter
public enum MisfirePolicyEnum {
    DEFAULT(0,"默认"),
    IMMEDIATELY(1,"立即触发执行"),
    IMMEDIATELY_ONCE(2,"触发一次执行"),
    NOT_TIGER_IMMEDIATE(3,"不触发立即执行")
    ;
    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
