package cn.murky.quartz.task;

import org.noear.solon.annotation.Component;

@Component(name = "demoTask")
public class DemoTast {

    public void test() {
        System.out.println("demoTask test");
    }
}
