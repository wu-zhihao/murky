package cn.murky.quartz.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

@Configuration
public class Config {
    @Bean
    public Scheduler jobInit() throws SchedulerException {
        //可以换成别的工厂
        return new StdSchedulerFactory().getScheduler();
    }
}
