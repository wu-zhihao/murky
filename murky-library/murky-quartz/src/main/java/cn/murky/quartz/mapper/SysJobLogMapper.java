package cn.murky.quartz.mapper;

import cn.murky.quartz.domain.entity.SysJobLog;
import com.mybatisflex.core.BaseMapper;

public interface SysJobLogMapper extends BaseMapper<SysJobLog> {
}
