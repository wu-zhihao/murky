package cn.murky.quartz.enums;

import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.noear.snack.annotation.ONodeAttr;

@AllArgsConstructor
@Getter
public enum ConcurrentEnum {
    ALLOW(0,"允许"),
    PROHIBIT(1,"禁止"),
    ;
    @EnumValue
    @ONodeAttr
    private final Integer code;
    private final String des;

    @Override
    public String toString() {
        return this.code+":"+this.des;
    }
}
