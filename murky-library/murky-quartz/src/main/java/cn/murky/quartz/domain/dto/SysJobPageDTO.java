package cn.murky.quartz.domain.dto;

import cn.murky.quartz.domain.entity.SysJob;
import cn.murky.quartz.enums.SysJobGroupEnum;
import cn.murky.quartz.enums.SysJobStatus;
import com.mybatisflex.core.paginate.Page;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.Size;

/**
 * 定时任务分页DTO
 */
@Data
@Accessors(chain = true)
public class SysJobPageDTO extends Page<SysJob> {
    /** 任务名称 */
    @NotBlank(message = "任务名称不能为空")
    @Size(min = 0, max = 64, message = "任务名称不能超过64个字符")
    private String jobName;

    /** 任务组名 */
    private SysJobGroupEnum jobGroup;

    /** 调用目标字符串 */
    @NotBlank(message = "调用目标字符串不能为空")
    @Size(min = 0, max = 500, message = "调用目标字符串长度不能超过500个字符")
    private String invokeTarget;

    /** 任务状态（0正常 1暂停） */
    private SysJobStatus status;
}
