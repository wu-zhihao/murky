package cn.murky.common.constant;

public class DictContant {

    /**
     * 字典缓存key
     */
    public static final String DICT_CACHE_KEY="System:Dict:";

    /**
     * i18n字典缓存key
     */
    public static final String I18N_LANGUAGE_DICT_KEY ="i18n:language";

    /**
     * i18n字典标签缓存key
     */
    public static final String I18N_TAG_DICT_KEY ="i18n:tag";

}
