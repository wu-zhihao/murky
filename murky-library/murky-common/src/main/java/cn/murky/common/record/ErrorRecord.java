package cn.murky.common.record;

public record ErrorRecord(Integer errCode,String message) {
}
