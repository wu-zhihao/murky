package cn.murky.common.utils;

import org.noear.solon.Utils;

public class StringUtils {
    public static final int INDEX_NOT_FOUND = -1;
    public static final String EMPTY = "";

    /**
     * 字符串是否为空
     * @param str 验证的字符串
     * @return 如果是空则返回true，否则false
     */
    public static boolean isEmpty(String str) {
        return Utils.isEmpty(str);
    }

    /**
     * 字符串是否不为空 ("",null)
     * @param str 验证的字符串
     * @return 如果是空则返回false，否则true
     */
    public static boolean isNotEmpty(String str) {
        return Utils.isNotEmpty(str);
    }
}
