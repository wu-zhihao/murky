package cn.murky.common.web;

import cn.murky.common.constant.CommonErrorConstant;
import cn.murky.common.record.ErrorRecord;
import lombok.Data;
import lombok.experimental.Accessors;
import org.noear.solon.i18n.I18nUtil;

import java.io.Serial;
import java.io.Serializable;
import java.util.Locale;

/**
 * 统一响应体数据结构
 *
 * @param <T> 任意类型
 * @auth hans
 */
@Accessors(chain = true)
@Data
public class ApiResult<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 响应码
     */
    private int code;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 响应数据
     */
    private T result;

    public static <T> ApiResult<T> ok() {
        return restResult(CommonErrorConstant.SUCCESS);
    }

    public static <T> ApiResult<T> fail(int code, String msg) {
        return restResult(code, msg, null);
    }

    public static <T> ApiResult<T> failNoI18n(int code, String msg) {
        return restResultNoI18n(code, msg, null);
    }
    public static <T> ApiResult<T> fail() {
        return restResult(CommonErrorConstant.FAIL);
    }

    public static <T> ApiResult<T> fail(String msg,String ...msgPamas) {
        return restResult(CommonErrorConstant.FAIL.errCode(), msg,null,msgPamas);
    }


    public static <T> ApiResult<T> fail(ErrorRecord errorRecord) {
        return restResult(errorRecord);
    }


    public static <T> ApiResult<T> ok(T data) {
        return restResult(CommonErrorConstant.SUCCESS, data);
    }

    public static <T> ApiResult<T> ok(T data, Locale locale) {
        return restResult(CommonErrorConstant.SUCCESS, data,locale);
    }

    private static <T> ApiResult<T> restResultNoI18n(int code, String msg, T data) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(code);
        apiResult.setResult(data);
        apiResult.setMessage(msg);
        return apiResult;
    }

    private static <T> ApiResult<T> restResult(int code, String msg, T data) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(code);
        apiResult.setResult(data);
        apiResult.setMessage(I18nUtil.getMessage(msg));
        return apiResult;
    }

    private static <T> ApiResult<T> restResult(int code, String msg, T data,String ...msgPamas) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(code);
        apiResult.setResult(data);
        apiResult.setMessage(I18nUtil.getMessage(msg,msgPamas));
        return apiResult;
    }



    private static <T> ApiResult<T> restResult(ErrorRecord errorRecord, T data) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(errorRecord.errCode());
        apiResult.setResult(data);
        apiResult.setMessage(I18nUtil.getMessage(errorRecord.message()));
        return apiResult;
    }

    private static <T> ApiResult<T> restResult(ErrorRecord errorRecord, T data,Locale locale) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(errorRecord.errCode());
        apiResult.setResult(data);
        apiResult.setMessage(I18nUtil.getMessage(locale,errorRecord.message()));
        return apiResult;
    }

    private static <T> ApiResult<T> restResult(ErrorRecord errorRecord) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(errorRecord.errCode());
        apiResult.setMessage(I18nUtil.getMessage(errorRecord.message()));
        return apiResult;
    }

}
