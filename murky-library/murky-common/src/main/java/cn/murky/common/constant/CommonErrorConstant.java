package cn.murky.common.constant;

import cn.murky.common.record.ErrorRecord;

public class CommonErrorConstant {
    // 统一成功状态码
    public static final ErrorRecord SUCCESS = new ErrorRecord(200, "request.success");
    // FAil(500,"失败","请求错误,根据具体的错误响应不同的状态码"),
    public static final ErrorRecord FAIL = new ErrorRecord(500, "request.fail");
    // NOT_LOGIN(401,"登录状态过期，请重新登录","token过期，未登录等错误"),
    public static final ErrorRecord NOT_LOGIN = new ErrorRecord(401, "login.statusExpired");
    // NOT_PREMISSION(403,"无此权限","无此权限"),
    public static final ErrorRecord NOT_PREMISSION = new ErrorRecord(403, "auth.unauthorized");
    // NOT_TENANT_PREMISSION(403,"无该租户权限","无该租户权限"),
    public static final ErrorRecord NOT_TENANT_PREMISSION = new ErrorRecord(403, "auth.notTenantAuth");
    // {0}不能为空
    public static final ErrorRecord NULL_ERROR = new ErrorRecord(405, "request.notNullError");
    public static final ErrorRecord NOT_NULL_ERROR = new ErrorRecord(405, "request.notNullError");

}
