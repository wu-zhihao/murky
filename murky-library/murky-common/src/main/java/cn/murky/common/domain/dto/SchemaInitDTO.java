package cn.murky.common.domain.dto;

import cn.murky.common.enums.CommonStatus;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class SchemaInitDTO {
    /**
     * 租户id
     */
    private Long id;

    /**
     * 权限组id
     */
    private Long fkGroupId;

    /**
     * 租户名
     */
    private String tenantName;

    /**
     * 租户管理员
     */
    private Long adminUser;

    /**
     * 到期时间
     */
    private OffsetDateTime expires;

    /**
     * 描述
     */
    private String describe;

    /**
     * 状态
     */
    private CommonStatus status;

}
