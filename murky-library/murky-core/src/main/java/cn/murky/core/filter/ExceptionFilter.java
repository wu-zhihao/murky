package cn.murky.core.filter;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.murky.common.constant.CommonErrorConstant;
import cn.murky.core.constant.ErrorConstant;
import cn.murky.common.web.ApiResult;
import cn.murky.core.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Filter;
import org.noear.solon.core.handle.FilterChain;
import org.noear.solon.validation.ValidatorException;

import java.util.MissingResourceException;

/**
 * 系统异常统一处理拦截
 */
@Component(index = -1)
@Slf4j
public class ExceptionFilter implements Filter {

    @Override
    public void doFilter(Context ctx, FilterChain chain) throws Throwable {
        try {
            chain.doFilter(ctx);
        }catch (NotLoginException ex){
            // 未登录异常处理
            log.debug("登录状态过期:{},{}",ex.getCode(),ex.getMessage());
            ctx.render(ApiResult.fail(CommonErrorConstant.NOT_LOGIN));
        }catch (NotPermissionException ex){
            // 没有权限异常处理
            ex.fillInStackTrace();
            ctx.render(ApiResult.fail(CommonErrorConstant.NOT_PREMISSION));
        }catch (ServiceException ex){
            // 业务异常处理
            ctx.render(ApiResult.fail(ex.getCode(),ex.getMessage()));
        }catch (ValidatorException ex){
            // 表单验证异常处理
            log.error("表单验证异常:{}",ex.getMessage());
            ctx.render(ApiResult.failNoI18n(ex.getCode(), ex.getResult().getDescription()));
        }catch (MissingResourceException ex){
            // 表单验证异常处理
            log.error("缺失国际化编码内容 key:{}",ex.getKey());
            if(CommonErrorConstant.SUCCESS.message().equals(ex.getKey())){
                ctx.render(ApiResult.ok());
            }
            ctx.render(ApiResult.fail(ex.getKey()));
        }catch (Exception ex){
            // 其他异常
            ex.printStackTrace();
            ctx.render(ApiResult.failNoI18n(ErrorConstant.ERROR.errCode(),ex.getMessage()));
        }

    }
}
