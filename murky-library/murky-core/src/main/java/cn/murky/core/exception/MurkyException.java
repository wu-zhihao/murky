package cn.murky.core.exception;

import lombok.Getter;

/**
 * 异常包装类
 */
public class MurkyException extends RuntimeException{

    private Throwable cause=this;
    public MurkyException(Throwable cause) {
        this.cause=cause;
    }

    public Throwable cause(){
        return (cause==this ? null : cause);
    }
}
