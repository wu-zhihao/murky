package cn.murky.core.filter;

import cn.murky.core.exception.MurkyException;
import lombok.extern.slf4j.Slf4j;
import org.noear.dami.exception.DamiException;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Filter;
import org.noear.solon.core.handle.FilterChain;

@Component(index = 0)
@Slf4j
public class CauseExceptionFilter implements Filter {
    @Override
    public void doFilter(Context ctx, FilterChain chain) throws Throwable {
        try {
            chain.doFilter(ctx);
        }catch (DamiException ex){
            // 解包damiBus异常
            throw ex.getCause();
        }catch (MurkyException ex){
            throw ex.cause();
        }
    }
}
