package cn.murky.core.constant;

import cn.murky.common.record.ErrorRecord;

public class ErrorConstant {
    public static final ErrorRecord ERROR = new ErrorRecord(500, "request.unknownError");
    public static final ErrorRecord ADD_ERROR = new ErrorRecord(1000, "request.addError");
    public static final ErrorRecord DELETE_ERROR = new ErrorRecord(1001, "request.removeError");
    public static final ErrorRecord EDIT_ERROR = new ErrorRecord(1002, "request.updateError");
    public static final ErrorRecord IMPORT_ERROR = new ErrorRecord(1003, "request.importError");

}
