package cn.murky.core.web;

import cn.murky.common.record.ErrorRecord;
import cn.murky.common.web.ApiResult;
import com.mybatisflex.core.service.IService;
import org.noear.solon.annotation.Inject;

/**
 * 公用Controller
 * @ignore
 * @auth hans
 */
public class BaseController<T extends IService<?>> {

    @Inject(required = false)
    protected T baseService;

    protected ApiResult<?> toResult(Boolean b) {
        return b ? ApiResult.ok() : ApiResult.fail();
    }


    protected ApiResult<?> toResult(Integer i) {
        return i > 0 ? ApiResult.ok() : ApiResult.fail();
    }

    protected ApiResult<?> toResult(Boolean b,String errorMsg) {
        return b? ApiResult.ok() : ApiResult.fail(errorMsg);
    }

    protected ApiResult<?> toResult(Boolean b, ErrorRecord errorRecord) {
        return b? ApiResult.ok() : ApiResult.fail(errorRecord);
    }

}
