package cn.murky.socketd.constants;

/**
 * socetd模块通用常量
 */
public class SocetdConstants {
    /**
     * 来源头
     */
    public static final String ORIGIN_COLUMN = "Origin";

    /**
     * 租户端连接管理端的令牌key
     */
    public static final String SD_TOKEN_COLUM = "ramdom_sa_token_colum";
    /**
     * 租户端连接管理端的令牌value
     */
    public static final String SD_TOKEN_VALUE = "R6VYDeEnIJL0Tlr";
}
